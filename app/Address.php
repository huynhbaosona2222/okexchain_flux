<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'creator_id','customer_name','address_detail','phone_number','address'
    ];
}
