<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;
    private $user = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (
            isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
        ) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }
        $urlResetPassword = $protocol . $_SERVER['HTTP_HOST'] . '/forgot-password';
        $userNewPassword = substr(md5($this->user->email . time()), 0, 10);
        $this->user->password = bcrypt($userNewPassword);
        $this->user->save();
        dd("dcm");
        return $this->view('mail-template.reset-password')
                    ->with([
                        'email' => $this->user->email,
                        'userNewPassword' => $userNewPassword,
                        'urlResetPassword' => $urlResetPassword
                    ])
                    ->subject('Your email has been reset!');
    }
}
