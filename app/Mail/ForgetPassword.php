<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $generatePassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$new_pass)
    {
        //
        $this->user = $user;
        $this->new_pass = $new_pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (
            isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
        ) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }
        $urlResetPassword = $protocol . $_SERVER['HTTP_HOST'] . '/reset-password?token=' . $this->user->otp;
        return $this->view('mail-template')
                        ->with([
                                'urlResetPassword' => $urlResetPassword, 
                                'token' => $this->user->otp,
                                'name' => $this->user->name,
                                'user_id' => $this->user->id,
                                'user_type' => $this->user->type,
                                'new_pass' => $this->new_pass,
                            ]);
    }
}
