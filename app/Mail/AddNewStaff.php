<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddNewStaff extends Mailable
{
    use Queueable, SerializesModels;
    private $user_info = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_info)
    {
        $this->user_info = $user_info;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('mail-template.add_new_staff')->with(['user_info'=>$user_info]);
    }
}
