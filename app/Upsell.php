<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upsell extends Model
{
    protected $fillable = [
        'id_product','new_price','qty'
    ];
}
