<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'name','slug','price','properties','id_category','discount','active','qty_available','description'
    ];
    public function tags()
    {
        return $this->belongsToMany(Tags::class);
    }
    public function hasTag($tagId){
        return in_array($tagId,$this->tags->pluck('id')->toArray());
    }
    
}
