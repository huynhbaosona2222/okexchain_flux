<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;
use App\Mail\ResetPassword;
use App\Helpers\AmazonS3;
use App\Mail\AddNewStaff;
use App\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected function isValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
    public function register(Request $request) {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if (!$this->isValidEmail($user->email)) {
            return redirect('/register')->with('error', 'Email is invalid');
        } else {
            $userEmail = User::where('email', $user->email)->first();
            if ($userEmail) return redirect('/register')->with('error', 'Email is already used');
        }
        $user->password = bcrypt($request->input('password'));
        $user->role_id = $request->role;
        $user->phone_number = $request->phone_number;
        if ($request->hasFile('ava_src')) {
            $ava_src = AmazonS3::uploadRandom($request->file('ava_src'), 'profile');
            $user->ava_src = $ava_src;
        }
        $user->save();
        $userData = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
        );
        Auth::attempt($userData);
        return redirect('/')->with('message', 'Welcome to PIXIO CMS');
    }
    public function login(Request $request) {
        if (!$this->isValidEmail($request->input('email'))) {
            if ($request->email)
                return redirect('/login')->with('error', 'Email is invalid');
        }
        $userData = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
        );
        Auth::attempt($userData);
        $user = Auth::user();

        if (!$user) {
            return redirect('/login')->with('error', 'Invalid Login information');
        }
        return redirect('/')->with('message', 'Welcome to Chillax');
    }
    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
    public function forgotPasswordView() {
        return view('auth.forgot-password');
    }
    public function forgotPassword(Request $request) {
        if (!$this->isValidEmail($request->input('email'))) {
            if ($request->email)
                return redirect('/forgot-password')->with('error', 'Email is invalid');
        }
        $user = User::where([
            'email' => $request->email,
            'status' => 1
        ])->first();
        if (!$user) {
            return redirect('/forgot-password')->with('error', 'Email has not verified yet!');
        }
        Mail::to($user->email)->send(new ResetPassword($user));
        if (count(Mail::failures()) > 0) {
            return redirect('/forgot-password')->with('error', 'There is something wrong with your account. Please try again later!');
        } else {
            return redirect('/forgot-password')->with('error', 'Please check email to reset your account');
        }
    }
    //***** */
    //Sonhuynh 
    //***** */

    public function getProfile()
    {
        $user_info=Auth::user();
        $role=Role::find($user_info->role_id);
        $user_info->role_name=$role->name;
        return view('profile')->with([
            'user_info'=>$user_info,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user_info=Auth::user();
        $user_info->name=$request->name;
        $user_info->phone_number=$request->phone_number;
        if($request->ava_src)
        {
            if($user_info->ava_src)
            {
                AmazonS3::delete($user_info->ava_src);
            }
            $user_info->ava_src = AmazonS3::uploadRandom($request->ava_src);
        }
        $user_info->save();
        return back()->with('success','Update Data Successfully');
    }

    public function updatePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return back()->with("success","Password changed successfully !");

    }

    public function getMemberPage()
    {
        $list_member=User::all();
        $list_role=Role::all();
        // dd($list_role);
        return view('member.member_list')->with([
            'list_member'=>$list_member,
            'list_role'=>$list_role
            ]);
    }

    public function addNewMember(Request $request)
    {
        if(!Auth::user()->id==1)
        {
            return back()->with('error','You do not have permission to do this action');
        }
        $check_user=User::where('email',$request->email)->first();
        if($check_user)
        {
            return back()->with('error','This email already exists ');
        }
        if (
            isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
        ) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }
        $url = $protocol . $_SERVER['HTTP_HOST'] . '/login';
        // dd($url);
        $rand_password=rand(000000000,999999999);
        $user_info=$request->all();
        Mail::send('mail-template.add_new_staff', ['name'=>$request->name,'email'=>$request->email,'password'=>$rand_password,'url'=>$url], function($message) use ($user_info){
            // $message->from('us@example.com', 'Laravel');
	        $message->to($user_info['email'], 'Visitor')->subject('Welcome to Chillax!');
	    });
        User::create([
            'email'=> $request->email,
            'name'=> $request->name,
            'phone_number'=> $request->phone_number,
            'role_id'=>$request->role,
            'ava_src'=>AmazonS3::uploadRandom($request->image),
            'password'=>Hash::make($rand_password),
        ]);
        
        return back()->with('success','Add new member successfully');
    }

    public function deleteMember($id)
    {
        if(!Auth::user()->id==1)
        {
            return back()->with('error','You do not have permission to do this action');
        }
        $user=User::find($id);
        if(!$user)
        {
            return back()->with('error','User is not found');
        }
        if($user->id==1)
        {
            return back()->with('error','Error');
        }
        if($user->ava_src)
        {
            AmazonS3::delete($user->ava_src);
        }
        $user->delete();
        return back()->with('success','Delete User successfully');
    }

    public function editMember($id)
    {
        if(!Auth::user()->id==1)
        {
            return back()->with('error','You do not have permission to do this action');
        }
        $user=User::find($id);
        if(!$user)
        {
            return back()->with('error','User is not found');
        }
        $list_member=User::all();
        $list_role=Role::all();
        return view('member.member_list')->with([
            'list_member'=>$list_member,
            'list_role'=>$list_role,
            'member'=>$user
        ]);
    }

    public function updateMember(Request $request,$id)
    {
        if(!Auth::user()->id==1)
        {
            return back()->with('error','You do not have permission to do this action');
        }
        $user=User::find($id);
        if(!$user)
        {
            return back()->with('error','User is not found');
        }
        // dd($request->all());
        $user->name=$request->name;
        $user->phone_number=$request->phone_number;
        $user->role_id=$request->role;
        if($request->new_password)
        {
            $password=Hash::make($request->new_password);
            $user->password=Hash::make($request->new_password);
        }
        if($request->image)
        {
            if($user->ava_src)
            {
                AmazonS3::delete($user->ava_src);
            }
            $user->ava_src = AmazonS3::uploadRandom($request->image);
        }
        $user->save();
        return back()->with('success','Update User successfully');
    }
}
