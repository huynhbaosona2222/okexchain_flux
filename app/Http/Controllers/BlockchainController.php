<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class BlockchainController extends Controller
{

    function getLatestBlock()
    {
        $endpoint = "https://blockchain.info/latestblock";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['query' => []]);
        $content = json_decode($response->getBody(), true);
        return $content;
    }

    function getInterva()
    {
        $endpoint = "https://blockchain.info/q/interval";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['query' => []]);
        $content = json_decode($response->getBody(), true);
        return $content;
    }

    public function index(Request $request)
    {
        $latest_block = $this->getLatestBlock();
        $current_block = $latest_block['height'];
        $target_block = 630000;
        $count = ($target_block - $current_block);
        $now = Carbon::now();
        $average_time = $this->getInterva();
        $target_time = $now->addMinute($count * ($average_time / 60))->format('M d,Y h:m:s');
        return view('countdown')->with('target_time', $target_time);
    }

    public function ttt(Request $request)
    {
        if (!$request->address) {
            return view('demo')->with('last_result', []);
        }
        $address = strtolower($request->address);
        $get_list = "https://www.oklink.com/api/explorer/v1/okexchain/addresses/".$address."/transfers/condition?t=1620959371282&offset=0&limit=1000&tokenType=OIP20";
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $get_list);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);
        $result = json_decode($output);

        // close curl resource to free up system resources
        curl_close($ch);
        $hits = $result->data->hits;
        $filter = [];
        foreach ($hits as $hit) {
            if ($hit->symbol == "SLP" && $hit->to == $address) {
                $trans_hash = 'https://www.oklink.com/api/explorer/v1/okexchain/transfers?offset=0&limit=1000&tranHash='.$hit->txhash;
                $filter[] = $trans_hash;
            }
        }
        foreach ($filter as $key => $item) {
            $data[$key] = curl_init($item);
            curl_setopt($data[$key], CURLOPT_RETURNTRANSFER, true);
        }
        // build the multi-curl handle, adding both $ch
        $mh = curl_multi_init();
        foreach ($filter as $key => $item) {
            curl_multi_add_handle($mh, $data[$key]);
        }

        // execute all queries simultaneously, and continue when all are complete
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        //close the handles
        foreach ($filter as $key => $item) {
            curl_multi_remove_handle($mh, $data[$key]);
        }
        curl_multi_close($mh);
        // all of our requests are done, we can now access the results
        $last_result = [];
        foreach ($filter as $key => $item) {
            $content = json_decode(curl_multi_getcontent($data[$key]));
            $transss = $content->data->hits;
            $a_trans = [];
            foreach ($transss as $i) {

                $mil = $i->blocktime;
                $seconds = $mil/1000;
                $dt = new DateTime("@$seconds");  // convert UNIX timestamp to PHP DateTime
                $a_trans['time'] = $dt->format('Y-m-d H:i:s');
                if ($i->symbol == "FLUXK") {
                    $a_trans['FLUX'] = $i->value;
                }
                if ($i->symbol == "WOKT") {
                    $a_trans['OKT'] = $i->value;
                }
            }
            $last_result[] = $a_trans;
        }
        // dd($last_result);
        return view('demo')->with('last_result', $last_result);
    }
}
