<?php

namespace App\Http\Controllers;

use App\DemoBills;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getOrderDetail($id)
    {
        $bill = DemoBills::find($id);
        if (!$bill) {
            $bill = DemoBills::find(10);
        }
        $user = User::find($bill->id_user);
        if ($user) {
            $bill->customer_name = $user->name;
        } else {
            $bill->customer_name = "User Not Found";
        }
        return view('order.detail')->with('bill', $bill);
    }
}
