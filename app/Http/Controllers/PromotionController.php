<?php

namespace App\Http\Controllers;

use App\Helpers\Pagination;
use App\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    private $MAX_VALUE = 10;
    function createSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        // Lowercase
        if ($options['lowercase']) {
            $str = mb_strtolower($str, 'UTF-8');
        }

        $char_map = array(
            // Latin
            'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'đ' => 'd', 'é' => 'e', 'è' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $str . '-' . time();
    }
    public function index(Request $request)
    {
        $list_promotion = Promotion::where([]);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $list_promotion->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $list_promotion = $list_promotion->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        foreach($list_promotion as $item)
        {
            // dd(Carbon::parse($item->expire_time)->format('Y.m.d'));
            $item->expire=Carbon::parse($item->expire_time)->format('Y-m-d');
            if($item->type==0)
            {
                $item->type_name= 'Precentage Discount';
            }else
            {
                $item->type_name= 'Price Discount';
            }
        }
        return view('promotion.index')->with([
            'promotions' => $list_promotion,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function createPromotion()
    {
        return view('promotion.create_promotion');
    }

    public function postcreatePromotion(Request $request)
    {
        if(!$request->description)
        {
            $request->description=" ";
        }
        $code=$request->name;
        $check_code=Promotion::where('name',$code)->first();
        if($check_code)
        {
            return back()->with('error', 'Promotion code is already exist');
        }
        $now = Carbon::now();
        $date = Carbon::parse($request->time);
        if ($date->lessThan($now)) {
            return back()->with('error', 'Date must be in future');
        }
        if ($request->percent_discount === null) {
            // dd(1);
            $value = $request->price_discount;
            $type = 1;
        } elseif ($request->price_discount === null) {
            $value = $request->percent_discount;
            $type = 0;
        }
        Promotion::create([
            'name' => $request->name,
            'description' => $request->description,
            'value' => $value,
            'type' => $type,
            'expire_time' => $date,
            'slug' => $this->createSlug($request->name),

        ]);
        return back()->with('success', 'Create Promotion Successfully');
    }

    public function editPromotion($id)
    {
        $promo = Promotion::find($id);
        $promo->expire=Carbon::parse($promo->expire_time)->format('m/d/Y');

        if (!$promo) {
            return back()->with('error', 'Promotion is not found');
        }
        return view('promotion.create_promotion')->with([
            'promotion' => $promo
        ]);
    }

    public function updatePromotion(Request $request, $id)
    {
        if(!$request->description)
        {
            $request->description=" ";
        }
        $code=$request->name;
        $check_code=Promotion::where('name',$code)->first();
        if($check_code)
        {
            if($check_code->id!=$id)
            return back()->with('error', 'Promotion code is already exist');
        }
        $now = Carbon::now();
        $date = Carbon::parse($request->time);
        if ($date->lessThan($now)) {
            return back()->with('error', 'Date must be in future');
        }
        if ($request->percent_discount === null) {
            $value = $request->price_discount;
            $type = 1;
        } elseif ($request->price_discount === null) {
            $value = $request->percent_discount;
            $type = 0;
        }
        $promo = Promotion::find($id);
        if (!$promo) {
            return back()->with('error', 'Promotion is not found');
        }
        $promo->name = $request->name;
        $promo->description = $request->description;
        $promo->value = $value;
        $promo->type = $type;
        $promo->save();

        return back()->with(
            'success',
            "Update Data Successfully"
        );
    }

    public function deletePromotion($id)
    {
        $promo = Promotion::find($id);
        if (!$promo) {
            return back()->with('error', 'Promotion is not found');
        }
        $promo->delete();
        return back()->with('success', "Delete Promotion Successfully");
    }
}
