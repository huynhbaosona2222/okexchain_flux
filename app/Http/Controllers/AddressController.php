<?php

namespace App\Http\Controllers;

use App\Address;
use App\DemoBills;
use App\District;
use App\Helpers\Pagination;
use App\Products;
use App\Province;
use App\Size;
use App\User;
use App\Ward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{

    private $MAX_VALUE = 15;

    public function index(Request $request)
    {
        $address = Address::all();
        $customer = User::where('role_id', 3);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $customer->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $customer = $customer->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        foreach ($address as $item) {
            $user_info = User::find($item->creator_id);
            if ($user_info) {
                $item->creator_name = $user_info->name;
            } else {
                $item->creator_name = "undefinded";
            }
        }
        return view('customer.index')->with([
            'address' => $address,
            'customer' => $customer,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function editAddress($id)
    {
        $province_list = Province::all();
        $address = Address::find($id);
        if ($address) {
            $user_info = User::find($address->creator_id);
            if ($user_info) {
                $address->creator_name = $user_info->name;
            } else {
                $address->creator_name = "undefinded";
            }
            return view('customer.custom')->with([
                'address' => $address,
                'province_list' => $province_list,

            ]);
        }
    }

    public function deleteAddress($id)
    {
        $address = Address::find($id);
        if ($address) {
            $address->delete();
            return back()->with('success', 'Delete this address successfully');
        }
        return back()->with('error', 'This Address Is Not Found');
    }

    public function updateAddress(Request $request, $id)
    {
        $address = Address::find($id);
        if ($address) {
            if ($request->ward_id) {
                $city_name = Province::find($request->city_id)->_name;
                $state_name = District::find($request->state_id)->_name;
                $ward_name = Ward::find($request->ward_id)->_name;
                $address->address = $city_name . " - " . $state_name . " - " . $ward_name;
            }
            $address->customer_name = $request->name;
            $address->address_detail = $request->address_detail;
            $address->phone_number = $request->phone_number;
            $address->save();
            return back()->with('success', 'Update this address successfully');
        }
        return back()->with('error', 'This Address Is Not Found');
    }

    public function createAddress()
    {
        $province_list = Province::all();
        return view('customer.custom')->with([
            'province_list' => $province_list,
        ]);
    }

    public function postcreateAddress(Request $request)
    {
        $city_name = Province::find($request->city_id)->_name;
        $state_name = District::find($request->state_id)->_name;
        $ward_name = Ward::find($request->ward_id)->_name;
        $address = $city_name . " - " . $state_name . " - " . $ward_name;
        Address::create([
            'creator_id' => Auth::user()->id,
            'customer_name' => $request->name,
            'address' => $address,
            'address_detail' => $request->address_detail,
            'phone_number' => $request->phone_number,
        ]);
        return back()->with('success', 'Added this address successfully');
    }

    public function getStates($id)
    {
        $states = District::where('_province_id', $id)->get();
        return response()->json(['states' => $states]);
    }

    public function getWards($id)
    {
        $wards = Ward::where('_district_id', $id)->get();
        return response()->json(['wards' => $wards]);
    }

    public function getCountDown()
    {
        return view('customer.countdown');
    }

    public function getDemoCart()
    {
        return view('customer.demo');
    }

    public function ComeBack()
    {
        return redirect('/admin/demo-cart')->with('error', 'Payment time out, please try again!!');
    }

    public function getCustomerDetail(Request $request, $id)
    {

        $date = Carbon::now();
        $count_day_in_month = $date->daysInMonth;
        $count_day_in_last_month = $date->subMonth()->daysInMonth;
        $arr_days = [];
        for ($i = 1; $i < 32; $i++) {
            $arr_days[] = $i;
        }
        $count_bill_in_month = [];
        for ($i = 1; $i <= $count_day_in_month; $i++) {
            $count_bill_in_month[] = 0;
        }
        // dd($count_bill_in_month);
        $date = Carbon::now();
        $list_bills = DemoBills::where('id_user', $id)->whereMonth('created_at', $date->month)->whereYear('created_at', $date->year)->get()->toArray();
        if (count($list_bills) > 0)
            foreach ($list_bills as $item) {
                $day = Carbon::parse($item['created_at'])->day;
                $count_bill_in_month[$day - 1]++;
            }
        for ($i = 1; $i <= $count_day_in_last_month; $i++) {
            $count_bill_in_last_month[] = 0;
        }
        $date = Carbon::now();
        $list_bills_last_month = DemoBills::where('id_user', $id)->whereMonth('created_at', $date->subMonth()->month)->whereYear('created_at', $date->year)->get()->toArray();
        if (count($list_bills_last_month) > 0) {
            foreach ($list_bills_last_month as $item) {
                $day = Carbon::parse($item['created_at'])->day;
                $count_bill_in_last_month[$day - 1]++;
            }
        }
        if (Auth::user()->id != 1) {
            return back()->with('error', 'You don\'t have permission to see this page');
        }
        $customer_info = User::find($id);
        if (!$customer_info) {
            return back()->with('error', 'Customer is not found');
        }
        $address_list = Address::where('creator_id', $id)->get();
        $bills_demo = DemoBills::where('id_user', $id);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $bills_demo->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $bills_demo = $bills_demo->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        return view('customer.customer_detail')->with([
            'customer_info' => $customer_info,
            'address_list' => $address_list,
            'arr_days' => json_encode($arr_days),
            'count_bill_in_month' => json_encode($count_bill_in_month),
            'count_bill_in_last_month' => json_encode($count_bill_in_last_month),
            'bills_demo' => $bills_demo,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function update()
    {
        // $int = rand(1578000000, 1585400000);
        // $int = rand(1585400000, 1585500000);
        $int = rand(1585100000, 1585400000);
        // $int= (1588000000);
        // dd($int);1580000000
        $string = date("Y-m-d H:i:s", $int);
        // dd($string);
        for ($i = 1; $i <= 200; $i++) {
            DemoBills::create([
                'id_bill' => 'CHILL' . rand(000000, 999999),
                'id_user' => rand(1, 5),
                'created_at' => $string = date("Y-m-d H:i:s", rand(1585100000, 1585400000))
            ]);
        }
        return redirect('/admin/customer/2');
    }

    public function gethour()
    {
        $add_total = DemoBills::all();
        foreach ($add_total as $item) {
            $item->total_price = rand(111111, 999999);
            $item->save();
        }
        return back();
    }

    public function addSize()
    {
        $size = ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL'];
        for ($i = 0; $i < 9; $i++) {
            Size::create([
                'size' => $size[$i],
            ]);
        }
        return "ok";
    }
}
