<?php

namespace App\Http\Controllers;

use App\DemoBills;
use App\Helpers\Pagination;
use Illuminate\Http\Request;

use App\User;
use App\Project;
use App\IUserProject;
use App\Order;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class ViewController extends Controller
{
    private $MAX_VALUE = 15;
    private function countIncome($arr_bills)
    {
        $income = 0;
        foreach ($arr_bills as $item) {
            $income = $income + $item['total_price'];
        }
        return $income;
    }
    public function index(Request $request)
    {
        $active = 2;
        // $id = 2;
        $date = Carbon::now();
        $count_day_in_month = $date->daysInMonth;
        $count_day_in_last_month = $date->subMonth()->daysInMonth;
        $arr_days = [];
        for ($i = 1; $i < 32; $i++) {
            $arr_days[] = $i;
        }
        $count_bill_in_time = [];
        for ($i = 1; $i <= $count_day_in_month; $i++) {
            $count_bill_in_time[] = 0;
        }
        $date = Carbon::now();
        // $list_bills = DemoBills::where('id_user', $id)->whereMonth('created_at', $date->month)->whereYear('created_at', $date->year)->get()->toArray();

        $list_bills = DemoBills::whereMonth('created_at', $date->month)->whereYear('created_at', $date->year)->get()->toArray();
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        $income = $this->countIncome($list_bills);
        $count_bills = count($list_bills);
        if ($date->month == 2) {
            $list_last_bills = DemoBills::whereMonth('created_at', 1)->whereYear('created_at', $date->year)->get()->toArray();
            $list_last__bills = DemoBills::whereMonth('created_at', 12)->whereYear('created_at', $date->year - 1)->get()->toArray();
        } elseif ($date->month == 1) {
            $list_last_bills = DemoBills::whereMonth('created_at', 12)->whereYear('created_at', $date->year - 1)->get()->toArray();
            $list_last__bills = DemoBills::whereMonth('created_at', 11)->whereYear('created_at', $date->year - 1)->get()->toArray();
        } else {
            $list_last_bills = DemoBills::whereMonth('created_at', $date->month - 1)->whereYear('created_at', $date->year)->get()->toArray();
            $list_last__bills = DemoBills::whereMonth('created_at', $date->month - 2)->whereYear('created_at', $date->year)->get()->toArray();
        }
        $last_income = $this->countIncome($list_last_bills);
        $count_last_bills = count($list_last_bills);
        $count_last__bills = count($list_last__bills);
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ
        //lấy ra các thông số để so sánh tăng trưởng trong kỳ

        if (count($list_bills) > 0)
            foreach ($list_bills as $item) {
                $day = Carbon::parse($item['created_at'])->day;
                $count_bill_in_time[$day - 1]++;
            }
        for ($i = 1; $i <= $count_day_in_last_month; $i++) {
            $count_bill_in_last_month[] = 0;
        }
        $date = Carbon::now();
        $list_bills_last_month = DemoBills::whereMonth('created_at', $date->subMonth()->month)->whereYear('created_at', $date->year)->get()->toArray();
        if (count($list_bills_last_month) > 0) {
            foreach ($list_bills_last_month as $item) {
                $day = Carbon::parse($item['created_at'])->day;
                $count_bill_in_last_month[$day - 1]++;
            }
        }
        $orders = Order::where([]);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $orders->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $orders = $orders->orderBy('order_created', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        foreach ($orders as $item) {
            $demo = json_decode($item->order_detai);
            $item->tien_thu_ho = $demo->TienThuHo;
        }
        if (isset($request->sort)) {
            $sort = $request->sort;
            if ($sort == "week") {
                $active = 1;
                for ($i = 1; $i <= 7; $i++) {
                    $count_bill_in_time[] = 0;
                }
                $arr_days = array('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN');
                $now = Carbon::now(); //spesific day
                $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d H:i');
                $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d H:i');
                $monday = (Carbon::parse($weekStartDate)->day);
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                $last_weekStartDate = Carbon::now()->subDays(7)->startOfWeek()->format('Y-m-d H:i');
                $last_weekEndDate = Carbon::now()->subDays(7)->endOfWeek()->format('Y-m-d H:i');
                $last_monday = (Carbon::parse($last_weekStartDate)->day);
                $list_last_bills = DemoBills::whereBetween('created_at', [$last_weekStartDate, $last_weekEndDate])->get()->toArray();
                $count_last_bills = count($list_last_bills);
                $last_income = $this->countIncome($list_last_bills);
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                //lấy ra các thông số để so sánh tăng trưởng trong kỳ
                $week = [];
                for ($i = 0; $i < 7; $i++) {
                    $week[] = Carbon::parse(Carbon::now()->startOfWeek()->addDay($i)->format('Y-m-d')); //push the current day and plus the mount of $i 
                }
                $list_bills = DemoBills::whereBetween('created_at', [$weekStartDate, $weekEndDate])->get();
                $count_bills=count($list_bills);
                $income = $this->countIncome($list_bills);

                if (count($list_bills) > 0) {
                    foreach ($count_bill_in_time as $val => $item) {
                        $count = DemoBills::whereBetween('created_at', [$weekStartDate, $weekEndDate])->whereDay('created_at', $monday + $val)->get();
                        $count_bill_in_time[$val] = count($count);
                    }
                }
            } elseif ($sort == "quater") {
                $now = Carbon::now();
                $active = 3;
                $lastOfQuarter = Carbon::now()->lastOfQuarter()->format('Y-m-d H:i');
                $firstOfQuarter = Carbon::now()->firstOfQuarter()->format('Y-m-d H:i');
                $ranges = CarbonPeriod::create($firstOfQuarter,  $lastOfQuarter);
                $dates = $ranges->toArray();
                $arr_days = [];
                foreach ($dates as $item) {
                    $arr_days[] = $item->format('Y-m-d');
                }
                unset($count_bill_in_time);
                for ($i = 1; $i <= count($dates); $i++) {
                    $count_bill_in_time[] = 0;
                }
                $first_day = (Carbon::parse($firstOfQuarter));
                foreach ($count_bill_in_time as $val => $item) {
                    $count = DemoBills::whereDate('created_at', ($first_day))->get();
                    $count_bill_in_time[$val] = count($count);
                    $first_day->addDays();
                }
            } elseif ($sort == "day") {
                $now = Carbon::now();
                $last_day=(Carbon::now()->subDays(1));
                $last__day=(Carbon::now()->subDays(2));
                $active = 0;
                $arr_days = [];
                $count_bill_in_time = [];
                for ($i = 0; $i < 24; $i++) {
                    $arr_days[] = $i."h00";
                    $count_bill_in_time[] = 0;
                }
                $list_bills = DemoBills::whereDate('created_at', $now)->get();
                $list_last_bills = DemoBills::whereDate('created_at', $last_day)->get();
                $list_last__bills = DemoBills::whereDate('created_at', $last__day)->get();
                $count_bills=count($list_bills);
                $count_last__bills=count($list_last_bills);
                $income=$this->countIncome($list_bills);
                $last_income=$this->countIncome($list_last_bills);
                if ($count_bills > 0) {
                    foreach ($list_bills as $item) {
                        $hour = Carbon::parse($item['created_at'])->hour;
                        $count_bill_in_time[$hour]++;
                    }
                }
            }
        }
        return view('dashboard')->with([
            'arr_days' => json_encode($arr_days),
            'count_bill_in_time' => json_encode($count_bill_in_time),
            'count_bill_in_last_month' => json_encode($count_bill_in_last_month),
            'count_bills'=>$count_bills,
            'count_last_bills'=>$count_last_bills,
            'count_last__bills'=>$count_last__bills,
            'income'=>$income,
            'last_income'=>$last_income,
            'active' => $active,
            'orders' => $orders,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }
}
