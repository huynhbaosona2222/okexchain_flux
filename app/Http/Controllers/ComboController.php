<?php

namespace App\Http\Controllers;

use App\Combo;
use App\Helpers\AmazonS3;
use App\Helpers\Pagination;
use App\ProductImage;
use App\Products;
use Illuminate\Http\Request;

class ComboController extends Controller
{
    private $MAX_VALUE = 10;

    function createSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        // Lowercase
        if ($options['lowercase']) {
            $str = mb_strtolower($str, 'UTF-8');
        }

        $char_map = array(
            // Latin
            'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'đ' => 'd', 'é' => 'e', 'è' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $str . '-' . time();
    }

    public function index(Request $request)
    {
        $combo_list=Combo::where([]);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $combo_list->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $combo_list = $combo_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        return view('combo.index')->with([
            'combo_list' => $combo_list,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function createCombo()
    {
        $list_pdt=Products::all();
        foreach ($list_pdt as $product) {
            $image = ProductImage::where('id_product',$product->id)->first();
            if ($image) {
                $product->image = $image->link;
            }
        }
        return view('combo.create_combo')->with([
            'list_pdt' => $list_pdt
        ]);
    }

    public function postcreateCombo(Request $request)
    {
        if(count($request->products) < 2)
        {
            session()->flash('error', 'Select More Than 2 Product');
            return back();
        }
        Combo::create([
            'name'=>$request->name,
            'new_price'=>$request->price,
            'products_id'=>json_encode($request->products),
            // 'qty' => $request->qty_available,
            'description' => $request->description,
            'slug' => $this->createSlug($request->name),
        ]);
        session()->flash('success', 'Create Combo Successfully');
        return back();
    }

    public function editCombo($id)
    {
        $combo=Combo::find($id);
        if($combo)
        {
            $pdts_combo=(json_decode($combo->products_id));
            $list_pdt=Products::whereIn('id',$pdts_combo)->get();
            return view('combo.create_combo')->with([
                'combo' => $combo,
                'list_pdt' => $list_pdt,
            ]);
        }
        session()->flash('error', 'This Combo Is Not Found');
        return back();
    }

    public function updateCombo(Request $request,$id)
    {
        if(count($request->products) < 2)
        {
            session()->flash('error', 'Select More Than 2 Product');
            return back();
        }
        $combo=Combo::find($id);
        if($combo)
        {
            $combo->name=$request->name;
            $combo->new_price=$request->price;
            // $combo->qty=$request->qty_available;
            $combo->products_id=json_encode($request->products);
            $combo->description=$request->description;
            $combo->save();
            session()->flash('success', 'Update Combo Successfully');
            return back();
        }
        session()->flash('error', 'Error');
            return back();
    }

    // public function updateComboImage(Request $request, $id)
    // {
    //     if ($request->images) {
    //         foreach ($request->images as $item) {
    //             ProductImage::create([
    //                 'id_combo' => $request->id_combo,
    //                 'link' => AmazonS3::uploadRandom($item),
    //             ]);
    //         }
    //         session()->flash('success', 'Update Image Successfully');
    //         return back();
    //     }
    //     $image = ProductImage::find($id);
    //     if ($image) {
    //         AmazonS3::delete($image->link);
    //         $image->link = AmazonS3::uploadRandom($request->link);
    //         $image->save();
    //         session()->flash('success', 'Update Image Successfully');
    //         return back();
    //     }
    //     session()->flash('error', 'This image is not Found');
    //     return back();
    // }

    public function deleteCombo($id)
    {
        $combo=Combo::find($id);
        if($combo)
        {
            // $list_image=ProductImage::where('id_combo', $combo->id)->get();
            // if($list_image)
            // {
            //     foreach($list_image as $item)
            //     {
            //         AmazonS3::delete($item->link);
            //         $item->delete();
            //     }
            // }
            $combo->delete();
            session()->flash('success', 'Delete Combo Successfully');
            return back();
        }
        session()->flash('Error', 'This Combo Is Not Found');
            return back();
    }

    public function findPdt(Request $request)
    {
        $term = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $products = Products::query()->where('name', 'LIKE', "%{$term}%") ->limit(10)->get();
        // return trim($request->q);

        $formatted_pdts = [];
        foreach ($products as $item) {
            $formatted_pdts[] = ['id' => $item->id, 'text' => $item->name];
        }
        return \Response::json($formatted_pdts);
    }
}
