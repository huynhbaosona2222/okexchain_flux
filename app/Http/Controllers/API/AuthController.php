<?php

namespace App\Http\Controllers\Api;

use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\ForgetPassword_mobile;
use App\Http\Controllers\Controller;
use App\UserSocial;
use Carbon\Carbon;
use Validator;
use App\User;
use App\Mail\ForgetPassword;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    function createSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        // Lowercase
        if ($options['lowercase']) {
            $str = mb_strtolower($str, 'UTF-8');
        }

        $char_map = array(
            // Latin
            'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'đ' => 'd', 'é' => 'e', 'è' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $str . '-' . time();
        // return $slug . '-' . time();
    }

    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone_number' => 'required|integer',
            'address' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->role_id = 3;
        $user->phone_number = $request->phone_number;
        $user->ava_src = '/default-avatar.jpg';
        $user->password = bcrypt($request->password);
        // $user->uuid = $this->createSlug($request->name);
        $user->save();

        $user_info = [];
        $user_info['name'] = $user->name;
        $user_info['ava_src'] = $user->ava_src;
        // $user_info['uuid']=$user->uuid;
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'message' => 'Successful',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user_info' => $user_info
        ]);
    }

    public function socialLogin(Request $request)
    {
        $email = $request->email;
        $name = $request->name;
        $user = User::where('email', $email)->first();
        if (!$user) {
            $user = new User();
            $user->email = $email;
            $user->name = $name;
            $now = Carbon::now();
            $user->password = bcrypt($now);
            $user->uuid = $this->createSlug($request->name);
            $user->save();

            $userSocial = new UserSocial();
            $userSocial->user_id = $user->id;
            $userSocial->provider = $request->provider;
            $userSocial->save();
        } else {
            $userSocial = UserSocial::where([
                'user_id' => $user->id,
                'provider' => $request->provider,
            ])->first();
            if (!$userSocial) {
                $userSocial = new UserSocial();
                $userSocial->user_id = $user->id;
                $userSocial->provider = $request->provider;
                $userSocial->save();
            }
        }
        Auth::login($user);
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'message' => 'Successful',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'message' => 'Successful',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }
    //create otp
    public function generateOTP()
    {
        $otp = mt_rand(100000, 999999);
        return $otp;
    }
    //send email and reset password
    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $now = Carbon::now();
            $user->otp = $this->generateOTP();
            $user->expired_otp = $now;
            $user->save();

            Mail::to($user->email)
                ->send(new ForgetPassword_mobile($user));
            return response()->json([
                'message' => 'Mail have been sent',
            ], 200);
        } else {
            return response()->json([
                'message' => 'unauthorized',
            ], 404);
        }
    }
    public function confirmOTP(Request $request)
    {
        $now = Carbon::now();
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->otp == $request->otp) {
                $otp_created = $user->expired_otp;
                $otp_created_carbon = Carbon::parse(($otp_created));
                if ($otp_created_carbon->addMinutes(10)->greaterThan($now)) {
                    return response()->json([
                        'message' => 'Confirm OTP success',
                        'user_id' => $user->id,
                    ], 200);
                } else {
                    return response()->json([
                        'message' => 'Expried time confirm'
                    ], 404);
                }
            } else {
                return response()->json([
                    'message' => 'Invalid otp'
                ], 401);
            }
        } else {
            return response()->json([
                'message' => 'unauthorized',
            ], 404);
        }
    }
    public function updateInfo(Request $request)
    {
        $user = Auth::user();
        $check_pass = $request->edit_password;
        $check_info = $request->edit_info;
        if ($user) {
            if ($check_pass == null && $check_info == null) {
                return response()->json([
                    'message' => 'Nothing to update',
                ], 400);
            }
            if ($check_pass == 1) {
                if (Hash::check($request->current_password, $user->password)) {
                    $user->password = bcrypt($request->password);
                } else {
                    return response()->json([
                        'message' => 'Curent Password is not right',
                    ], 400);
                }
            }
            if ($check_info == 1) {
                $user->name = $request->name;
                $user->phone_number = $request->phone_number;
                $user->address = $request->address;
                $user->sex = $request->sex;
            }
            $user->save();
            return response()->json([
                'message' => 'Edit info success',
                'request' => $request->all()
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unauthorized',
            ], 404);
        }
    }

    public function postForgetPassword(Request $request)
    {
        $email = $request->email;
        $phone = $request->phone;
        if ($email) {
            $user = User::where('email', $email)->first();
            if ($user) {
                $now = Carbon::now();
                $new_pass = 'nothing';
                $user->otp = bcrypt($now);
                $new_pass = rand(100000000, 999999999);
                $user->password = bcrypt($new_pass);
                $user->expired_otp = $now;
                $user->save();
                Mail::to($email)
                    ->send(new ForgetPassword($user, $new_pass));
                return response()->json([
                    'message' => 'Check your email to reset passsword'
                ], 200);
            }
            return response()->json([
                'message' => 'Email does not exist!'
            ], 404);
        } elseif ($phone) {
            $user = User::where('phone_number', $phone)->first();
            if ($user) {
                $phone = $user->phone_number;
                $rand = rand(100000, 999999);
                $user->otp = $rand;
                $user->expired_otp = Carbon::now();
                $send_code = $this->send($rand, $phone);
                if ($send_code['CodeResult'] != 100) {
                    return response()->json([
                        'message' => 'Somethings wrong',
                    ], 400);
                }
                $user->otp = $rand;
                $user->save();
                return response()->json([
                    'message' => 'Success',
                ], 200);
            }
            return response()->json([
                'message' => 'Phone Number does not exist!'
            ], 404);
        }
        return response()->json([
            'message' => 'Email or Phone Number can not be null'
        ], 400);
    }

    public function postResetPassword(Request $request)
    {

        $now = Carbon::now();
        $token = $request->token;
        $otp = $request->otp;
        if(!$request->password)
        {
            return response()->json([
                'message' => 'Missng data',
            ], 400);
        }
        if ($token) {
            $user = User::where('otp', $token)->first();
            if ($user) {
                $token_created = $user->expired_otp;
                $token_created_carbon = Carbon::parse(($token_created));
                if ($user && ($token_created_carbon->addMinutes(5)->greaterThan($now))) {
                    $user->password = bcrypt($request->password);
                    $user->save();
                    return response()->json([
                        'message' => 'Update Password Successfully',
                    ], 200);
                }
                return response()->json([
                    'message' => 'Expired time',
                ], 400);
            }
            return response()->json([
                'message' => 'User is not found',
            ], 400);
        } elseif ($otp) {
            $user = User::where('otp', $otp)->first();
            if ($user) {
                $token_created = $user->expired_otp;
                $token_created_carbon = Carbon::parse(($token_created));
                if ($user && ($token_created_carbon->addMinutes(5)->greaterThan($now))) {
                    $user->password = bcrypt($request->password);
                    $user->otp = null;
                    $user->save();
                    return response()->json([
                        'message' => 'Update Password Successfully',
                    ], 200);
                }
                return response()->json([
                    'message' => 'Expired time',
                ], 400);
            }
            return response()->json([
                'message' => 'User is not found',
            ], 400);
        }
        return response()->json([
            'message' => 'Missing data'
        ], 400);
    }

    public function send($rand, $phone)
    {
        $endpoint = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get";
        $client = new \GuzzleHttp\Client();
        $api_key = "3D9645E7E0729D0F9D68009F8EA891";
        $serect_key = "B0E147D42575D4639B9B4EDBBDD271";
        $content = "Chao mung den voi Chillax, Ma xac nhan cua ban la " . $rand . ". Ma se het hang sau 5 phut";
        // $phone = "0373205443";
        $response = $client->request('GET', $endpoint, ['query' => [
            'Phone' => $phone,
            'Content' => $content,
            'ApiKey' => $api_key,
            'SecretKey' => $serect_key,
            'SmsType' => 2,
            'Brandname' => 'NhacLichhen',
        ]]);
        $content = json_decode($response->getBody(), true);
        return $content;
    }

    public function sendOTP()
    {
        $user = Auth::user();
        if ($user) {
            $phone = $user->phone_number;
            $rand = rand(1000, 4444);
            $user->otp = $rand;
            $user->expired_otp = Carbon::now();
            $send_code = $this->send($rand, $phone);
            if ($send_code['CodeResult'] != 100) {
                return response()->json([
                    'message' => 'Somethings wrong',
                ], 400);
            }
            $user->otp = $rand;
            $user->save();
            return response()->json([
                'message' => 'Success',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unauthorized',
            ], 404);
        }
    }

    public function submitOTP(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            $otp = $request->otp;
            if (!$otp) {
                return response()->json([
                    'message' => 'OTP can not be null',
                ], 404);
            }
            $token_created = $user->expired_otp;
            $now = Carbon::now();
            $token_created_carbon = Carbon::parse(($token_created));
            if ($token_created_carbon->addMinutes(5)->greaterThan($now)) {
                if ($otp == $user->otp) {
                    $user->is_verify = true;
                    $user->otp = null;
                    $user->save();
                    return response()->json([
                        'message' => 'Verify Phone Successfully',
                    ], 200);
                } else {
                    return response()->json([
                        'message' => 'OTP is not right, try again',
                    ], 404);
                }
            }
            return response()->json([
                'message' => 'Expired time',
            ], 400);
        } else {
            return response()->json([
                'message' => 'Unauthorized',
            ], 404);
        }
    }
}
