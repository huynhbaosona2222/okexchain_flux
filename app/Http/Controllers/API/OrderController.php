<?php

namespace App\Http\Controllers\API;

use App\DemoBills;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function getOrderDetail(Request $request)
    {
        $order = Order::where(['order_id' => $request->order_id, 'customer_phone' => $request->customer_phone])->first();
        if ($order === null) {
            return response()->json([
                'message' => 'Order is not found',
            ], 404);
        }
        if (isset($order) & $order != null)
            $order->detail = json_decode($order->order_detai); {
            return response()->json([
                'message' => 'Success',
                'order' => $order,
            ], 200);
        }
    }

    public function getCustomerOrder(Request $request)
    {
        $user = Auth::user();
        $email = $request->email;
        $phone = $request->phone;
        if ($user) {
            if ($user->id != 1) {
                return response()->json([
                    'message' => 'Permission Error',
                ], 404);
            }
            if ($email) {
                $customer = User::where('email', $email)->first();
                if (!$customer) {
                    return response()->json([
                        'message' => 'Customer is not found',
                    ], 404);
                }
                $customer_id = $customer->id;
            } elseif ($phone) {
                $customer = User::where('phone_number', $phone)->first();
                if (!$customer) {
                    return response()->json([
                        'message' => 'Customer is not found',
                    ], 404);
                }
                $customer_id = $customer->id;
            }
            $list_order = DemoBills::where('id_user', $customer_id)->take(10)->get();
            return response()->json([
                'message' => 'success',
                'list_order' => $list_order,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Unauthorized',
            ], 404);
        }
    }

    public function send()
    {
        $endpoint = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get";
        $client = new \GuzzleHttp\Client();
        $api_key = "3D9645E7E0729D0F9D68009F8EA891";
        $serect_key = "B0E147D42575D4639B9B4EDBBDD271";
        $content = "Chao mung den voi Chillax, Ma xac nhan cua ban la 1234";
        $phone = "0373205443";
        $response = $client->request('GET', $endpoint, ['query' => [
            'Phone' => $phone,
            'Content' => $content,
            'ApiKey' => $api_key,
            'SecretKey' => $serect_key,
            'SmsType' => 2,
            'Brandname' => 'NhacLichhen',
        ]]);
        $content = json_decode($response->getBody(), true);
        return $content;
    }

    public function demoSendCode()
    {
        $demo = $this->send();
        dd($demo);
        return ($demo);
    }
}
