<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Color;
use App\Combo;
use App\Helpers\Pagination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductColor;
use App\ProductImage;
use App\Products;
use App\ProductSize;
use App\Size;
use App\Upsell;

class HomeController extends Controller
{
    private $MAX_VALUE = 12;

    public function getProductsByCategory(Request $request, $category_slug)
    {
        $category = Category::where('slug', $category_slug)->first();
        if ($category) {
            $products_list = Products::where('id_category', $category->id);
            $page = $request->query('page') ? $request->query('page') : 1;
            $number = $products_list->count();
            $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
            $previousPage = ($page == 1) ? 1 : ($page - 1);
            $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
            $listPages = Pagination::initArray($page, $totalPage);
            $products_list = $products_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
            $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
            $currUrl = $fullUrl[0];
            foreach ($products_list as $item) {
                $item_colors_id = ProductColor::where(['product_id' => $item->id])->pluck('color_id')->toArray();
                $item_colors = Color::whereIn('id', $item_colors_id)->get();
                $item->colors = $item_colors;
            }
            return response()->json([
                'message' => 'Success',
                'products_list' => $products_list,
                'currUrl' => $currUrl,
                'totalPage' => $totalPage,
                'previousPage' => $previousPage,
                'nextPage' => $nextPage,
                'listPages' => $listPages,
                'currPage' => $page,
            ], 200);
        }
        return response()->json([
            'message' => 'This category not found',
        ], 404);
    }

    public function getProductsDetail($product_slug)
    {
        $product = Products::where('slug', $product_slug)->first();
        if ($product) {
            $category = Category::find($product->id_category);
            $item_colors_id = ProductColor::where(['product_id' => $product->id])->pluck('color_id')->toArray();
            $item_colors = Color::whereIn('id', $item_colors_id)->get();
            $product->colors = $item_colors;
            $upsell_list = Upsell::where('id_product', $product->id)->select('qty', 'new_price')->get();
            $product->upsell_list = $upsell_list;
            $combos = Combo::all();
            $list_combo_pdt_id = [];
            foreach ($combos as $item) {
                $pdt_id = json_decode($item->products_id);
                if (in_array($product->id, $pdt_id)) {
                    foreach ($pdt_id as $i) {
                        $list_combo_pdt_id[] = $i;
                    }
                }
            }
            $unique = (array_unique($list_combo_pdt_id));
            $list_combo = Products::whereIn('id', $unique)->select('id','name','price','slug')->get();
            foreach($list_combo as $item)
            {
                $image = ProductImage::where('id_product',$item->id)->first();
                if($image)
                {
                    $item->image = $image->link;
                }
                else
                {
                    $item->image = "undefinded";
                }
            }
            $product->list_combo = $list_combo;
            if ($category) {
                $product->category_name = $category->name;
            } else {
                $product->category_name = "Category Not Found";
            }
            return response()->json([
                'message' => 'Success',
                'product' => $product
            ], 200);
        }
        return response()->json([
            'message' => 'This product is not found',
        ], 404);
    }

    public function getProductsBySize(Request $request, $size_id)
    {
        $size = Size::find($size_id);
        if ($size) {
            $list_pdt = ProductSize::where('size_id', $size_id)->pluck('product_id')->toArray();
            if (count($list_pdt) > 0) {
                $products_list = Products::whereIn('id', $list_pdt);
                $page = $request->query('page') ? $request->query('page') : 1;
                $number = $products_list->count();
                $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
                $previousPage = ($page == 1) ? 1 : ($page - 1);
                $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
                $listPages = Pagination::initArray($page, $totalPage);
                $products_list = $products_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
                $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
                $currUrl = $fullUrl[0];
                foreach ($products_list as $item) {
                    $item_colors_id = ProductColor::where(['product_id' => $item->id])->pluck('color_id')->toArray();
                    $item_colors = Color::whereIn('id', $item_colors_id)->get();
                    $item->colors = $item_colors;
                }
            } else {
                $products_list = [];
            }
            return response()->json([
                'message' => 'Success',
                'products_list' => $products_list,
                'currUrl' => $currUrl,
                'totalPage' => $totalPage,
                'previousPage' => $previousPage,
                'nextPage' => $nextPage,
                'listPages' => $listPages,
                'currPage' => $page,
            ], 200);
        }
        return response()->json([
            'message' => 'This Size is not right',
        ], 404);
    }

    public function getProductsByColor(Request $request, $color_id)
    {
        $color = Size::find($color_id);
        if ($color) {
            $list_pdt = ProductColor::where('color_id', $color_id)->pluck('product_id')->toArray();
            if (count($list_pdt) > 0) {
                $products_list = Products::whereIn('id', $list_pdt);
                $page = $request->query('page') ? $request->query('page') : 1;
                $number = $products_list->count();
                $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
                $previousPage = ($page == 1) ? 1 : ($page - 1);
                $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
                $listPages = Pagination::initArray($page, $totalPage);
                $products_list = $products_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
                $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
                $currUrl = $fullUrl[0];
                foreach ($products_list as $item) {
                    $item_colors_id = ProductColor::where(['product_id' => $item->id])->pluck('color_id')->toArray();
                    $item_colors = Color::whereIn('id', $item_colors_id)->get();
                    $item->colors = $item_colors;
                }
            } else {
                $products_list = [];
            }
            return response()->json([
                'message' => 'Success',
                'products_list' => $products_list,
                'currUrl' => $currUrl,
                'totalPage' => $totalPage,
                'previousPage' => $previousPage,
                'nextPage' => $nextPage,
                'listPages' => $listPages,
                'currPage' => $page,
            ], 200);
        }
        return response()->json([
            'message' => 'This Color is not right',
        ], 404);
    }

    public function getProductsByPrice(Request $request)
    {
        $min = $request->min;
        $max = $request->max;
        if (is_numeric($min) & is_numeric($max)) {
            $products_list = Products::whereBetween('price', [$min, $max]);
            $page = $request->query('page') ? $request->query('page') : 1;
            $number = $products_list->count();
            $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
            $previousPage = ($page == 1) ? 1 : ($page - 1);
            $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
            $listPages = Pagination::initArray($page, $totalPage);
            $products_list = $products_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
            $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
            $currUrl = $fullUrl[0];
            foreach ($products_list as $item) {
                $item_colors_id = ProductColor::where(['product_id' => $item->id])->pluck('color_id')->toArray();
                $item_colors = Color::whereIn('id', $item_colors_id)->get();
                $item->colors = $item_colors;
            }
            return response()->json([
                'message' => 'Success',
                'products_list' => $products_list,
                'currUrl' => $currUrl,
                'totalPage' => $totalPage,
                'previousPage' => $previousPage,
                'nextPage' => $nextPage,
                'listPages' => $listPages,
                'currPage' => $page,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Value Is Not Right',
            ], 404);
        }
    }
    public function getAllProducts(Request $request)
    {
        $category_slug = $request->category_slug;
        $size_id = $request->size_id;
        $color_id = $request->color_id;
        $min = $request->min_price;
        $max = $request->max_price;
        $products_list = Products::where([]);
        if ($category_slug) {
            $category = Category::where('slug', $category_slug)->first();
            if ($category) {
                $products_list->where('id_category', $category->id);
            }
        }
        if ($size_id) {
            $size = Size::find($size_id);
            if ($size) {
                $list_pdt = ProductSize::where('size_id', $size_id)->pluck('product_id')->toArray();
                $products_list->whereIn('id', $list_pdt);
            }
        }
        if ($color_id) {
            $color = Size::find($color_id);
            if ($color) {
                $list_pdt = ProductColor::where('color_id', $color_id)->pluck('product_id')->toArray();
                $products_list->whereIn('id', $list_pdt);
            }
        }
        if (isset($min) & isset($max)) {
            $products_list->whereBetween('price', [$min, $max]);
        } else {
            if ($min) {
                $products_list->where('price', '>', $min);
            }
            if ($max) {
                $products_list->where('price', '<', $max);
            }
        }
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $products_list->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $products_list = $products_list->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        foreach ($products_list as $item) {
            $item_colors_id = ProductColor::where(['product_id' => $item->id])->pluck('color_id')->toArray();
            $item_colors = Color::whereIn('id', $item_colors_id)->get();
            $item->colors = $item_colors;
            $category = Category::find($item->id_category);
            if ($category) {
                $item->category_name = $category->name;
            } else {
                $item->category_name = "Category undefinded";
            }
        }
        return response()->json([
            'message' => 'Success',
            'products_list' => $products_list,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ], 200);
    }
    public function getAllSizes()
    {
        $sizes = Size::where([])->select('id', 'size')->get();
        return response()->json([
            'message' => 'Success',
            'sizes' => $sizes,
        ], 200);
    }

    public function getAllColors()
    {
        $colors = Color::where([])->select('id', 'color')->get();
        return response()->json([
            'message' => 'Success',
            'colors' => $colors,
        ], 200);
    }

    public function getAllCategories()
    {
        $categories = Category::where([])->select('id', 'name', 'slug')->get();
        return response()->json([
            'message' => 'Success',
            'categories' => $categories,
        ], 200);
    }
}
