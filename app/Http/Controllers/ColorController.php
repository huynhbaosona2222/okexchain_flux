<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        $color_list = Color::all();
        return view('color.index')->with(['color_list' => $color_list]);
    }

    public function createColor(Request $request)
    {
        $color = Color::where('color',$request->color)->first();
        if($color)
        {
            return back()->with('error', 'This Color Exists');
        }
        Color::create([
            'color' => $request->color
        ]);
        return back()->with('success', 'Add color successfully');
    }

    public function editColor($id)
    {
        $color = Color::find($id);
        $color_list = Color::all();
        if ($color) {
            return view('color.index')->with(['color' => $color, 'color_list' => $color_list]);
        } else {
            return redirect('/admin/products/colors')->with('error', 'Color Is Not Exists');
        }
    }

    public function updateColor(Request $request, $id)
    {
        $color = Color::where('color',$request->color)->first();
        if($color)
        {
            return back()->with('error', 'This Color Exists');
        }
        $color = Color::find($id);
        if ($color) {
            if (isset($color)) {
                $color->color = $request->color;
            }
            $color->save();
            session()->flash('success', 'Color Update Successfully');
        } else {
            session()->flash('error', 'Color Update Failed');
        }
        return redirect('/admin/products/colors');
    }

    public function deleteColor($id)
    {
        $color = Color::find($id);
        if ($color) {
            $color->delete();
            session()->flash('success', 'Delete Color Successfully');
        } else {
            session()->flash('error', 'This Color Is Not Found');
        }
        return redirect('/admin/products/colors');
    }
}
