<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use App\Helpers\AmazonS3;
use App\Products;
use Illuminate\Http\Request;
use App\Helpers\Pagination;
use App\ProductColor;
use App\ProductImage;
use App\ProductSize;
use App\ProductsTags;
use App\Size;
use App\Tags;
use App\Upsell;

class ProductController extends Controller
{
    private $MAX_VALUE = 10;

    function createSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        // Lowercase
        if ($options['lowercase']) {
            $str = mb_strtolower($str, 'UTF-8');
        }

        $char_map = array(
            // Latin
            'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'đ' => 'd', 'é' => 'e', 'è' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $str . '-' . time();
    }

    public function index(Request $request)
    {
        $products = Products::where([]);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $products->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $products = $products->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        // dd($products);
        foreach ($products as $product) {
            $category = Category::find($product->id_category);
            if ($category) {
                $product->category_name = $category->name;
                $product->category_id = $category->id;
            }
        }
        return view('product.index')->with([
            'products' => $products,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function createProduct()
    {
        $categories = Category::all();
        $colors = Color::all();
        $tags = Tags::all();
        return view('product.create_product')->with([
            'categories' => $categories,
            'tags' => $tags,
            'colors' => $colors
        ]);
    }

    public function postcreateProduct(Request $request)
    {
        $size_default = ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL'];
        $size = strtoupper($request->size);
        $size_arr = explode(',', $size);
        foreach ($size_arr as $key => $val) {
            $size_arr[$key] = trim($val);
            if (!in_array(trim($val), $size_default)) {
                unset($size_arr[$key]);
            }
        }
        $size_id = Size::whereIn('size', $size_arr)->pluck('id')->toArray();
        if (isset($request->new_tags)) {
            $tagNames = explode(',', $request->get('new_tags'));
            $tagIds = [];
            foreach ($tagNames as $tagName) {
                $tag = Tags::firstOrCreate([
                    'name' => trim($tagName),
                ]);
                if ($tag) {
                    $tag->slug = $this->createSlug($tagName);
                    $tag->save();
                    $tagIds[] = $tag->id;
                }
            }
        }
        $new_product = Products::create([
            'name' => $request->name,
            'price' => $request->price,
            'qty_available' => $request->qty_available,
            'id_category' => $request->category,
            'description' => $request->description,
            'slug' => $this->createSlug($request->name),
        ]);
        if ($request->tags) {
            $new_product->tags()->attach($request->tags);
        }
        if (isset($tagNames)) {
            $new_product->tags()->attach($tagIds);
        }
        if (count($size_id) > 0) {
            foreach ($size_id as $item) {
                ProductSize::create([
                    'product_id' => $new_product->id,
                    'size_id' => $item,
                ]);
            }
        }
        if ($request->images) {
            foreach ($request->images as $item) {
                ProductImage::create([
                    'id_product' => $new_product->id,
                    'link' => AmazonS3::uploadRandom($item),
                ]);
            }
        }
        if ($request->color) {
            foreach ($request->color as $item) {
                ProductColor::create([
                    'product_id' => $new_product->id,
                    'color_id' => $item,
                ]);
            }
        }

        session()->flash('success', 'Create Product Successfully');
        return redirect('/admin/products');
    }

    public function editProduct($id)
    {
        $tags = Tags::all();
        $colors = Color::all();
        $product = Products::find($id);
        if ($product) {
            $category = Category::all();
            $upsell = Upsell::where('id_product', $id)->get();
            $list_pdt_tags = ProductsTags::where('products_id', $product->id)->pluck('tags_id')->toArray();
            $list_tags_name = Tags::whereIn('id', $list_pdt_tags)->pluck('name')->toArray();
            $list_size_id = ProductSize::where('product_id', $product->id)->pluck('size_id')->toArray();
            $list_size_name = Size::whereIn('id', $list_size_id)->pluck('size')->toArray();
            $sizes_name = implode(',', $list_size_name);
            $tags_name = implode(',', $list_tags_name);
            foreach ($upsell as $item) {
                $item->price_discount = $item->new_price * $product->price / 100;
            }
            $images = ProductImage::where('id_product', $id)->get();
            $product->tags_name = $tags_name;
            $product->sizes_name = $sizes_name;
            $active_color = ProductColor::where('product_id', $product->id)->pluck('color_id')->toArray();
            return view('product.create_product')->with([
                'categories' => $category,
                'product' => $product,
                'tags' => $tags,
                'project_images' => $images,
                'upsell' => $upsell,
                'colors' => $colors,
                'active_color' => $active_color

            ]);
        }
        session()->flash('error', 'This Product is not Found');
        return redirect('/admin/products');
    }

    public function updateProduct(Request $request, $id)
    {
        $product = Products::find($id);
        if ($product) {
            $size_default = ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL'];
            $current_size = ProductSize::where('product_id', $product->id)->get();
            if (count($current_size) > 0) {
                foreach ($current_size as $item) {
                    $item->delete();
                }
            }
            $current_color = ProductColor::where('product_id', $product->id)->get();
            if (count($current_color) > 0) {
                foreach ($current_color as $item) {
                    $item->delete();
                }
            }
            if ($request->size) {
                $size = strtoupper($request->size);
                $size_arr = explode(',', $size);
                foreach ($size_arr as $key => $val) {
                    $size_arr[$key] = trim($val);
                    if (!in_array(trim($val), $size_default)) {
                        unset($size_arr[$key]);
                    }
                }
                $size_id = Size::whereIn('size', $size_arr)->pluck('id')->toArray();
                if (count($size_id) > 0) {
                    foreach ($size_id as $item) {
                        ProductSize::create([
                            'product_id' => $id,
                            'size_id' => $item,
                        ]);
                    }
                }
            }
            if ($request->color) {
                foreach ($request->color as $item) {
                    ProductColor::create([
                        'product_id' => $product->id,
                        'color_id' => $item,
                    ]);
                }
            }
            if (isset($request->new_tags)) {
                $tagNames = explode(',', $request->get('new_tags'));
                $tagIds = [];
                foreach ($tagNames as $tagName) {
                    $tag = Tags::firstOrCreate([
                        'name' => trim($tagName),
                    ]);
                    $tag->slug = $this->createSlug($tagName);
                    $tag->save();
                    if ($tag) {
                        $tagIds[] = $tag->id;
                    }
                }
            }
            $product->name = $request->name;
            $product->id_category = $request->category;
            $product->price = $request->price;
            $product->qty_available = $request->qty_available;
            $product->description = $request->description;
            $product->slug = $this->createSlug($request->name);
            ProductsTags::where('products_id', $product->id)->delete();
            if ($request->tags) {
                $product->tags()->attach($request->tags);
            }
            if (isset($tagNames)) {
                $product->tags()->attach($tagIds);
            }
            $product->save();
            session()->flash('success', 'Update Product Successfully');
            return back();
        }
        session()->flash('error', 'This Product is not found');
        return back();
    }

    public function updateProductImage(Request $request, $id)
    {
        if ($request->images) {
            foreach ($request->images as $item) {
                ProductImage::create([
                    'id_product' => $request->id_project,
                    'link' => AmazonS3::uploadRandom($item),
                ]);
            }
            session()->flash('success', 'Update Image Successfully');
            return back();
        }
        $image = ProductImage::find($id);
        if ($image) {
            AmazonS3::delete($image->link);
            $image->link = AmazonS3::uploadRandom($request->link);
            $image->save();
            session()->flash('success', 'Update Image Successfully');
            return back();
        }
        session()->flash('error', 'This image is not Found');
        return redirect('/admin/products');
    }

    public function deleteProductImage(Request $request, $id)
    {
        $image = ProductImage::find($id);
        if ($image) {
            AmazonS3::delete($image->link);
            $image->delete();
            session()->flash('success', 'Update Image Successfully');
            return back();
        }
        session()->flash('error', 'This image is not Found');
        return redirect('/admin/products');
    }

    public function deleteProduct($id)
    {
        $product = Products::find($id);
        if ($product) {
            $list_image = ProductImage::where('id_product', $product->id)->get();
            if ($list_image) {
                foreach ($list_image as $item) {
                    AmazonS3::delete($item->link);
                    $item->delete();
                }
            }
            $list_image = Upsell::where('id_product', $product->id)->delete();
            $list_pdt_tags = ProductsTags::where('products_id', $product->id)->get();

            if (count($list_pdt_tags > 0)) {
                foreach ($list_pdt_tags as $item) {
                    $item->delete();
                }
            }
            $product->delete();
            return back()->with('success', 'Delete Product Successfully');
        }
        return back()->with('error', 'This Product Is Not Found');
    }

    public function createProductUpsell(Request $request, $id)
    {
        if ($request->qty < 2) {
            return back()->with('error', 'Quantity Must Be >= 2');
        }
        $product = Products::find($id);
        if ($product) {
            Upsell::firstOrCreate([
                'id_product' => $id,
                'qty' => $request->qty,
                'new_price' => $request->new_price,
            ]);
            return back()->with('success', 'Update Product Successfully');
        }
        return back()->with('error', 'This Product Is Not Found');
    }

    public function updateProductUpsell(Request $request, $id)
    {
        if ($request->qty < 2) {
            return back()->with('error', 'Quantity Must Be >= 2');
        }
        $upsell = Upsell::find($id);
        if ($upsell) {
            $upsell->qty = $request->qty;
            $upsell->new_price = $request->new_price;
            $upsell->save();
            return back()->with('success', 'Update Upsell Successfully');
        }
        return back()->with('error', 'This Option Is Not Found');
    }

    public function getProductByCategory(Request $request, $id)
    {
        $category = Category::find($id);
        if (!$category) {
            return back()->with('error', 'This Category Is Not Found');
        }
        $products = Products::where('id_category', $id);
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $products->count();
        $totalPage = (int) ($number / $this->MAX_VALUE) + (($number % $this->MAX_VALUE) !== 0);
        $previousPage = ($page == 1) ? 1 : ($page - 1);
        $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
        $listPages = Pagination::initArray($page, $totalPage);
        $products = $products->orderBy('created_at', 'DESC')->skip($this->MAX_VALUE * ($page - 1))->take($this->MAX_VALUE)->get();
        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
        $currUrl = $fullUrl[0];
        // dd($products);
        foreach ($products as $product) {
            $category = Category::find($product->id_category);
            if ($category) {
                $product->category_name = $category->name;
                $product->category_id = $category->id;
            }
        }
        return view('product.index')->with([
            'products' => $products,
            'currUrl' => $currUrl,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
        ]);
    }

    public function deleteProductUpsell($id)
    {
        $option_upsell = Upsell::find($id);
        if (!$option_upsell) {
            return back()->with('error', 'This Option Is Not Found');
        }
        $option_upsell->delete();
        return back()->with('session', 'Delete Option Successfully');
    }
}
