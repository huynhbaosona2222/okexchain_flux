<?php

namespace App\Http\Controllers;

use App\ProductsTags;
use App\Tags;
use Illuminate\Http\Request;

class TagController extends Controller
{

    function createSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        // Lowercase
        if ($options['lowercase']) {
            $str = mb_strtolower($str, 'UTF-8');
        }

        $char_map = array(
            // Latin
            'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'đ' => 'd', 'é' => 'e', 'è' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $str . '-' . time();
        // return $slug . '-' . time();
    }


    public function index()
    {
        $tag_list = Tags::all();
        return view('product.tags')->with(['tag_list' => $tag_list]);
    }

    public function deleteTag($id)
    {
        $tag = Tags::find($id);
        if ($tag) {
            $pdt_tags = ProductsTags::where('tags_id', $tag->id)->get();
            if (count($pdt_tags) > 0) {
                foreach ($pdt_tags as $item) {
                    $item->delete();
                }
            }
            $tag->delete();
            session()->flash('success', 'Delete Tag Successfully');
        }else{
            session()->flash('error', 'This Tag Is Not Found');
        }
        return redirect('/admin/products/tags');
    }

    // public function createTag(Request $request) {
    //     $tag = Tags::where('name', $request->input('tag_name'))->first();
    //     if ($tag) {
    //         session()->flash('warning','Tag Already Exists');
    //     } else {
    //         Tags::create([
    //             'name'=>$request->tag_name,
    //             'slug'=>$this->createSlug($request->tag_name),
    //         ]);
    //         session()->flash('success','Tag Create Successfully');
    //     }
    //     return redirect('/admin/products/tags');
    // }

    public function editTag($id)
    {
        $tag = Tags::find($id);
        $tag_list = Tags::all();
        if ($tag) {
            return view('product.tags')->with(['tag' => $tag, 'tag_list' => $tag_list]);
        } else {
            session()->flash('error', 'Tag Is Not Exists');
            return redirect('/admin/products/tags');
        }
    }

    public function updateTag(Request $request)
    {
        $id = $request->input('id');
        $name = $request->input('tag_name');
        $tag = Tags::find($id);
        if ($tag) {
            if (isset($name)) {
                $tag->name = $name;
                $tag->slug = $this->createSlug($name);
            }
            $tag->save();
            session()->flash('success', 'Tag Update Successfully');
        } else {
            session()->flash('error', 'Tag Update Failed');
        }
        return redirect('/admin/products/tags');
    }
}
