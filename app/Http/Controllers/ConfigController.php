<?php

namespace App\Http\Controllers;

use App\Config;
use App\Helpers\Pagination;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    private $limitPerPage = 100;

    public function index(Request $request)
    {
        $configs=Config::all();
        $page = $request->query('page') ? $request->query('page') : 1;
        $number = $configs->count();
            $totalPage = (int)($number / $this->limitPerPage) + (($number % $this->limitPerPage) !== 0);
            $previousPage = ($page == 1) ? 1 : ($page - 1);
            $nextPage = ($page == $totalPage) ? $totalPage : ($page + 1);
            $listPages = Pagination::initArray($page, $totalPage);
            $configs = Config::skip($this->limitPerPage * ($page - 1))->take($this->limitPerPage)->get();
        return view('config.list_conf')->with([
            'configs'=>$configs,
            'totalPage' => $totalPage,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'listPages' => $listPages,
            'currPage' => $page,
            'limitPerPage' => $this->limitPerPage
        ]);
    }

    public function update(Request $request,$id)
    {
        $value=$request->value;
        $config=Config::find($id);
        if(!$config)
        {
            return back()->with('error','Config is not found');
        }
        if($config->id==1)
        {
            if($value < 60 or $value > 1200 )
            {
                return back()->with('error','Config value is wrong format');
            }
        }
        $config->value=$value;
        $config->save();
        return back()->with('success','Update config successfully');
    }
}
