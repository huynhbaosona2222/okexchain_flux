<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoBills extends Model
{
    protected $fillable = [
        'id_bill','id_user','created_at'
    ];
}
