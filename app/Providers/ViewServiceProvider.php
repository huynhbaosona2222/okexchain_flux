<?php

namespace App\Providers;

use App\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Role;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {

    }
    public function boot()
    {
        View::composer('*', function ($view) {
            $user = Auth::user();
            if ($user) {
                $role = Role::where('id', $user->role_id)->first();
                if ($role) {
                    $user->role_name = $role->name;
                }
                $view->with([
                    'user' => $user,
                ]);
            }
        });
    }
}
