<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application_staging', 'chillax');

// Project repository
set('repository', 'git@bitbucket.org:pixiostudiovn/chillax.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Default Branch
set('branch', 'master');

set('default_timeout', null);

set('laravel_version', function () {
    $result = run('cd {{release_path}} && {{bin/php}} artisan --version');

    preg_match_all('/(\d+\.?)+/', $result, $matches);

    $version = $matches[0][0] ?? 5.5;

    return $version;
});

// Shared files/dirs between deploys
add('shared_files', [
  '.env',
  '.htaccess',
]);

add('shared_dirs', [
  'storage',
  'bootstrap/cache',
]);

add('writable_dirs', [
  'bootstrap/cache',
  'storage',
  'storage/app',
  'storage/app/public',
  'storage/framework',
  'storage/framework/cache',
  'storage/framework/sessions',
  'storage/framework/views',
  'storage/logs',
]);

// List of hosts
host('development')
  ->hostname('149.28.150.80')
  ->user('spyets')
  ->stage('development')
  ->set('deploy_path', '~/public_html/{{application_staging}}')
  ->set('http_user', 'spyets')
  ->set('writable_mode', 'chmod')
  ->set('branch', 'develop')
  ->forwardAgent(false);

host('production')
  ->hostname('149.28.150.80')
  ->user('spyets')
  ->stage('production')
  ->set('deploy_path', '~/public_html/{{application_staging}}')
  ->set('http_user', 'spyets')
  ->set('writable_mode', 'chmod')
  ->set('branch', 'master')
  ->forwardAgent(false);


/**
 * Helper tasks
 */

// laravel version
desc('Show laravel version');
task('laravel:version', function () {
    $output="{{laravel_version}}";
    writeln('<info>' . $output . '</info>');
})->once();

// build assets npm
desc('NPM install and Build Assets');
task('npm:build:assets', function () {
  run('cd {{release_path}} && npm install');
  run('cd {{release_path}} && npm run prod');
})->once();

// artisan migrate
desc('Execute artisan migrate');
task('artisan:migrate', function () {
    run('{{bin/php}} {{release_path}}/artisan migrate --force');
})->once();

// seed database
desc('Execute artisan db:seed');
task('artisan:db:seed', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan db:seed --force');
    writeln('<info>' . $output . '</info>');
});

// queue restart
desc('Execute artisan queue:restart');
task('artisan:queue:restart', function () {
    run('{{bin/php}} {{release_path}}/artisan queue:restart');
});

// clear cache
desc('Execute artisan optimize:clear');
task('artisan:optimize:clear', function () {
    $needsVersion = 5.7;
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        $output = run('{{bin/php}} {{release_path}}/artisan optimize:clear');
        writeln('<info>' . $output . '</info>');
    }
});

task('deploy', [
  // outputs the branch and IP address to the command line
  'deploy:info',
  // preps the environment for deploy, creating release and shared directories
  'deploy:prepare',
  // adds a .lock file to the file structure to prevent numerous deploys executing at once
  'deploy:lock',
  // removes outdated release directories and creates a new release directory for deploy
  'deploy:release',
  // clones the project Git repository
  'deploy:update_code',
  // loops around t he list of shared directories defined in the config file
  // and generates symlinks for each
  'deploy:shared',
  // loops around the list of writable directories defined in the config file
  // and changes the owner and permissions of each file or directory
  'deploy:writable',

  'npm:build:assets',
  // if Composer is used on the site, the Composer install command is executed
  'deploy:vendors',
  // npm build assets
  // 'npm:build:assets',
  'artisan:migrate',
  // clear cache
  'artisan:optimize:clear',
  // migrate database
  // seed database
  // 'artisan:db:seed',
  // loops around release and removes unwanted directories and files
  'deploy:clear_paths',
  // links the deployed release to the "current" symlink
  'deploy:symlink',
  // deletes the unlock file, allowing further deploys to be executed
  'deploy:unlock',
  // loops around a list of release directories and removes any which are now outdated
  'cleanup',
  // can be used by the user to assign custom tasks to execute on successful deployments
  'success',
]);

after('deploy:failed', 'deploy:unlock');
