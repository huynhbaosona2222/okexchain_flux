<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('index');
});
Route::get('/shop', function() {
    return view('shop');
});
Route::get('/shop-item', function() {
    return view('shop-item');
});
Route::get('/checkout', function() {
    return view('checkout');
});
Route::get('/cart', function() {
    return view('cart');
});
Route::get('/about', function() {
    return view('about');
});
Route::get('/contact', function() {
    return view('contact');
});
Route::get('/account', function() {
    return view('account');
});

Route::group([
    'middleware' => 'auth'
], function() {
    Route::get('/dashboard', 'ViewController@index')->name('dashboard');
    Route::group([
        // 'middleware' => 'admin',
        'prefix' => 'admin'
    ], function () {
        // Route::get('/', 'UserController@getProfile');
        Route::get('/profile', 'UserController@getProfile');
        Route::get('/member', 'UserController@getMemberPage')->name('member');
        Route::get('/delete-member/{id}', 'UserController@deleteMember');
        Route::get('/edit-member/{id}', 'UserController@editMember')->name('member');
        Route::post('/profile/update', 'UserController@updateProfile');
        Route::post('/profile/update-password', 'UserController@updatePassword');
        Route::post('/add-member', 'UserController@addNewMember');
        Route::post('/update-member/{id}', 'UserController@updateMember');
        Route::group([
            'prefix' => 'products'
        ], function () {
            Route::get('/', 'ProductController@index')->name('products');
            Route::post('/create', 'ProductController@postcreateProduct')->name('products');
            Route::get('/create', 'ProductController@createProduct')->name('products');;
            Route::get('/edit/{id}', 'ProductController@editProduct')->name('products');
            Route::get('/delete/{id}', 'ProductController@deleteProduct');
            Route::post('/update/{id}', 'ProductController@updateProduct');
            Route::post('/edit-image/{id}', 'ProductController@updateProductImage');
            Route::get('/delete-image/{id}', 'ProductController@deleteProductImage');
            Route::post('/upsell/{id}', 'ProductController@createProductUpsell');
            Route::get('/upsell/delete/{id}', 'ProductController@deleteProductUpsell');
            Route::post('/upsell/update/{id}', 'ProductController@updateProductUpsell');
            Route::get('/category/{id}', 'ProductController@getProductByCategory')->name('products');
            Route::get('/tag/{id}', 'ProductController@getProductByTag');

            Route::get('/combos', 'ComboController@index')->name('combos');
            Route::get('/combos/create', 'ComboController@createCombo')->name('combos');
            Route::get('/combos/delete/{id}', 'ComboController@deleteCombo');
            Route::get('/combos/edit/{id}', 'ComboController@editCombo')->name('combos');
            Route::post('/combos/create', 'ComboController@postcreateCombo');
            Route::post('/combos/update/{id}', 'ComboController@updateCombo');
            Route::post('/combos/edit-image/{id}', 'ComboController@updateComboImage');

            Route::get('/categories', 'CategoryController@index')->name('categories');
            Route::post('/categories/create', 'CategoryController@createCategory');
            Route::get('/categories/edit/{id}', 'CategoryController@editCategory')->name('categories');
            Route::get('/categories/delete/{id}', 'CategoryController@deleteCategory');
            Route::post('/categories/update', 'CategoryController@updateCategory');

            Route::get('/tags', 'TagController@index')->name('tags');
            // Route::post('/tags/create', 'TagController@createTag');
            Route::get('/tags/edit/{id}', 'TagController@editTag')->name('tags');
            Route::get('/tags/delete/{id}', 'TagController@deleteTag');
            Route::post('/tags/update', 'TagController@updateTag');

            Route::get('/promotions', 'PromotionController@index')->name('promo');
            Route::get('/promotions/create', 'PromotionController@createPromotion')->name('promo');
            Route::get('/promotions/edit/{id}', 'PromotionController@editPromotion')->name('promo');
            Route::post('/promotions/create', 'PromotionController@postcreatePromotion');
            Route::post('/promotions/update/{id}', 'PromotionController@updatePromotion');
            Route::get('/promotions/delete/{id}', 'PromotionController@deletePromotion');

            Route::get('/colors', 'ColorController@index')->name('color');
            Route::post('/colors/create', 'ColorController@createColor');
            Route::get('/colors/edit/{id}', 'ColorController@editColor');
            Route::get('/colors/delete/{id}', 'ColorController@deleteColor');
            Route::post('/colors/edit/{id}', 'ColorController@updateColor');
        });

        Route::get('/address', 'AddressController@index')->name('address');
        Route::get('/address/edit/{id}', 'AddressController@editAddress')->name('address');
        Route::get('/address/create', 'AddressController@createAddress')->name('address'); //demo feature
        Route::post('/address/create', 'AddressController@postcreateAddress')->name('address'); //demo feature
        Route::post('/address/update/{id}', 'AddressController@updateAddress')->name('address');
        Route::get('/address/delete/{id}', 'AddressController@deleteAddress')->name('address');
        Route::get('/address/get-states/{id}', 'AddressController@getStates');
        Route::get('/address/get-wards/{id}', 'AddressController@getWards');

        Route::get('/customer', 'AddressController@index')->name('address');
        Route::get('/customer/{id}', 'AddressController@getCustomerDetail')->name('address');

        Route::get('/countdown', 'AddressController@getCountDown')->name('countdown');
        Route::get('/demo-cart', 'AddressController@getDemoCart')->name('countdown');
        Route::get('/expired', 'AddressController@ComeBack')->name('countdown');

        Route::get('/configs', 'ConfigController@index')->name('config');
        Route::post('/configs/update/{id}', 'ConfigController@update')->name('config');

        Route::get('/deliveries', 'DeliveryController@index')->name('delivery');
        Route::post('/deliveries/create', 'DeliveryController@createDelivery')->name('delivery');
        Route::get('/deliveries/edit/{id}', 'DeliveryController@editDelivery')->name('delivery');
        Route::post('/deliveries/update/{id}', 'DeliveryController@updateDelivery')->name('delivery');
        Route::get('/deliveries/delete/{id}', 'DeliveryController@deleteDelivery')->name('delivery');
        Route::get('/deliveries/active/{id}', 'DeliveryController@activeDelivery')->name('delivery');

        Route::get('/order/{id}', 'OrderController@getOrderDetail');



        Route::get('/demo/bills', 'AddressController@update')->name('config');

        Route::post(
            '/payment',
            function () {
                return redirect('/admin/demo-cart')->with('success', 'Payment Complete!! ');
            }
        );
    });
});
Route::get('/demo/gethour', 'AddressController@gethour')->name('config');
Route::get('/demo/add-size', 'AddressController@addSize')->name('config');

Route::get('/pdt/find', 'ComboController@findPdt');
Route::post('/user-register', 'UserController@register');
Route::post('/user-login', 'UserController@login');
Route::get('/forgot-password', 'UserController@forgotPasswordView');
Route::post('/forgot-password', 'UserController@forgotPassword');
Route::get('/logout', 'UserController@logout');
Auth::routes();


Route::get('/countdown', 'BlockchainController@index');
Route::get('/ttt', 'BlockchainController@ttt');


