<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'API\AuthController@login'); //ok
    Route::post('/signup', 'API\AuthController@signup'); //ok
    Route::post('/forget-password', 'API\AuthController@postForgetPassword');
    Route::post('/reset-password', 'API\AuthController@postResetPassword');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/logout', 'API\AuthController@logout'); //ok
        Route::get('/profile', 'API\AuthController@user');
        Route::put('/profile/update', 'Api\AuthController@updateInfo');

        Route::get('/demo-verify-sms', 'Api\AuthController@sendOTP');
        Route::post('/submit-otp-sms', 'Api\AuthController@submitOTP');
    });
});
Route::post('/order-detail', 'API\OrderController@getOrderDetail'); //ok

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/customer-orders', 'API\OrderController@getCustomerOrder'); //ok
});

Route::get('/demoSendCode', 'API\OrderController@demoSendCode');

Route::get('/sizes', 'API\HomeController@getAllSizes'); //ok
Route::get('/colors', 'API\HomeController@getAllColors');//ok
Route::get('/categories', 'API\HomeController@getAllCategories'); //ok


Route::get('/products', 'API\HomeController@getAllProducts');
Route::get('/products/category/{category_slug}', 'API\HomeController@getProductsByCategory'); //ok
Route::get('/products/detail/{product_slug}', 'API\HomeController@getProductsDetail'); //ok

Route::get('/products/size/{size_id}', 'API\HomeController@getProductsBySize'); //ok
Route::get('/products/color/{color_id}', 'API\HomeController@getProductsByColor'); //ok
Route::get('/products/price', 'API\HomeController@getProductsByPrice'); //ok
