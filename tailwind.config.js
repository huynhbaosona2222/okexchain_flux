module.exports = {
  theme: {
    screens: {
      xxl: { min: "1441px" },
      xl: { max: "1440px" },
      lg: { max: "1279px" },
      md2: { max: "1023px" },
      md: { max: "767px" },
      sm: { max: "639px" }
    },
    extend: {
      colors: {
        "c-red": {
          "100": "#FF492E"
        },
        "c-orange": {
          "100": "#F6E5DC",
          "200": "#D47C61"
        },
        "c-black": {
          "100": "#111010",
          "200": "#727272"
        },
        "c-blue": {
          "100": "#11478F",
          "200": "#146AA8"
        },
        "c-yellow": {
          "100": "#FFE661"
        }
      },
      borderRadius: {
        "8": "2rem"
      },
      fontSize: {
        "c-12": "1.2rem",
        "c-14": "1.4rem",
        "c-18": "1.8rem",
        "c-20": "2rem",
        "c-26": "2.6rem",
        "c-30": "3rem",
        "c-40": "4rem",
        "c-80": "8rem",
        "c-120": "12rem"
      },
      lineHeight: {
        "c-17": "1.7rem",
        "c-20": "2rem",
        "c-22": "2.2rem",
        "c-25": "2.5rem",
        "c-30": "3rem",
        "c-32": "3.2rem",
        "c-34": "3.4rem",
        "c-36": "3.6rem",
        "c-38": "3.8rem",
        "c-50": "5rem",
        "c-100": "10rem",
        "c-150": "15rem"
      },
      spacing: {
        "120": "30rem",
        "160": "40rem",
        "240": "60rem",
        "p-12": "12%",
        "p-15": "15%"
      }
    }
  },
  variants: {},
  plugins: []
};
