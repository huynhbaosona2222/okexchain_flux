
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chillax | Register</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" href="https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
    <div style="padding-top: 0px; width: 60%; margin-left: 20%;" class="text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">
                    <img src="https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg"/>
                </h1>
            </div>
            <h3>Welcome to Chillax</h3>
            <p>Stay home! Wear Chillax!</p>
            <form class="m-t" role="form" action="/user-register" method="POST" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="col-lg-6">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="" id="password">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="" id="confirmpassword">
                    </div>
                    <div class="form-group">
                        <select class="form-control" required="" name="role">
                            @if (count($roles) > 0)
                                @foreach ($roles as $key => $role)
                                    <option value="{{$role->id}}" {{$key == 0 ? 'selected' : ''}}>{{$role->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" required=""/>
                    </div>
                    <div class="form-group">
                        <input type="file" name="ava_src" required />
                    </div>
                </div>
                <div class="form-group">
                    @if(session('error'))
                    <p style="color: #ed5565;">
                        {{session('error')}}
                    </p>
                    @endif
                </div>
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
                    <a href="/forgot-password"><small>Forgot password?</small></a>
                    <p class="text-muted text-center"><small>Already have an account?</small></p>
                    <a class="btn btn-sm btn-white btn-block" href="/login">Login</a>
                </div>
            </form>
        </div>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            var password = document.getElementById("password")
            var confirm_password = document.getElementById("confirmpassword");

            function validatePassword(){
              if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Confirm Password is not match");
              } else {
                confirm_password.setCustomValidity('');
              }
            }

            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;
        })
    </script>
</body>
</html>
