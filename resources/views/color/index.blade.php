@extends('layouts.app')
<title>Pixio Studio| Color</title>
@section('header')
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Colors</h2>
        @if(isset($color))
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li>
                <a href="/admin/products/colors">Colors</a>
            </li>
            <li class="active">
                <strong> Edit</strong>
            </li>
        </ol>
        @else
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Product</a>
            </li>
            <li class="active">
                <strong> colors</strong>
            </li>
        </ol>
        @endif
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="content">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item">
                    {{ $error }}
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ isset($color) ? "Edit Color":"Add New Color "}}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" action="{{ isset($color) ? "/admin/products/colors/edit/".$color->id :"/admin/products/colors/create"}}" class="form-horizontal">
                        @csrf
                        <div class="form-group"><label class="col-sm-2 control-label">Color</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control demo1" value="{{ isset($color) ? "$color->color":"#fafafa"}}" name="color" required />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">{{ isset($color) ? "Update Color":"Create Color"}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Colors List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                            <tr>
                                <th data-toggle="true"></th>
                                <th>Color</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            } else {
                                $page = 1;
                            }
                            ?>
                            @if(!isset ($page))
                            $page==1
                            @endif
                            @foreach($color_list as $color)
                            <tr>
                                <!-- <td>{{(($page-1) * 10) + $loop->index + 1}}</td> -->
                                <td class="project-title" style=" width:20%; background-image: linear-gradient(to right, {{$color->color}} , white);"></td>
                                <td><a href="/admin/products/color/{{$color->id}}">{{$color->color}} </a></td>
                                <td>
                                    <a href="/admin/products/colors/edit/{{$color->id}}" class="btn btn-info btn-sm">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger demo4" id="{{$color->id}}" data-id="{{$color->id}}">
                                        Delete
                                    </button>
                                </td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Color picker -->
<script src="/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="/js/plugins/clockpicker/clockpicker.js"></script>
<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script>
    $(document).ready(function() {

        $('.demo1').colorpicker();
        var divStyle = $('.back-change')[0].style;
        $('#demo_apidemo').colorpicker({
            color: divStyle.backgroundColor
        }).on('changeColor', function(ev) {
            divStyle.backgroundColor = ev.color.toHex();
        });

        $('.clockpicker').clockpicker();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this Color!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "This Color has been deleted.", "success");
                        window.location.href = '/admin/products/colors/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection