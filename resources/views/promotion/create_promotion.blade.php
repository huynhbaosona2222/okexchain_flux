@extends('layouts.app')
@section('header')
<title>Pixio Studio| Blogs</title>
<link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }

    .note-editor {
        border: 1px solid #a9a9a9 !important;
    }
</style>
@endsection
@section('title')
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            @if(isset($promotion))
            Edit Promotion
            @else
            Create Promotion
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li>
                <a href="/admin/products/promotions">Promotions</a>
            </li>
            <li class="active">
                <strong>
                    @if(isset($promotion))
                    Edit
                    @else
                    Create
                    @endif
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1">Promotion Info</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active ">
                        <div class="panel-body">
                            <form action="{{ isset($promotion) ? '/admin/products/promotions/update/'.$promotion->id: '/admin/products/promotions/create'}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="demo_hidden">
                                @csrf
                                <div class="form-group"><label class="col-sm-2 control-label">Promotion Code</label>

                                    <div class="col-sm-7"><input type="text" pattern=".{6,15}" required title="6 to 15 characters" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="name" id="rand" value="{{ isset($promotion) ? $promotion->name:   ''  }}" required>
                                    </div>
                                    <div class="col-sm-3">
                                        <a class="btn btn-primary" style="width:100%" onclick="random()">Generate Promotion Code </a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"> Discount</label>
                                    <div class="col-sm-10">
                                        <div class="input-group m-b">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" id="show_choose">
                                                    @if(isset($promotion))
                                                    {{$promotion->type ? "Price Discount (VND)" : "Percentage Discount (%)"}}
                                                    @else
                                                    Percentage Discount (%)
                                                    @endif
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li onclick="choosePercent()"><a href="#"> Percentage Discount (%) </a></li>
                                                    <li onclick="choosePrice()"><a href="#">Price Discount (VND)</a></li>
                                                </ul>
                                            </div>
                                            @if(isset($promotion))
                                            <input type="number" name="percent_discount" id="percent" max="100" style="display: {{$promotion->type ? "none" : "block"}}" class="form-control" value="{{ ($promotion->type) ? "":   $promotion->value  }}">
                                            <input type="number" name="price_discount" id="price" min="5000" max="10000000" class="form-control" style="display: {{$promotion->type ?  "block" : "none" }}" value="{{ ($promotion->type) ? $promotion->value:   ''  }}">
                                            @else
                                            <input type="number" name="percent_discount" id="percent" max="100" style="display: block " class="form-control" required>
                                            <input type="number" name="price_discount" id="price" min="5000" max="10000000" class="form-control" style="display: none">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="data_1"><label class="col-sm-2 control-label">Time Expired</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" value="{{ isset($promotion) ? $promotion->expire:   ''  }}" name="time" required>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Description (Optional)</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="description" id="detail" >{{ isset($promotion) ? $promotion->description:   ''  }}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" id="submit-form-btn" onclick="submitForm('#demo_hidden', '#submit-form-btn')" type="submit">
                                            @if(isset($promotion))
                                            Edit promotion
                                            @else
                                            Create promotion
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script> -->
<script>
    function random() {
        let r = Math.random().toString(36).substring(4).toUpperCase();
        document.getElementById("rand").value = "PRO" + r;
    }
</script>
<script>
    function choosePrice() {
        document.getElementById("show_choose").innerHTML = "Price Discount (VND)";
        document.getElementById("price").style.display = "block";
        document.getElementById("percent").style.display = "none";
        document.getElementById("percent").value = null;
        document.getElementById("price").attributes["required"] = "";
        document.getElementById("price").required = true;
        document.getElementById("percent").required = false;
    }

    function choosePercent() {
        document.getElementById("show_choose").innerHTML = "Percentage Discount (%)";
        document.getElementById("price").style.display = "none";
        document.getElementById("price").value = null;
        document.getElementById("percent").style.display = "block";
        document.getElementById("percent").attributes["required"] = "";
        document.getElementById("percent").required = true;
        document.getElementById("price").required = false;


    }
    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
</script>

@endsection