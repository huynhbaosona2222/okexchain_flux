@extends('layouts.app')
@section('header')
<title>Pixio Studio| Blogs</title>
<link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }

    .note-editor {
        border: 1px solid #a9a9a9 !important;
    }

    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        border: 2px solid #676A6C;

    }

    /* On mouse-over, add a grey background color */
    .container:hover input~.checkmark {
        background-color: #ccc;
    }



    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked~.checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid orange;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
@endsection
@section('title')
<title>Admin - Project | CS-Design </title>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            @if(isset($product))
            Edit Product
            @else
            Create Product
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Projects</a>
            </li>
            <li class="active">
                <strong>
                    @if(isset($product))
                    Edit
                    @else
                    Create
                    @endif
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1">Product Info</a></li>
                    @if (isset ($product))
                    <li class=""><a data-toggle="tab" href="#tab-4">Image Detail</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-3">Upsell Option</a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active ">
                        <div class="panel-body">
                            <form action="{{ isset($product) ? '/admin/products/update/'.$product->id: '/admin/products/create'}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="demo_hidden">
                                @csrf
                                <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{ isset($product) ? $product->name:   ''  }}" required>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Price (VND)</label>
                                    <div class="col-sm-10"><input type="number" class="form-control" name="price" value="{{ isset($product) ? $product->price:   ''  }}" required>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Quantity available</label>
                                    <div class="col-sm-10"><input type="number" class="form-control" name="qty_available" value="{{ isset($product) ? $product->qty_available:   ''  }}" required>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Size</label>
                                    <div class="col-sm-10">
                                        <input type="text" placeholder="eg: S,M,L" class="form-control" name="size" style="text-transform:uppercase" value="{{ isset($product) ? $product->sizes_name:   ''  }}">
                                        <span class="help-block m-b-none">Accept : XS, S, M, L, XL, 2XL, 3XL, 4XL, 5XL</span>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Color</label>
                                    <div class="col-sm-10">
                                        @foreach($colors as $color)
                                        <div class="col-sm-1">
                                            <label class="container">
                                                <input type="checkbox" name="color[]" value="{{$color->id}}" @if(isset($product)) @foreach($active_color as $item) @if($item==$color->id)
                                                checked
                                                @endif
                                                @endforeach
                                                @endif
                                                >
                                                <span class="checkmark" style="background-color: {{$color->color}};"></span>
                                            </label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Category</label>
                                    <div class="col-sm-10">
                                        <div>
                                            <select data-placeholder="Choose a Country..." class="chosen-select" name="category" tabindex="2" required>
                                                <option value="">Select</option>
                                                @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if(isset($product)) @if($category->id
                                                    ==
                                                    $product->id_category)
                                                    selected
                                                    @endif
                                                    @endif
                                                    >
                                                    {{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="description" id="detail" required>{{ isset($product) ? $product->description:   ''  }}</textarea>
                                    </div>
                                </div>
                                <!-- <div class="form-group"><label class="col-sm-2 control-label">Tags</label>
                                    <div class="col-sm-10">
                                        <div>
                                            <select name="tags[]" data-placeholder="Choose a tag" class="chosen-select" multiple style="width:350px;" tabindex="4">
                                                @foreach($tags as $tag)
                                                <option value="{{$tag->id}}" @if(isset($product)) @if($product->
                                                    hasTag($tag->id))
                                                    selected
                                                    @endif
                                                    @endif
                                                    >{{$tag->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-group"><label class="col-sm-2 control-label">New Tags</label>
                                    <div class="col-sm-10">
                                        <input style="width:100%" class="tagsinput form-control" type="text" name="new_tags" value="{{isset($product)?"$product->tags_name":""}}" />
                                    </div>
                                </div>
                                @if(!isset($product))
                                <div class="form-group"><label class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10"> <input type="file" id="user_profile_pic" class="form-control" accept="image/x-png,image/gif,image/jpeg" name="images[]" multiple>
                                    </div>
                                </div>
                                @endif
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" id="submit-form-btn" onclick="submitForm('#demo_hidden', '#submit-form-btn')" type="submit">
                                            @if(isset($product))
                                            Edit product
                                            @else
                                            Create product
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if (isset ($product))
                    <div id="tab-4" class="tab-pane ">
                        <div class="panel-body">
                            <div class="col-sm-1"></div>
                            <div class="table-responsive col-sm-10">
                                <table class="table table-bordered table-stripped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Image Preview
                                            </th>
                                            <th>
                                                Edit
                                            </th>
                                            <th>
                                                Delete
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($project_images))
                                        @foreach($project_images as $project_image)
                                        <tr>
                                            <td>
                                                <img src="{{$project_image->link}}" alt="" style="width:100px;border-radius:5px">
                                            </td>
                                            <form action="/admin/products/edit-image/{{$project_image->id}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <td>
                                                    <input type="file" id="file" class="form-control" name="link" required>
                                                    <input type="hidden" id="id_product" class="" name="id_product" value="{{$project_image->id_product}}">
                                                    <button type="submit" class="btn btn-primary pull-right" style="margin-top:30px"><i class="fa fa-upload"></i> </button>
                                                </td>
                                            </form>
                                            <td>
                                                <button class="btn btn-danger demo4" style="margin-top: 64px" id="{{$project_image->id}}"><i class="fa fa-trash"></i> </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        <tr>
                                            <td>
                                                @lang('add-more-img')
                                            </td>
                                            <form action="/admin/products/edit-image/{{$product->id}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <td>
                                                    <input type="file" id="file" class="form-control" name="images[]" multiple required>
                                                    <input type="hidden" id="id_project" class="" name="id_project" value="{{$product->id}}">
                                                    <button type="submit" class="btn btn-primary pull-right" style="margin-top:30px"><i class="fa fa-upload"></i> </button>
                                                </td>
                                            </form>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane ">
                        <div class="panel-body">
                            <div class="col-sm-1"></div>
                            <div class="table-responsive  col-sm-10">
                                <table class="table table-bordered table-stripped">
                                    <thead>
                                        <tr>
                                            <th>
                                            </th>
                                            <th>
                                                Option
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($upsell as $item)
                                        <tr>
                                            <td>
                                            </td>
                                            <form method="post" action="/admin/products/upsell/update/{{$item->id}}">
                                                @csrf
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">
                                                            Quantity Product
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number" min="2" class="form-control" name="qty" value="{{$item->qty}}" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" data-toggle="tooltip" data-placement="left" title="Price after discount = Product Price - (Product Price * Percentage Discount )">
                                                        <label class="col-sm-4 control-label">
                                                            Percentage Discount (%)
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number" oninput="PercentToPrice(event.target)" class="form-control" name="new_price" min="0" max="100" value="{{$item->new_price}}" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" data-toggle="tooltip" data-placement="left" title="Price after discount = Product Price -  Price Discount ">
                                                        <label class="col-sm-4 control-label">
                                                            Price Discount (VND)
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number"  readonly oninput="PriceToPercent(event.target)" class="form-control" value="{{$item->price_discount}}" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" style="margin-top: 5px;">
                                                        <label class="col-sm-4 control-label">
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <button type="submit" class="btn btn-sm btn-success" style="float: right; margin-top:10px">
                                                                Update
                                                            </button>
                                                        </div>
                                                        <div class="col-sm-1"></div>

                                                    </div>
                                                </td>
                                            </form>
                                            <td>
                                                <button id="{{$item->id}}" class="btn btn-danger demo5"><i class="fa fa-trash"></i> </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td>
                                                Add new option
                                            </td>
                                            <form method="post" action="/admin/products/upsell/{{$product->id}}">
                                                @csrf
                                                <td>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">
                                                            Quantity Product
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number" min="2" class="form-control" name="qty" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" data-toggle="tooltip" data-placement="left" title="Price after discount = Product Price - (Product Price * Percentage Discount )">
                                                        <label class="col-sm-4 control-label">
                                                            Percentage Discount (%)
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number" oninput="PercentToPrice(event.target)" class="form-control" name="new_price" min="0" max="100" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" data-toggle="tooltip" data-placement="left" title="Price after discount = Product Price -  Price Discount ">
                                                        <label class="col-sm-4 control-label">
                                                            Price Discount (VND)
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <input type="number" readonly oninput="PriceToPercent(event.target)" class="form-control" required>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                    <div class="form-group" style="margin-top: 5px;">
                                                        <label class="col-sm-4 control-label">
                                                        </label>
                                                        <div class="col-sm-7">
                                                            <button type="submit" class="btn btn-sm btn-success" style="float: right; margin-top:10px">
                                                                Create
                                                            </button>
                                                        </div>
                                                        <div class="col-sm-1"></div>
                                                    </div>
                                                </td>
                                            </form>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
<script>
    $(document).ready(function() {
        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });
        $('.chosen-select').chosen({
            width: "100%"
        });
    });
</script>
<script>
    function PriceToPercent(element) {
        let value = element.value;
        let parentElement = $(element).closest('.form-group');
        parentElement.prev().find('input').val(value * 100 / "{{isset($product) ? $product->price : 0}}");
    }

    function PercentToPrice(element) {
        let value = element.value;
        let parentElement = $(element).closest('.form-group');
        parentElement.next().find('input').val("{{isset($product) ? $product->price : 0}}"* value / 100);
        }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover it!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "Action Successfully.", "success");
                        window.location.href = '/admin/products/delete-image/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo5').click(function() {
            var link = $(this).attr('id');
            console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover it!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "Action Successfully.", "success");
                        window.location.href = '/admin/products/upsell/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection