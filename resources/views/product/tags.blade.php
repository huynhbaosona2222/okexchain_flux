@extends('layouts.app')
<title>Pixio Studio| Tags</title>
@section('header')
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2> Tags</h2>
        @if(isset($tag))
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li>
                <a href="/admin/products/tags">Tags</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
        @else
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li class="active">
                <strong> Tags</strong>
            </li>
        </ol>
        @endif
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="content">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                <li class="list-group-item">
                    {{ $error }}
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div class="row">
    @if( isset($tag) )
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ isset($tag) ? "Edit Tag":"Create new Tag"}}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-group" method="POST" action="{{isset($tag)?"/admin/products/tags/update":"/admin/products/tags/create"}}">
                        @csrf
                        <input class="form-control" value="{{ isset($tag) ? $tag->name:""}}" type="text" placeholder="Tag Name" name="tag_name" maxlength="16"
                            required />
                            <input name="id" value="{{ isset($tag) ? $tag->id:""}}" hidden />
                        <br />
                        <button type="submit" class="btn btn-primary">{{ isset($tag) ? "Update":"Create"}}</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
    @else
    <div class="col-lg-12">
    @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tags List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                            <tr>
                                <th data-toggle="true">ID</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($_GET['page'])) {
                                    $page = $_GET['page'];
                                } else {
                                    $page = 1;
                                }
                                ?>
                            @if(!isset ($page))
                            $page==1
                            @endif
                            @foreach($tag_list as $tag)
                            <tr>
                                <td>{{(($page-1) * 10) + $loop->index + 1}}</td>
                                <td><a href="/admin/products/tag/{{$tag->id}}"> {{$tag->name}} </a></td>
                                <td>
                                    <a href="/admin/products/tags/edit/{{$tag->id}}" class="btn btn-info btn-sm">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <button type="button" 
                                       class="btn btn-danger demo4" id="{{$tag->id}}" data-id="{{$tag->id}}">
                                        Delete
                                    </button>
                                </td>

                                </td>
                            </tr>
                            <!-- <div class="modal fade" id="exampleModal{{$tag->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog mt-5" role="document" style="width:30%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="hident exampleModalLabel {{$tag->id}}">
                                                Delete This Tag?
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Hmmmm,
                                                wait!
                                                </button>
                                            <a type="button" style="margin-bottom: 5px; " class="btn btn-danger"
                                                href="/admin/products/tags/delete/{{$tag->id}}">Sure!!!</a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this product!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "This post has been deleted.", "success");
                        window.location.href = '/admin/products/tags/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection