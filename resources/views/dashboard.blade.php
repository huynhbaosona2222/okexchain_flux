@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li class="active">
                <strong>Dashboard</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <!-- <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">Monthly</span>
                    <h5>Income</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">40 886,200</h1>
                    <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                    <small>Total income</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Annual</span>
                    <h5>Orders</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">275,800</h1>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>New orders</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">Today</span>
                    <h5>visits</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">106,120</h1>
                    <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                    <small>New visits</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-danger pull-right">Low value</span>
                    <h5>User activity</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">80,600</h1>
                    <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i>
                    </div>
                    <small>In first month</small>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <div class="col-lg-12" style="height: auto !important">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                            <a href="/?sort=day" class="btn btn-xs btn-white {{$active== 0 ? "active":""}}">Today</a>
                            <a href="/?sort=week" class="btn btn-xs btn-white {{$active== 1 ? "active":""}}">This Week</a>
                            <a href="/?sort=month" class="btn btn-xs btn-white {{$active== 2 ? "active":""}}">This Month</a>
                            <!-- <a  href="/?sort=quater" class="btn btn-xs btn-white {{$active== 3 ? "active":""}}" >This Quarter</a> -->
                        </div>
                    </div>
                </div>
                <div class="ibox-content" style="height: 100%">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="flot-chart" style="height: auto !important">
                                <canvas id="lineChart" height="auto"></canvas>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <ul class="stat-list">
                                <li>
                                    <h2 class="no-margins">{{$count_bills}}</h2>
                                    <small>Total orders in period</small>
                                    {{$count_last_bills == 0 ? $count_last_bills = 1:  $count_last_bills = $count_last_bills}}
                                    @if($count_bills > $count_last_bills)
                                    <div class="stat-percent">{{number_format($count_bills/$count_last_bills*100,2,'.','')}}% <i class="fa fa-level-up text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width: {{number_format(($count_bills/$count_last_bills*100-100),2,'.','')}}%" class="progress-bar"></div>
                                    </div>
                                    @else
                                    <div class="stat-percent">{{number_format($count_bills/$count_last_bills*100,2,'.','')}}% <i style="color:#ed5565" class="fa fa-level-down text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width:{{number_format(($count_bills/$count_last_bills*100),2,'.','')}}%;background-color:#ed5565" class="progress-bar"></div>
                                    </div>
                                    @endif
                                </li>
                                <li>
                                    <h2 class="no-margins">{{$count_last_bills}}</h2>
                                    <small>Orders in last period</small>
                                    {{$count_last__bills == 0 ? $count_last__bills = 1:  $count_last__bills = $count_last__bills}}
                                    @if($count_last_bills > $count_last__bills)
                                    <div class="stat-percent">{{number_format($count_last_bills/$count_last__bills*100,2,'.','')}}% <i class="fa fa-level-up text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width: {{number_format(($count_last_bills/$count_last__bills*100-100),2,'.','')}}%" class="progress-bar"></div>
                                    </div>
                                    @else
                                    <div class="stat-percent">{{number_format($count_last_bills/$count_last__bills*100,2,'.','')}}% <i style="color:#ed5565" class="fa fa-level-down text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width:{{number_format(($count_last_bills/$count_last__bills*100),2,'.','')}}%;background-color:#ed5565" class="progress-bar"></div>
                                    </div>
                                    @endif
                                </li>
                                <li>
                                    <h2 class="no-margins ">{{ number_format($income, 2) }} VND</h2>
                                    <small>Total Income from orders</small>
                                    {{$last_income == 0 ? $last_income = 1:  $last_income = $last_income}}
                                    @if($income > $last_income)
                                    <div class="stat-percent">{{number_format($income/$last_income*100,2,'.','')}}% <i class="fa fa-level-up text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width: {{number_format(($income/$last_income*100-100),2,'.','')}}%" class="progress-bar"></div>
                                    </div>
                                    @else
                                    <div class="stat-percent">{{number_format($income/$last_income*100,2,'.','')}}% <i style="color:#ed5565" class="fa fa-level-down text-navy"></i>
                                    </div>
                                    <div class="progress progress-mini">
                                        <div style="width:{{number_format(($income/$last_income*100),2,'.','')}}%;background-color:#ed5565" class="progress-bar"></div>
                                    </div>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th data-hide="phone">Customer</th>
                                <th data-hide="phone">Phone</th>
                                <th data-hide="phone">Amount</th>
                                <th data-hide="phone">Date added</th>
                                <th data-hide="phone,tablet">Ship expected</th>
                                <th data-hide="phone">Status</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $item)
                            <tr>
                                <td>
                                    {{$item->order_id}}
                                </td>
                                <td>
                                    {{$item->customer_name}}
                                </td>
                                <td>
                                    {{$item->customer_phone}}
                                </td>
                                <td>
                                    {{$item->tien_thu_ho}} ₫
                                </td>
                                <td>
                                    {{$item->order_created}}
                                </td>
                                <td>
                                    {{$item->ship_expected}}
                                </td>
                                <td>
                                    @if($item->status ==5 )
                                    <span class="label label-primary">Complete</span>
                                    @else
                                    <span class="label label-success">Pending</span>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <!-- <button data-toggle="modal" data-target="#exampleModal{{$item->id}}" class="btn-white btn btn-xs">View detail</button> -->
                                        <a data-toggle="modal" href="/admin/order/17" class="btn-white btn btn-xs">View detail</a>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" style="margin-top:125px" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog mt-5" role="document" style="width:30%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="hident exampleModalLabel {{$item->id}}">
                                                Order Detail
                                            </h5>
                                            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button> -->
                                        </div>
                                        <div class="modal-body">
                                            {{$item->detail}}
                                        </div>
                                        <!-- <div class="modal-footer">
                                           
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                @if($orders)
                                <td colspan="8">
                                    @if($totalPage!=1)
                                    <ul class="pagination pull-right">
                                        <ul class="pagination">
                                            @if ($currPage > 1)
                                            <li class="footable-page">
                                                <a class="page-link" href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                                            </li>
                                            @endif
                                            @foreach ($listPages as $page)
                                            @if($page== '...')
                                            <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                                <a class="">{{$page}} </a></li>
                                            @endif
                                            @if($page!= '...')
                                            <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                                <a class="" href="{{$currUrl}}?page={{$page}}">{{$page}} </a></li>
                                            @endif
                                            @endforeach
                                            @if ($currPage < $totalPage) <li class="footable-page">
                                                <a class="page-link" href="{{$currUrl}}?page={{$nextPage}}">&raquo; </a>
                                                </li>
                                                @endif
                                        </ul>
                                    </ul>
                                    @endif
                                </td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@section('footer')
<script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<script>
    $(function() {


        var lineData = {
            // labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
            labels: {!! $arr_days !!},
            datasets: [

                {
                    label: "Order Quantity",
                    backgroundColor: 'rgba(26,179,148,0.5)',
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: {{ $count_bill_in_time}}
                }
                // ,
                //  {
                //     label: "Last Month",
                //     backgroundColor: 'rgba(220, 220, 220, 0.5)',
                //     pointBorderColor: "#fff",
                //     data: {{$count_bill_in_last_month}}
                // }
            ]
        };

        var lineOptions = {
            responsive: true
        };

        var ctx = document.getElementById("lineChart").getContext("2d");
        new Chart(ctx, {
            type: 'line',
            data: lineData,
            options: lineOptions
        });
    });
</script>
<script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
@endsection
@endsection