<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Chillax | Dashboard</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    @yield('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('public/logo_chillax.png') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .img-circle {
            width: 50px;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="{{Auth::user()->ava_src}}" width="40%" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong>
                                    </span> <span class="text-muted text-xs block">{{Auth::user()->role_name}} <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="/admin/profile">Profile</a></li>
                                <!-- <li><a href="">Mailbox</a></li> -->
                                <li class="divider"></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            Chillax
                        </div>
                    </li>
                    <li class="{{Route::current()->getName() == 'dashboard' ? 'active' : ''}}">
                        <a href="/"><i class="fa fa-bar-chart"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li class="{{Route::current()->getName() == 'member' ? 'active' : ''}}">
                        <a href="/admin/member"><i class="fa fa-group"></i> <span class="nav-label">Member</span></a>
                    </li>
                    <li class="{{Route::current()->getName() == 'categories' ? 'active' : ''}}{{Route::current()->getName() == 'tags' ? 'active' : ''}}
                        {{Route::current()->getName() == 'products' ? 'active' : ''}}{{Route::current()->getName() == 'combos' ? 'active' : ''}}
                        {{Route::current()->getName() == 'promo' ? 'active' : ''}}{{Route::current()->getName() == 'color' ? 'active' : ''}}">
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Product</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="{{Route::current()->getName() == 'categories' ? 'active' : ''}}"><a href="/admin/products/categories">Categories</a></li>
                            <li class="{{Route::current()->getName() == 'tags' ? 'active' : ''}}"><a href="/admin/products/tags">Tags</a></li>
                            <li class="{{Route::current()->getName() == 'products' ? 'active' : ''}}"><a href="/admin/products">Products</a></li>
                            <li class="{{Route::current()->getName() == 'combos' ? 'active' : ''}}"><a href="/admin/products/combos">Combos</a></li>
                            <li class="{{Route::current()->getName() == 'promo' ? 'active' : ''}}"><a href="/admin/products/promotions">Promotions</a></li>
                            <li class="{{Route::current()->getName() == 'color' ? 'active' : ''}}"><a href="/admin/products/colors">Color</a></li>
                        </ul>
                    </li>
                    <!-- <li class="{{Route::current()->getName() == 'address' ? 'active' : ''}}">
                        <a href="/admin/address"><i class="fa fa-address-book"></i> <span class="nav-label">Address Management</span></a>
                    </li> -->
                    <li class="{{Route::current()->getName() == 'address' ? 'active' : ''}}">
                        <a href="/admin/customer"><i class="fa fa-address-book"></i> <span class="nav-label">Customer</span></a>
                    </li>
                    <!-- <li class="{{Route::current()->getName() == 'countdown' ? 'active' : ''}}">
                        <a href="/admin/demo-cart"><i class="fa fa-clock-o"></i> <span class="nav-label">Demo Countdown</span></a>
                    </li> -->
                    <li class="{{Route::current()->getName() == 'config' ? 'active' : ''}}">
                        <a href="/admin/configs"><i class="fa fa-wrench"></i> <span class="nav-label">Config</span></a>
                    </li>
                    <li class="{{Route::current()->getName() == 'delivery' ? 'active' : ''}}">
                        <a href="/admin/deliveries"><i class="fa fa-bicycle"></i> <span class="nav-label">Delivery</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom">
                            <div class="form-group">
                                <input type="text" readonly class="form-control" name="search-phase">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <!-- <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                            </a>
                        </li> -->
                        <li>
                            <a href="/logout">
                                <i class="fa fa-sign-out"></i>Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            @yield('content')
        </div>
    </div>
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <!-- <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script> -->
    <script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    @yield('footer')
</body>

</html>