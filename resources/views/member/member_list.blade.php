@extends('layouts.app')
@section('header')
<title>Pixio Studio| Member</title>
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading" style="margin-bottom: 15px;">
    <div class="col-lg-10">
        <h2>Member Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li class="active">
                <strong>Member Management</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="content">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
</div>
<div class="wrapper wapper-corntent" style="margin-bottom: -30px;">
    <div class="row">
        @if(isset($member))
        <div class="wrapper col-lg-12 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Edit member
                </h2>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <form action="/admin/update-member/{{$member->id}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required readonly name="email" value="{{$member->email}}"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required name="name" required value="{{$member->name}}"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Phone Number</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required name="phone_number" required value="{{$member->phone_number}}"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Role</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="role">
                                    @if (count($list_role) > 0)
                                    @foreach ($list_role as $key => $role)
                                    <option value="{{$role->id}}" {{$member->role_id==$role->id ? "selected":""}}>{{$role->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10"> <img src="{{$member->ava_src}}" width="20%"> </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">New Image</label>
                            <div class="col-sm-10"><input type="file" class="form-control" name="image" value=""></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">New password (*Emergency only)</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" minlength="8" name="new_password">
                            </div>
                        </div>
                        <div class="mail-body text-right tooltip-demo">
                            <button type="submit" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"> Update member</button>
                        </div>
                    </form>
                </div>
            </div>
            @else
            <div class="col-lg-12 animated fadeInRight">
                <div class="mail-box-header">
                    <h2>
                        Add new member
                    </h2>
                </div>
                <div class="mail-box">
                    <div class="mail-body">
                        <form action="/admin/add-member" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10"><input type="email" class="form-control" name="email" required value=""></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10"><input type="text" class="form-control" name="name" required value=""></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Phone number</label>
                                <div class="col-sm-10"><input type="text" class="form-control" name="phone_number" required value=""></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <select class="form-control" required="" name="role">
                                        @if (count($list_role) > 0)
                                        @foreach ($list_role as $key => $role)
                                        <option value="{{$role->id}}" {{$key == 0 ? 'selected' : ''}}>{{$role->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10"><input type="file" class="form-control" name="image" required value=""></div>
                            </div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                        <button type="submit" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"> Add member</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif

    </div>
    <div class="row wrapper">
        <div class="row">
            @foreach ($list_member as $member)
            <div class="col-lg-3">
                <div class="contact-box center-version" style="min-height: 300px">
                    <a href="#">
                        <img alt="image" class="" width="100%" src="{{$member->ava_src}}">
                        <h3 class="m-b-xs"><strong>{{$member->name}}</strong></h3>
                        <div class="font-bold">{{$member->email}}</div>
                    </a>
                    <div class="contact-box-footer">
                        <div class="m-t-xs btn-group">
                            <a href="/admin/edit-member/{{$member->id}}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            <a class="btn btn-xs btn-danger demo4" id="{{$member->id}}" data-id="{{$member->id}}"><i class="fa fa-edit"></i> Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            // console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this account!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "This member has been deleted.", "success");
                        window.location.href = '/admin/delete-member/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection