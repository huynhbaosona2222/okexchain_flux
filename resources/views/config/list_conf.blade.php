@extends('layouts.app')
@section('header')
<link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Configs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li class="active">
                <strong>Config list</strong>
            </li>
        </ol>
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
       <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Configs list</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table dataTables table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Config</th>
                                    <th style="width: 350px;">Description</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($configs) > 0)
                                @php
                                $count= $limitPerPage * ($currPage - 1) + 1;
                                @endphp
                                @foreach ($configs as $config)
                                <tr>
                                    <td>{{$count++}}</td>
                                    <td>{{$config->conf_name}}</td>
                                    <td>{!!$config->description!!}</td>
                                    <form action="/admin/configs/update/{{$config->id}}" method="post">
                                    @csrf
                                    <td>
                                        <input name="value" class="form-control" style="width: 50%"  type="number" required value="{{$config->value}}"></input>
                                    </td>
                                    <td>
                                        <button style="margin: 5px;"  class="btn btn-warning btn-custom" type="submit">
                                            Update
                                        </button>
                                    </td>
                                    </form>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>

                        @php
                        $fullUrl = explode('?', $_SERVER['REQUEST_URI']);
                        $currUrl = $fullUrl[0];
                        @endphp
                        <!-- <div class="pagination">
                            @if ($currPage > 1)
                            <a href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                            @endif

                            @foreach ($listPages as $page)

                            @if($page!= '...')
                            <a class="{{($page==$currPage) ? 'active':''}}" href="{{$currUrl}}?page={{$page}}">{{$page}}</a>
                            @else
                            <a>{{$page}}</a>
                            @endif

                            @endforeach

                            @if ($currPage < $totalPage) <a href="{{$currUrl}}?page={{$nextPage}}">&raquo;</a>
                                @endif
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('footer')

@endsection