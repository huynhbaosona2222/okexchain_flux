@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li class="active">
                <strong>Profile</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper animated fadeInRight">
    <div class="content" style="margin-top:15px;">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
        @endif
    </div>
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-4">

            <div class="profile-image">
                <img src="{{$user_info->ava_src}}" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div>
                    <h2 class="no-margins">
                        {{$user_info->name}}
                    </h2>
                    <h4>{{$user_info->role_name}}</h4>
                    <strong>Email: </strong>{{$user_info->email}} <br>
                    <strong>Phone: </strong>{{$user_info->phone_number}}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <button type="button" class="btn btn-white btn-sm" data-toggle="modal" data-target="#myModal"><i
                    class="fa fa-pencil"></i> Edit Profile </button> <br><br>
            <button type="button" class="btn btn-white btn-sm" data-toggle="modal" data-target="#myModal1"><i
                    class="fa fa-pencil"></i> Edit Password</button>
        </div>
        <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <img src="{{$user_info->ava_src}}" width="40%" alt="">

                        <h4 class="modal-title">Hi {{$user_info->name}} !</h4>
                        <small class="font-bold">Edit your profile below</small>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="/admin/profile/update" class="form-horizontal"
                            enctype="multipart/form-data">

                            @csrf
                            <div class="form-group"><label class="col-sm-4 control-label">Your Name:</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="name" required
                                        value="{{$user_info->name}}"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Your phone:</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="phone_number"
                                        required value="{{$user_info->phone_number}}"></div>

                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">New Image:</label>
                                <div class="col-sm-8"><input type="file" class="form-control" name="ava_src"></div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <img src="{{$user_info->ava_src}}" width="40%" alt="">
                        <h4 class="modal-title">Hi {{$user_info->name}} !</h4>
                        <small class="font-bold">Edit your password below</small>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/profile/update-password" method="POST" class="form-horizontal">
                            @csrf
                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="current-password" type="password" class="form-control"
                                        name="current-password" placeholder="Current Password" required>
                                    @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="new-password" type="password" class="form-control" name="new-password"
                                        placeholder="New Password" required>

                                    @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input id="new-password-confirm" type="password" class="form-control"
                                        name="new-password_confirmation" placeholder="Confirm successNew Password"
                                        required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="display: flex; justify-content: center;">
                                    <button type="submit" class="btn btn-primary">
                                        Change Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <table class="table small m-b-xs">
                <tbody>
                    <tr>
                        <td>
                            <strong>142</strong> Projects
                        </td>
                        <td>
                            <strong>22</strong> Followers
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <strong>61</strong> Comments
                        </td>
                        <td>
                            <strong>54</strong> Articles
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>154</strong> Tags
                        </td>
                        <td>
                            <strong>32</strong> Friends
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-2">
            <small>Sales in last 24h</small>
            <h2 class="no-margins">206 480</h2>
            <div id="sparkline1"></div>
        </div>
    </div>
</div>
</div>
@endsection