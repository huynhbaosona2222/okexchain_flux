@extends('layouts.app')
@section('header')
<link href="{{ asset('css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Delivery management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li class="active">
                <strong>Delivery management</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="content">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
        @endif
        @yield('content')
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{isset($delivery)?"Edit":"Add New"}} Delivery</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-group" method="POST" action="{{isset($delivery)? "/admin/deliveries/update/$delivery->id" : "/admin/deliveries/create"}}">
                        @csrf
                        <input class="form-control" type="text" placeholder="Delivery Name" name="name" maxlength="16" required value="{{isset($delivery)?"$delivery->name":""}}" />
                        <br />
                        <textarea class="form-control" type="text" rows="5" placeholder="Description (Optional)" name="description">{{isset($delivery)?"$delivery->description":""}}</textarea>
                        <br />
                        <button type="submit" class="btn btn-primary">{{isset($delivery)?"Update":"Add New"}}</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List Delivery</h5>
                </div>
                <div class="ibox-content">
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>
                                    <center>
                                        Action
                                    </center>
                                </th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($deliveries as $item)
                            <tr>
                                <td><a href="#">{{$item->name}} </a></td>
                                <td>
                                    <center>
                                        <a href="/admin/deliveries/edit/{{$item->id}}" class="btn btn-info btn-sm">
                                            Edit
                                        </a>
                                        <!-- <button type="button" data-toggle="modal" data-target="#exampleModal{{$item->id}}" class="btn btn-danger btn-sm">
                                            Delete
                                        </button> -->
                                        <a class="btn btn-danger btn-sm demo4" id="{{$item->id}}" data-id="{{$item->id}}">Delete</a>
                                    </center>
                                </td>
                                <td>
                                    <a href="/admin/deliveries/active/{{$item->id}}" class="btn btn-primary btn-sm">
                                        @if($item->active == false)
                                        Active
                                        @else
                                        Disable
                                        @endif
                                    </a>
                                </td>
                            </tr>
                            <!-- <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog mt-5" role="document" style="width:30%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="hident exampleModalLabel {{$item->id}}">
                                                Delete This Delivery?
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Hmmmm,
                                                wait!</button>
                                            <a type="button" style="margin-bottom: 5px; " class="btn btn-danger" href="/admin/deliveries/delete/{{$item->id}}">Sure!!!</a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this product!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "This post has been deleted.", "success");
                        window.location.href = '/admin/deliveries/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection