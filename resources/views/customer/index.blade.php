@extends('layouts.app')
@section('header')
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Customer management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Customer management</a>
            </li>
            <li class="active">
                <strong>Customers</strong>
            </li>
        </ol>
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row" style="display:flex; align-items: center; flex-wrap: wrap;">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Customer List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                    <div class="ibox-content">
                        <table class="footable table table-stripped toggle-arrow-tiny">
                            <thead>
                                <tr>
                                    <th data-toggle="true">Name</th>
                                    <th>Phone number</th>
                                    <th>Complete order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $item)
                                <tr>
                                    <td><a href="/admin/customer/{{$item->id}}" style="color: #676A6C">{{$item->name}}</a></td>
                                    <td><a style="color: #676A6C">{{$item->phone_number}}</a></td>
                                    <td>
                                        <div class="col-sm-2"></div> {{$item->complete_order}}
                                    </td>
                                    <td class="">
                                        <div class="btn-group">
                                            <a href="/admin/customer/{{$item->id}}" class="btn-primary btn btn-sm">View detail</a>
                                            <!-- <a href="/admin/products/edit/{{$item->id}}" class="btn-info btn btn-xs">Edit</a> -->
                                            <!-- <a class="btn-danger btn btn-xs demo4" id="{{$item->id}}" data-id="{{$item->id}}">Delete</a> -->
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8">
                                        @if($totalPage!=1)
                                        <ul class="pagination pull-right">
                                            @if ($currPage > 1)
                                            <li class="footable-page">
                                                <a class="page-link" href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                                            </li>
                                            @endif
                                            @foreach ($listPages as $page)
                                            @if($page== '...')
                                            <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                                <a class="">{{$page}} </a></li>
                                            @endif
                                            @if($page!= '...')
                                            <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                                <a class="" href="{{$currUrl}}?page={{$page}}">{{$page}} </a></li>
                                            @endif
                                            @endforeach
                                            @if ($currPage < $totalPage) <li class="footable-page">
                                                <a class="page-link" href="{{$currUrl}}?page={{$nextPage}}">&raquo; </a>
                                                </li>
                                                @endif
                                        </ul>
                                        @endif
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- @foreach($address as $item)
        <div class="col-lg-4">
            <div class="contact-box">
                <a href="/admin/address/edit/{{$item->id}}">
                    <div class="col-sm-11">
                        <h3><strong>{{$item->customer_name}}</strong></h3>
                        <p><i class="fa fa-map-marker"></i> {{$item->address}}</p>
                        <address>
                            <strong>Detail</strong><br>
                            {{$item->address_detail}}<br>
                            <strong>Added_by:</strong> {{$item->creator_name}} <br>
                            <strong>Phone:</strong> {{$item->phone_number}} <br>
                            <strong>Count complete order:</strong> {{$item->count_complete_order}}
                        </address>
                    </div>
                    <div class="clearfix">
                        <a class="demo4" id="{{$item->id}}" data-id="{{$item->id}}">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </a>
            </div>
        </div>
        @endforeach
        <div class="col-lg-4">
            <a href="/admin/address/create">
                <i class="fa fa-plus-square-o fa-x5" style="font-size: 64px; padding-left:30px"></i>
            </a>
        </div> -->
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            // console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover it!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "Action complete.", "success");
                        window.location.href = '/admin/address/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection