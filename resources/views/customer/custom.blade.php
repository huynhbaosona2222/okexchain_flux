@extends('layouts.app')
@section('header')
<title>Pixio Studio| Position</title>
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Address</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a  href="/admin/address">Address</a>
            </li>
            <li class="active">
                <strong>{{ isset($address)? "Edit Address" : "Add New Address"}}</strong>
            </li>
        </ol>
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @if(isset($address)) 
        <div class="mail-box-header">
            <h2>
                Edit Address
            </h2>
        </div>
        <div class="mail-box">
            <div class="mail-body">
                <form action="/admin/address/update/{{$address->id}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Customer name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" required name="name" value="{{$address->customer_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Current Address
                        </label>
                        <div class="col-sm-10"><input type="text" class="form-control" readonly required value="{{$address->address}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Address
                        </label>
                        <div class="col-sm-3">
                            <select name="city_id" id="city_id" class="form-control" >
                                <option value="" selected disabled>Chọn Tỉnh / Thành phố</option>
                                @foreach ($province_list as $item)
                                    <option value="{{$item->id}}">{{$item->_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="state_id" id="state_id" class="form-control" >
                                <option value="" selected disabled>Chọn Quận / Huyện </option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="ward_id" id="ward_id" class="form-control" >
                                <option value="" selected disabled>Chọn Xã / Phường </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address detail</label>
                        <div class="col-sm-10"><input type="text" class="form-control" required name="address_detail" required value="{{$address->address_detail}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10"><input type="text" class="form-control" required name="phone_number" required value="{{$address->phone_number}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Added by </label>
                        <div class="col-sm-10"><input type="text" class="form-control" required readonly required value="{{$address->creator_name}}"></div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                        <button type="submit" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"> Update address</button>
                    </div>
                </form>
            </div>
        </div>
        @else
        <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    Add new address
                </h2>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <form action="/admin/address/create" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group"><label class="col-sm-2 control-label">Customer name</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required name="name"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-3">
                                <select name="city_id" id="city_id" class="form-control" required>
                                    <option value="" selected disabled>Chọn Tỉnh / Thành phố</option>
                                    @foreach ($province_list as $item)
                                    <option value="{{$item->id}}">{{$item->_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="state_id" id="state_id" class="form-control" required>
                                    <option value="" selected disabled>Chọn Quận / Huyện </option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="ward_id" id="ward_id" class="form-control" required>
                                    <option value="" selected disabled>Chọn Xã / Phường </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Address detail</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required name="address_detail" required></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Phone Number</label>
                            <div class="col-sm-10"><input type="text" class="form-control" required name="phone_number" required></div>
                        </div>
                </div>
                <div class="mail-body text-right tooltip-demo">
                    <button type="submit" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"> Add Address</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@section('footer')
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.demo4').click(function() {
            var link = $(this).attr('id');
            // console.log(link);
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover it!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {

                    if (isConfirm) {
                        swal("Deleted!", "This post has been deleted.", "success");
                        window.location.href = '/admin/address/delete/' + link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
        $('#city_id').on('change', function() {
            var id = $(this).val();
            $.ajax({
                url: '/admin/address/get-states/' + id,
                type: "get",
                success: function(data) {
                    var html = '';
                    $('#state_id').children().remove();

                    var list = data.states;
                    for (var i = 0; i < list.length; i++) {
                        html += '<option value="' + list[i].id + '">' + list[i]._name + '</option>';
                    }
                    $('#state_id').append(html);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
        $('#state_id').on('change', function() {
            var id = $(this).val();
            $.ajax({
                url: '/admin/address/get-wards/' + id,
                type: "get",
                success: function(data) {
                    var html = '';
                    $('#ward_id').children().remove();

                    var list = data.wards;
                    for (var i = 0; i < list.length; i++) {
                        html += '<option value="' + list[i].id + '">' + list[i]._name + '</option>';
                    }
                    $('#ward_id').append(html);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
    });
</script>
@endsection