@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Customer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="#">Customers</a>
            </li>
            <li class="active">
                <strong>Detail</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper animated fadeInRight">

    <div class="content" style="margin-top:15px;">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{session()->get('error')}}
        </div>
        @endif
    </div>

    <div class="row m-b-lg m-t-lg">
        <div class="col-md-4">

            <div class="profile-image">
                <img src="{{$customer_info->ava_src}}" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div>
                    <h2 class="no-margins">
                        {{$customer_info->name}}
                    </h2>
                    <h4>{{$customer_info->role_name}}</h4>
                    <strong>Email: </strong>{{$customer_info->email}} <br>
                    <strong>Phone: </strong>{{$customer_info->phone_number}}
                </div>
            </div>
        </div>
        <!-- <div class="col-md-3">
            <table class="table small m-b-xs">
                <tbody>
                    <tr>
                        <td>
                            <strong>142</strong> Projects
                        </td>
                        <td>
                            <strong>22</strong> Followers
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <strong>61</strong> Comments
                        </td>
                        <td>
                            <strong>54</strong> Articles
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>154</strong> Tags
                        </td>
                        <td>
                            <strong>32</strong> Friends
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> -->
        <!-- <div class="col-md-3">
            <small>Sales in last 24h</small>
            <h2 class="no-margins">206 480</h2>
            <div id="sparkline1"></div>
        </div> -->
    </div>
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Order in last month
                        <!-- <small>With custom colors.</small> -->
                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChart" height="120"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">

                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                        <h3>Order History</h3>
                        <tr>
                            <th>Order ID</th>
                            <!-- <th data-hide="phone">Customer</th> -->
                            <th data-hide="phone">Amount</th>
                            <th data-hide="phone">Time Created</th>
                            <th data-hide="phone">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bills_demo as $item)
                        <tr>
                            <td>
                                {{$item->id_bill}}
                            </td>
                            <!-- <td>
                            </td> -->
                            <td>
                                {{rand(111,999)}}.000VND
                            </td>
                            <td>
                                {{$item->created_at}}
                            </td>
                            <td>
                                <span class="label label-primary">Complete</span>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a href="/admin/order/{{$item->id}}" class="btn-white btn btn-xs">Detail</a>
                                    <!-- <button class="btn-white btn btn-xs">Edit</button>
                                        <button class="btn-white btn btn-xs">Delete</button> -->
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8">
                                @if($totalPage!=1)
                                <ul class="pagination pull-right">
                                    @if ($currPage > 1)
                                    <li class="footable-page">
                                        <a class="page-link" href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                                    </li>
                                    @endif
                                    @foreach ($listPages as $page)
                                    @if($page== '...')
                                    <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                        <a class="">{{$page}} </a></li>
                                    @endif
                                    @if($page!= '...')
                                    <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                        <a class="" href="{{$currUrl}}?page={{$page}}">{{$page}} </a></li>
                                    @endif
                                    @endforeach
                                    @if ($currPage < $totalPage) <li class="footable-page">
                                        <a class="page-link" href="{{$currUrl}}?page={{$nextPage}}">&raquo; </a>
                                        </li>
                                        @endif
                                </ul>
                                @endif
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">

                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                    <thead>
                        <h3>List Address</h3>
                        <tr>
                            <th>ID</th>
                            <th data-hide="phone">Name</th>
                            <th data-hide="phone">Phone number</th>
                            <th data-hide="phone">Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($address_list as $address)
                        <tr>
                            <td>
                                {{$address->id}}
                            </td>
                            <td>
                                {{$address->customer_name}}
                            </td>
                            <td>
                                {{$address->phone_number}}
                            </td>
                            <td>
                                {{$address->address}}
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                    <!-- <tfoot>
                            <tr>
                                <td colspan="7">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot> -->
                </table>

            </div>
        </div>
    </div>
</div>
</div>
@section('footer')
<script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script>
    $(function() {
        var lineData = {
            labels: {{$arr_days}} ,
            datasets: [
                {
                    label: "This Month",
                    backgroundColor: 'rgba(26,179,148,0.5)',
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: {{$count_bill_in_month}}
                }, {
                    label: "Last Month",
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    pointBorderColor: "#fff",
                    data: {{$count_bill_in_last_month}}
                }
            ]
        };
        var lineOptions = {
            responsive: true
        };
        var ctx = document.getElementById("lineChart").getContext("2d");
        new Chart(ctx, {
            type: 'line',
            data: lineData,
            options: lineOptions
        });
    });
</script>
<script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
<!-- <script src="{{ asset('js/demo/chartjs-demo.js') }}"></script> -->
@endsection
@endsection