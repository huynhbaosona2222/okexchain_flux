@extends('layouts.app')
@section('header')
<title>Pixio Studio|Combos</title>
<link href="/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Combos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li class="active">
                <strong>Combos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="/admin/products/combos/create" class="btn btn-w-m btn-primary">
                        Add New Combo
                    </a>
                </div>
                <div class="ibox-content">
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                            <tr>
                                <th data-toggle="true">Name</th>
                                <th >Price</th>
                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($combo_list as $combo)
                            <tr>
                                <td><a href="/admin/products/combos/edit/{{$combo->id}}" style="color: #676A6C">{{$combo->name}}</a></td>
                                <td><a href="#" style="color: #676A6C">{{$combo->new_price}}</a></td>
                               
                                <td class="">
                                    <div class="btn-group">
                                        <!-- <a href="/admin/blog_detail/{{$combo->id}}" class="btn-primary btn btn-xs">View</a> -->
                                        <a href="/admin/products/combos/edit/{{$combo->id}}" class="btn-info btn btn-xs">Edit</a>
                                        <a class="btn-danger btn btn-xs demo4" id="{{$combo->id}}" data-id="{{$combo->id}}" 
                                          >Delete</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8">
                                    @if($totalPage!=1)
                                    <ul class="pagination pull-right">
                                        @if ($currPage > 1)
                                        <li class="footable-page">
                                            <a class="page-link" href="{{$currUrl}}?page={{$previousPage}}">&laquo;</a>
                                        </li>
                                        @endif
                                        @foreach ($listPages as $page)
                                        @if($page== '...')
                                        <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                            <a class="">{{$page}} </a></li>
                                        @endif
                                        @if($page!= '...')
                                        <li class="footable-page {{($page==$currPage) ? 'active':''}}">
                                            <a class="" href="{{$currUrl}}?page={{$page}}">{{$page}} </a></li>
                                        @endif
                                        @endforeach
                                        @if ($currPage < $totalPage) <li class="footable-page">
                                            <a class="page-link" href="{{$currUrl}}?page={{$nextPage}}">&raquo; </a>
                                            </li>
                                            @endif
                                    </ul>
                                    @endif
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="/js/plugins/footable/footable.all.min.js"></script>
<script src="/js/plugins/sweetalert/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $('.footable').footable();
        $('.footable2').footable();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.demo4').click(function () {
            var link=$(this).attr('id');
            console.log(link);
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this combo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                            
                    if (isConfirm) {
                        swal("Deleted!", "This post has been deleted.", "success");
                        window.location.href = '/admin/products/combos/delete/'+link;
                    } else {
                        swal("Cancelled", ":)", "error");
                    }
                });
        });
    });
</script>
@endsection