@extends('layouts.app')
@section('header')
<title>Pixio Studio| Blogs</title>
<link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
<style>
    .bootstrap-tagsinput {
        width: 100%;
    }

    .note-editor {
        border: 1px solid #a9a9a9 !important;
    }
</style>
@endsection
@section('title')
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            @if(isset($combo))
            Edit Combo
            @else
            Create Combo
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Admin</a>
            </li>
            <li>
                <a href="/admin/products">Products</a>
            </li>
            <li>
                <a href="/admin/products/combos">Combos</a>
            </li>
            <li class="active">
                <strong>
                    @if(isset($combo))
                    Edit
                    @else
                    Create
                    @endif
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="content" style="margin-top:30px">
    @if(session()->has('success'))
    <div class="alert alert-success">
        {{session()->get('success')}}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        {{session()->get('error')}}
    </div>
    @endif
    @yield('content')
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1">Combo Info</a></li>
                    @if (isset ($combo))
                    <!-- <li class=""><a data-toggle="tab" href="#tab-4">Image Detail</a></li> -->
                    @endif
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active ">
                        <div class="panel-body">
                            <form action="{{ isset($combo) ? '/admin/products/combos/update/'.$combo->id: '/admin/products/combos/create'}}" method="POST" class="form-horizontal" enctype="multipart/form-data" id="demo_hidden">
                                @csrf
                                <div class="form-group"><label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{ isset($combo) ? $combo->name:   ''  }}" required>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Price</label>
                                    <div class="col-sm-10"><input type="number" class="form-control" name="price" value="{{ isset($combo) ? $combo->new_price:   ''  }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Choose Products <br>(More than 1 item)</label>
                                    <div class="col-sm-10">
                                        <select id="pdt_list" name="products[]" class="form-control" multiple>
                                            @if(isset($combo))
                                            @foreach ($list_pdt as $item)
                                            <option value="{{$item->id}}" selected="selected">{{$item->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group" style="margin-bottom: 20px"><label class="col-sm-2 control-label">Choose Products</label>
                                    <div class="col-sm-10" style="margin-top: 20px;">
                                        <div class="scroll_content" style="max-height: 400px">
                                            <div class="users-list">
                                                @foreach($list_pdt as $item)
                                                <div class="col-sm-6" style="margin-top: 10px;">
                                                    <div class="i-checks">
                                                        <input type="checkbox" name="products[]" value="{{$item->id}}" @if(isset($combo) && $item->checked=="ok")
                                                        checked
                                                        @endif
                                                        >
                                                        <img style="margin-left: 10px;margin-right:10px" width="10%" src="{{$item->image}}">
                                                        <span style="pull-right">{{$item->name}}</span>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="description" id="detail" required>{{ isset($combo) ? $combo->description:   ''  }}</textarea>
                                    </div>
                                </div>
                                <!-- 
                                @if(!isset($combo))
                                <div class="form-group"><label class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10"> <input type="file" id="user_profile_pic" class="form-control" accept="image/x-png,image/gif,image/jpeg" name="images[]" multiple>
                                    </div>
                                </div>
                                @endif -->
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" id="submit-form-btn" onclick="submitForm('#demo_hidden', '#submit-form-btn')" type="submit">
                                            @if(isset($combo))
                                            Edit Combo
                                            @else
                                            Create Combo
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- @if (isset ($combo))
                    <div id="tab-4" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-1"></div>
                            <div class="table-responsive col-sm-10">
                                <table class="table table-bordered table-stripped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Image
                                            </th>
                                            <th>
                                                Edit
                                            </th>
                                            <th>
                                                Delete (1 Click)
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($combo_images))
                                        @foreach($combo_images as $image)
                                        <tr>
                                            <td>
                                                <img src="{{$image->link}}" alt="" style="width:100px;border-radius:5px">
                                            </td>
                                            <form action="/admin/products/combos/edit-image/{{$image->id}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <td>
                                                    <input type="file" id="file" class="" name="link" required>
                                                    <input type="hidden" id="id_product" class="" name="id_product" value="{{$image->id_product}}">
                                                    <button type="submit" class="btn btn-white" style="margin-top:30px"><i class="fa fa-edit"></i> </button>
                                                </td>
                                            </form>
                                            <td>
                                                <a href="/admin/products/delete-image/{{$image->id}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        <tr>
                                            <td>
                                                @lang('add-more-img')
                                            </td>
                                            <form action="/admin/products/combos/edit-image/{{$combo->id}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <td>
                                                    <input type="file" id="file" class="" name="images[]" multiple required>
                                                    <input type="hidden" id="id_project" class="" name="id_combo" value="{{$combo->id}}">
                                                    <button type="submit" class="btn btn-white" style="margin-top:30px"><i class="fa fa-edit"></i> </button>
                                                </td>
                                            </form>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script src="{{ asset('js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/js/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
<script>
    $(document).ready(function() {
        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });
        $('.chosen-select').chosen({
            width: "100%"
        });
    });
    $(document).ready(function() {

        // Add slimscroll to element
        // $('.scroll_content').slimscroll({
        //     height: '400px'
        // });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

    });
</script>
<script>
    $('#pdt_list').select2({
        // placeholder: "Choose products...",
        minimumInputLength: 2,
        ajax: {
            url: '/pdt/find',
            // type: "POST",
            dataType: 'json',
            data: function(params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function(data) {
                console.log(data)
                return {
                    results: data
                };
            },
            cache: true
        }
    });
</script>
@endsection