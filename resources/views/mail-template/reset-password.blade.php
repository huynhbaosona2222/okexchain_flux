<p>
    Your email has been successfully reset!
</p>
<p>
    Please go to <a href="{{$urlResetPassword}}">{{$urlResetPassword}}</a> to login in with your new credentials!
</p>
<h4>YOUR UPDATED CREDENTIAL:</h4>
<p>Email: {{$email}}</p>
<p>Password: {{$userNewPassword}}</p>
<p>
    Thank You.
</p>
