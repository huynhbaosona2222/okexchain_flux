@if($user_type == 2)
<h3>Hello {{$name}}</h3>
<br>
<h4>Your new password: {{$new_pass}}</h4>
<br>
@else
<h3>Hello {{$name}}</h3>
<br>
<h4>Your reset password link</h4>
<br>
<a href="{{$urlResetPassword}}"> Click here to reset your password</a>
@endif
