const path = require("path");
const glob = require("glob");
const webpack = require("webpack");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserJSPlugin = require("terser-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
//   .BundleAnalyzerPlugin;

process.env.NODE_ENV = process.env.NODE_ENV || "development";

function buildGlob(dir, ext) {
  return glob.sync(path.join(dir, "**." + ext)).reduce(function(obj, el) {
    obj[path.parse(el).name] = el;
    return obj;
  }, {});
}

module.exports = function(env) {
  const isProduction = env === "production";

  const plugins = [
    new MiniCssExtractPlugin({
      filename: path.join("css", "[name].css")
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ];
  // if (!isProduction) {
  //   plugins.push(new BundleAnalyzerPlugin());
  // }

  return {
    plugins,
    mode: isProduction ? "production" : "development",
    devtool: "source-map",
    resolve: {
      extensions: [".js", ".ts", ".tsx"]
    },
    optimization: isProduction
      ? {
          minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})]
        }
      : {},
    module: {
      rules: [
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "ts-loader"
            },
            {
              loader: "eslint-loader"
            }
          ]
        },
        {
          enforce: "pre",
          test: /\.js$/,
          loader: "source-map-loader"
        },
        {
          test: /\.s?css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: true,
                config: {
                  path: "postcss.config.js"
                }
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        }
      ]
    },

    entry: buildGlob(path.join(__dirname, "client", "entries"), "tsx"),
    output: {
      filename: path.join("js", "[name].js"),
      path: path.join(__dirname, "public")
    },
    externals: {
      react: "React",
      "react-dom": "ReactDOM"
    }
  };
};
