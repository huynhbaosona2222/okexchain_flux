<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([

            [
                'name' => 'PixioStudio',
                'slug' => 'pixio2tudio',
            
            ],

            [
                'name' => 'Fashion',
                'slug' => 'fashion',
            
            ],

            [
                'name' => 'School',
                'slug' => 'school',
            ],
        ]);
    }
}
