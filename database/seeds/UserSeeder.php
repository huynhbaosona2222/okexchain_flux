<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'PixioStudio',
                'email'=> 'hello@pixiostudio.com',
                'phone_number' => '84373205443',
                'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                'role_id' => 1,
            ],

            [
                'name' => 'Nguyễn Văn A',
                'email'=> 'vananguyen@pixiostudio.com',
                'phone_number' => '84919412094',
                'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                'role_id' => 3,
            ],

            [
                'name' => 'Lê Văn B',
                'email'=> 'vanble@pixiostudio.com',
                'phone_number' => '841415132094',
                'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                'role_id' => 3,
            ],

            [
                'name' => 'Nguyễn Văn C',
                'email'=> 'vancnguyen@pixiostudio.com',
                'phone_number' => '84919412094',
                'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                'role_id' => 3,
            ],

            [
                'name' => 'Trần Văn D',
                'email'=> 'vandtran@pixiostudio.com',
                'phone_number' => '841415132094',
                'ava_src' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/77c1544a037de2f54.jpg',
                'password'=>'$2y$10$egpKAKvCPvY4jGh5Qy6G0uqWYWhxu8/L2Hz/KkIMIn4aDrPvSUMVO',
                'role_id' => 3,
            ],
           ]);
    }
}
