<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_images')->insert([
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/208ebd14a6499ba8d.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/ce8e009dcaf21a0e8.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/eb7181d0f5df6fcfd.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/422940fa36b41e084.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/f0dc96af2e57580ab.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/19101933c20d8ae07.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/c4e1b49f7ea26794f.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/e489ea08b1aa98725.jpg',
            ],
             [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/810837e555d42de6e.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/208ebd14a6499ba8d.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/ce8e009dcaf21a0e8.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/eb7181d0f5df6fcfd.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/422940fa36b41e084.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/f0dc96af2e57580ab.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/19101933c20d8ae07.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/c4e1b49f7ea26794f.jpg',
            ],
            [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/e489ea08b1aa98725.jpg',
            ],
             [
                'id_product' => rand(1,5),
                'link' => 'https://s3.ap-southeast-1.amazonaws.com/yamlivevideo/810837e555d42de6e.jpg',
            ],
        ]);
    }
}
