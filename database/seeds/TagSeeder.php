<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([

            [
                'name' => 'PixioStudio',
                'slug' => 'pixio2tudio',
            
            ],

            [
                'name' => 'PixioStudio2',
                'slug' => 'pixioetudio',
            
            ],

            [
                'name' => 'PixioStudio3',
                'slug' => 'pixioftudio',
            
            ],

        ]);
    }
}
