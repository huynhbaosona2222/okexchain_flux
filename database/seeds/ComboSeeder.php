<?php

use Illuminate\Database\Seeder;

class ComboSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('combos')->insert([

            [
                'name' => 'Summer Como 02 ',
                'products_id' => '["1","3"]',
                // 'qty' => '10',
                'qty_sold' => '2',
                'new_price' => '200',
                'description' => 'desciption demo',
                'slug' => 'pixio_studio1',
            ],
            
            [
                'name' => 'Summer Como 03',
                'products_id' => '["1","4","5"]',
                // 'qty' => '10',
                'qty_sold' => '2',
                'new_price' => '200',
                'description' => 'desciption demo',
                'slug' => 'pixio_studiod',
            ],
            
            [
                'name' => 'Summer Como 022',
                'products_id' => '["1","5"]',
                // 'qty' => '10',
                'qty_sold' => '2',
                'new_price' => '200',
                'description' => 'desciption demo',
                'slug' => 'pixio_studios',
            ],
            
            [
                'name' => 'Summer Como  02 12',
                'products_id' => '["1","2","4"]',
                // 'qty' => '10',
                'qty_sold' => '2',
                'new_price' => '200',
                'description' => 'desciption demo',
                'slug' => 'pixio_studiosd',
            ],

           
        ]);
    }
}
