<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([

            [
                'name' => 'Boxy7 T-Shirt with Roll Sleeve',
                'price' => 120000,
                'id_category' => rand(1,3),
                'description' => "Phasellus egestas nisi nisi, lobortis ultricies risus semper nec. Vestibulum pharetra ac ante ut pellentesque. Curabitur fringilla dolor quis lorem accumsan, vitae molestie urna dapibus. Pellentesque porta est ac neque bibendum viverra. Vivamus lobortis magna ut interdum laoreet. Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula.",
                'discount' => 0,
                'qty_available' => 100,
                'qty_sold' => 10,
                'active' => true,
                'slug' => 'boxy7-t-shirt-with-roll-sleeve-1583382941'.(rand(0000,9999)),
            
            ],
            [
                'name' => 'Boxy2 T-Shirt with Roll Sleeve',
                'price' => 120000,
                'id_category' => rand(1,3),
                'description' => "Phasellus egestas nisi nisi, lobortis ultricies risus semper nec. Vestibulum pharetra ac ante ut pellentesque. Curabitur fringilla dolor quis lorem accumsan, vitae molestie urna dapibus. Pellentesque porta est ac neque bibendum viverra. Vivamus lobortis magna ut interdum laoreet. Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula.",
                'discount' => 0,
                'qty_available' => 100,
                'qty_sold' => 10,
                'active' => true,
                'slug' => 'boxy7-t-shirt-with-roll-sleeve-1583382941'.(rand(0000,9999)),

            
            ],
            [
                'name' => 'RUFFLED MOHAIR AND WOOL BLEND SWEATER',
                'price' => 120000,
                'id_category' => rand(1,3),
                'description' => "Phasellus egestas nisi nisi, lobortis ultricies risus semper nec. Vestibulum pharetra ac ante ut pellentesque. Curabitur fringilla dolor quis lorem accumsan, vitae molestie urna dapibus. Pellentesque porta est ac neque bibendum viverra. Vivamus lobortis magna ut interdum laoreet. Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula.",
                'discount' => 0,
                'qty_available' => 100,
                'qty_sold' => 10,
                'active' => true,
                'slug' => 'boxy7-t-shirt-with-roll-sleeve-1583382941'.(rand(0000,9999)),

            
            ],
            [
                'name' => 'SPARKLY PADDED HEADBAND',
                'price' => 120000,
                'id_category' => rand(1,3),
                'description' => "Phasellus egestas nisi nisi, lobortis ultricies risus semper nec. Vestibulum pharetra ac ante ut pellentesque. Curabitur fringilla dolor quis lorem accumsan, vitae molestie urna dapibus. Pellentesque porta est ac neque bibendum viverra. Vivamus lobortis magna ut interdum laoreet. Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula.",
                'discount' => 0,
                'qty_available' => 100,
                'qty_sold' => 10,
                'active' => true,
                'slug' => 'boxy7-t-shirt-with-roll-sleeve-1583382941'.(rand(0000,9999)),

            
            ],
            [
                'name' => 'SPARKLY PADDED HEADBAND as',
                'price' => 120000,
                'id_category' => rand(1,3),
                'description' => "Phasellus egestas nisi nisi, lobortis ultricies risus semper nec. Vestibulum pharetra ac ante ut pellentesque. Curabitur fringilla dolor quis lorem accumsan, vitae molestie urna dapibus. Pellentesque porta est ac neque bibendum viverra. Vivamus lobortis magna ut interdum laoreet. Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula.",
                'discount' => 0,
                'qty_available' => 100,
                'qty_sold' => 10,
                'active' => true,
                'slug' => 'boxy7-t-shirt-with-roll-sleeve-1583382941'.(rand(0000,9999)),

            
            ],

          
        ]);
    }
}
