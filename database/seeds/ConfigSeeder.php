<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([

            [
                'conf_name' => 'Countdown time',
                'description' => 'This is countdown time in payment page. <br> Unit: (second) <br> Range: 60-1200',
                'value' => '300',
            
            ],

            
        ]);
    }
}
