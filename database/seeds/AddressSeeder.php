<?php

use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addresses')->insert([

            [
                'creator_id' => 2,
                'customer_name' => 'Nhung Nguyen',
                'address' => 'Binh Thanh - Ho Chi Minh - Viet Nam',
                'address_detail' => '41 To Ky, Binh Thanh - Ho Chi Minh - Viet Nam',
                'phone_number' => 84932931,
            ],

            [
                'creator_id' => 2,
                'customer_name' => 'Tran Nam ',
                'address' => 'Binh Thanh - Ho Chi Minh - Viet Nam',
                'address_detail' => '41 To Ky, Binh Thanh - Ho Chi Minh - Viet Nam',
                'phone_number' => 84932122,
            ],

            [
                'creator_id' => 2,
                'customer_name' => 'Ane Kim Chi',
                'address' => 'Binh Thanh - Ho Chi Minh - Viet Nam',
                'address_detail' => '41 To Ky, Binh Thanh - Ho Chi Minh - Viet Nam',
                'phone_number' => 84932931,
            ],

            [
                'creator_id' => 2,
                'customer_name' => 'Nhat Thi',
                'address' => 'Binh Thanh - Ho Chi Minh - Viet Nam',
                'address_detail' => '41 To Ky, Binh Thanh - Ho Chi Minh - Viet Nam',
                'phone_number' => 846315312,
            ],
        ]);
    }
}
