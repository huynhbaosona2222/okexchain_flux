import ProductRaw from "./ProductRaw";
import ImageList from "./ProductImageList";

/**
 * Nếu trong database dùng class khác thì sửa ConstructorProps ở trên và sửa constructor ở dưới.
 * Cố gắng đừng chỉnh sửa các property của class vì sẽ ảnh hưởng đến nhiều component.
 */
class Product {
  id: string;
  slug: string;
  name: string;
  description: string;
  price: number;
  oldPrice: number | null;
  sizeList: string[];
  remainCount: number;
  discountByNumber: { count: number; discount: number }[];
  discountByAccessories: {
    discount: number;
    accessoryList: { id: string; slug: string; previewURL: string }[];
  } | null;
  colourList: ImageList[];

  constructor({
    id,
    slug,
    name,
    description,
    price,
    oldPrice,
    colourList,
    sizeList,
    remainCount,
    discountByNumber,
    discountByAccessories
  }: ProductRaw) {
    this.id = id;
    this.slug = slug;
    this.name = name;
    this.description = description;
    this.price = price;
    this.oldPrice = oldPrice || null;
    this.colourList = colourList;
    this.sizeList = sizeList;
    this.remainCount = remainCount;
    this.discountByNumber = discountByNumber;
    this.discountByAccessories = discountByAccessories || null;
  }
}

export default Product;
