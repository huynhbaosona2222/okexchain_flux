import ShippingInfoRaw from "./ShippingInfoRaw";

class ShippingInfo {
  fullname: string;
  email: string | undefined;
  phone1: string;
  phone2: string | undefined;
  city: string;
  district: string;
  ward: string;
  address: string;
  shippingMethod: number;
  shippingNote: string;
  invoice:
    | { companyName: string; taxId: string; email: string; address: string }
    | undefined;
  paymentMethod: number;

  constructor({
    fullname,
    email,
    phone1,
    phone2,
    city,
    district,
    ward,
    address,
    shippingMethod,
    shippingNote,
    invoice,
    paymentMethod
  }: ShippingInfoRaw) {
    this.fullname = fullname;
    this.email = email;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.city = city;
    this.district = district;
    this.ward = ward;
    this.address = address;
    this.shippingMethod = shippingMethod;
    this.shippingNote = shippingNote;
    this.invoice = invoice;
    this.paymentMethod = paymentMethod;
  }
}

export default ShippingInfo;
