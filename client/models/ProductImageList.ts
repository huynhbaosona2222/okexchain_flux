interface ImageList {
  colour: string;
  imageList: string[];
}

export default ImageList;
