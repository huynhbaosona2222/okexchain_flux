import CartProductRaw from "./CartProductRaw";
import ShippingInfoRaw from "./ShippingInfoRaw";

interface OrderRaw {
  id: string;
  date: Date;
  productList: CartProductRaw[];
  shippingInfo: ShippingInfoRaw;
}

export default OrderRaw;
