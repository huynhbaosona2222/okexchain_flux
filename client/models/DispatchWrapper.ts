import { Dispatch } from "react";

type DispatchWrapper<T> = Dispatch<T | ((prev: T) => T)>;
export default DispatchWrapper;
