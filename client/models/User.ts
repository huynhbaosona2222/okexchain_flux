import UserRaw from "./UserRaw";

class User {
  fullname: string;
  email: string;
  phone: string;
  address: string;
  gender: number;

  constructor({
    name: fullname,
    email,
    phone_number: phone,
    address,
    sex: gender
  }: UserRaw) {
    this.fullname = fullname;
    this.email = email;
    this.phone = phone;
    this.address = address;
    this.gender = gender;
  }
}

export default User;
