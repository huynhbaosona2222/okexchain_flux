interface UserRaw {
  name: string;
  address: string;
  phone_number: string;
  email: string;
  sex: 0 | 1;
}

export default UserRaw;
