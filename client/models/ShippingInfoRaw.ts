interface ShippingInfoRaw {
  fullname: string;
  email?: string;
  phone1: string;
  phone2?: string;
  city: string;
  district: string;
  ward: string;
  address: string;
  shippingMethod: number;
  shippingNote: string;
  invoice?: {
    companyName: string;
    taxId: string;
    email: string;
    address: string;
  };
  paymentMethod: number;
}

export default ShippingInfoRaw;
