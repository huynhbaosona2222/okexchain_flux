import OrderRaw from "./OrderRaw";
import CartProduct from "./CartProduct";
import ShippingInfo from "./ShippingInfo";

class Order {
  id: string;
  date: Date;
  productList: CartProduct[];
  shippingInfo: ShippingInfo;

  constructor({ id, date, productList, shippingInfo }: OrderRaw) {
    this.id = id;
    this.date = date;
    this.productList = productList.map((product) => new CartProduct(product));
    this.shippingInfo = new ShippingInfo(shippingInfo);
  }

  get shippingDate(): [Date, Date] {
    let dif;
    switch (this.shippingInfo.shippingMethod) {
      case 0:
        dif = 7;
        break;
      case 1:
        dif = 14;
        break;
      default:
        dif = 21;
        break;
    }
    let x = new Date(this.date);
    let y = new Date(this.date);
    x.setDate(x.getDate() + dif);
    y.setDate(y.getDate() + dif + 2);
    return [x, y];
  }
}

export default Order;
