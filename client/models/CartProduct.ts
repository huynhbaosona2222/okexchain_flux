import Product from "./Product";
import CartProductRaw from "./CartProductRaw";

class CartProduct {
  product: Product;
  colour: string;
  size: string;
  quantity: number;
  constructor({ product, colour, size, quantity }: CartProductRaw) {
    this.product = product instanceof Product ? product : new Product(product);
    this.colour = colour;
    this.size = size;
    this.quantity = quantity;
  }
  get previewImage(): string | null {
    let ans = this.product.colourList.find(
      ({ colour }) => colour === this.colour
    );
    if (ans && ans.imageList.length > 0) {
      return ans.imageList[0];
    }
    return null;
  }
}

export default CartProduct;
