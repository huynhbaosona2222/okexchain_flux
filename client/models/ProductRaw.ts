import ImageList from "./ProductImageList";

interface ProductRaw {
  id: string;
  slug: string;
  name: string;
  description: string;
  price: number;
  oldPrice?: number;

  // Each colour has a list of preview images in that colour
  colourList: ImageList[];

  sizeList: string[];
  remainCount: number; // number of remaining products

  // Discount by bought number: "Giảm {discount}% khi mua {count} sản phẩm"
  discountByNumber: { count: number; discount: number }[];

  // Discount by accessories: "Giảm 10% khi mua các sản phẩm sau"
  discountByAccessories?: {
    discount: number;
    accessoryList: { id: string; slug: string; previewURL: string }[];
  };
}

export default ProductRaw;
