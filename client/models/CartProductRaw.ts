import ProductRaw from "./ProductRaw";
import Product from "./Product";

interface CartProductRaw {
  product: ProductRaw | Product;
  colour: string;
  size: string;
  quantity: number;
}

export default CartProductRaw;
