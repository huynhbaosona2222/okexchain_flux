import React, { useState, useEffect } from "react";
import padNumber from "../../helpers/padNumber";

function CheckoutTimer() {
  let [remain, setRemain] = useState(10 * 60); // 10 minutes
  useEffect(function () {
    let interval = setInterval(function () {
      setRemain(function (current) {
        if (current === 0) {
          clearTimeout(interval);
          return current;
        }
        return current - 1;
      });
    }, 1000);
    return function () {
      clearInterval(interval);
    };
  }, []);

  let minute = Math.floor(remain / 60);
  let second = remain % 60;
  return (
    <div className="checkout-timer">
      <div className="checkout-timer__rect">
        <span className="value">{padNumber(minute)}</span>
        <span className="unit">minute</span>
      </div>
      <div className="checkout-timer__rect">
        <span className="value">{padNumber(second)}</span>
        <span className="unit">second</span>
      </div>
      <p className="checkout-timer__desc">
        Đơn hàng của bạn sẽ được lưu lại trong {minute}m
        {second > 0 ? " " + second + "s" : ""} nữa
      </p>
      <hr className="checkout-timer__divider" />
    </div>
  );
}

export default CheckoutTimer;
