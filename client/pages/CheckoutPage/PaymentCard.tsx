import React from "react";

interface PaymentCardProps {
  imageUrl: string;
  title: string;
  description: string;
}

function PaymentCard({ imageUrl, title, description }: PaymentCardProps) {
  return (
    <label className="payment-card--wrapper">
      <input type="radio" name="paymentMethod" hidden />
      <div className="payment-card">
        <img src={imageUrl} alt={title} />
        <div>
          <p>{title}</p>
          <p>{description}</p>
        </div>
      </div>
    </label>
  );
}

export default PaymentCard;
