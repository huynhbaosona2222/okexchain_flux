import React from "react";
import ModalProps from "../../models/ModalProps";
import useModal from "../../hooks/useModal";

function CheckoutResultPopup({ show, onClose }: ModalProps) {
  let ref = useModal(onClose);

  return (
    <div className={"my-modal--wrapper" + (show ? " active" : "")} ref={ref}>
      <div className="my-modal checkout-result">
        <button type="button" onClick={onClose}>
          &times;
        </button>
        <h2>Đặt hàng thành công</h2>
        <p>
          Đơn hàng của bạn đã được đặt thành công vui lòng kiếm tra chi tiết đơn
          hàng đã đặt trong mục <strong>TÀI KHOẢN</strong> của bạn.
        </p>
      </div>
    </div>
  );
}

export default CheckoutResultPopup;
