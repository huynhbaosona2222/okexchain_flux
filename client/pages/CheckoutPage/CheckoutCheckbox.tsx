import React, { HTMLProps } from "react";

function CheckoutCheckbox({ children, ...props }: HTMLProps<HTMLInputElement>) {
  return (
    <label className="checkout-checkbox">
      <input {...props} hidden />
      <span className="checkout-checkbox__mark">
        <svg
          width="2rem"
          height="1.8rem"
          viewBox="0 0 20 18"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M19.2563 1.2578C18.4194 0.578003 17.2153 0.676213 16.5663 1.47532L8.01015 12.024L3.54423 6.22072C2.87805 5.35577 1.63657 5.19397 0.77157 5.85939C-0.0941943 6.52518 -0.255944 7.76705 0.410236 8.6332L6.40455 16.4225C7.07073 17.2875 8.3126 17.4497 9.17798 16.7839C9.42471 16.594 9.60717 16.3543 9.73655 16.0924L19.5981 3.93532C20.2468 3.13506 20.0935 1.93683 19.2563 1.2578Z"
            fill="white"
          />
        </svg>
      </span>
      <div className="spacer" />
      <div>{children}</div>
    </label>
  );
}

export default CheckoutCheckbox;
