import React, { useState, memo, ReactNode } from "react";

let Tooltip = memo(function Tooltip({ children }: { children: ReactNode }) {
  let [active, setActive] = useState(false);
  return (
    <div
      className={"my-tooltip" + (active ? " active" : "")}
      onMouseLeave={() => setActive(false)}>
      <button type="button" onMouseOver={() => setActive(true)}>
        <svg
          width="2.6rem"
          height="2.6rem"
          viewBox="0 0 26 26"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M13 0C5.83136 0 0 5.83136 0 13C0 20.1686 5.83136 26 13 26C20.1686 26 26 20.1686 26 13C26 5.83136 20.1686 0 13 0ZM12.4583 21.6667C11.5622 21.6667 10.8333 20.9378 10.8333 20.0417C10.8333 19.1456 11.5622 18.4167 12.4583 18.4167C13.3544 18.4167 14.0833 19.1456 14.0833 20.0417C14.0833 20.9378 13.3544 21.6667 12.4583 21.6667ZM14.0833 15.0588V16.7917C14.0833 17.0911 13.841 17.3334 13.5416 17.3334H11.375C11.0756 17.3334 10.8333 17.0911 10.8333 16.7917V14.0834C10.8333 12.8889 11.8056 11.9167 13 11.9167C14.1944 11.9167 15.1667 10.9444 15.1667 9.75C15.1667 8.55557 14.1944 7.58332 13 7.58332C11.8056 7.58332 10.8333 8.55557 10.8333 9.75V10.2917C10.8333 10.5911 10.591 10.8334 10.2916 10.8334H8.125C7.82559 10.8334 7.58332 10.5911 7.58332 10.2917V9.75C7.58332 6.7634 10.0134 4.33332 13 4.33332C15.9866 4.33332 18.4167 6.76345 18.4167 9.75C18.4167 12.3494 16.5854 14.552 14.0833 15.0588Z"
            fill={active ? "#F47500" : "#F4BF00"}
          />
        </svg>
      </button>
      <div className="my-tooltip__content">{children}</div>
    </div>
  );
});

export default Tooltip;
