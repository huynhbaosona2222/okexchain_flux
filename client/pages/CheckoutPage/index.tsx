import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import CheckoutTimer from "./CheckoutTimer";
import CheckoutForm from "./CheckoutForm";
import App from "../../components/App";
import CheckoutCart from "./CheckoutCart";

function CheckoutPage() {
  return (
    <App>
      <Header route="" />
      <section className="checkout">
        <div>
          <CheckoutTimer />
          <CheckoutForm />
        </div>
        <CheckoutCart />
      </section>
      <Footer />
    </App>
  );
}

export default CheckoutPage;
