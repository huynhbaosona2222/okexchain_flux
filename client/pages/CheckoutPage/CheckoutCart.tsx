import React, { useContext } from "react";
import CartProductCard from "../../components/CartProductCard";
import CartContext from "../../contexts/CartContext";
import computeTotal from "../../helpers/computeTotal";
import formatPrice from "../../helpers/formatPrice";

function CheckoutCart() {
  let { cartProductList } = useContext(CartContext);

  let totalPrice = 0;
  for (let cartProduct of cartProductList) {
    totalPrice += computeTotal(
      cartProduct.product.price,
      cartProduct.quantity,
      cartProduct.product
    );
  }

  return (
    <div className="checkout-cart">
      <div className="checkout-cart__head">
        <h2>Giỏ hàng</h2>
      </div>
      <div className="checkout-cart__body">
        {cartProductList.map(function (cartProduct, idx) {
          return (
            <CartProductCard
              key={idx}
              cartProduct={cartProduct}
              compact={true}
            />
          );
        })}
      </div>
      <div className="checkout-cart__foot">
        <hr />
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Tổng cộng</span>
          <span>{formatPrice(totalPrice)} VND</span>
        </div>
        <div style={{ height: "0.9rem" }} />
        <div className="checkout-cart__foot-info">
          <span />
          <p className="price-vat">(Đã bao gồm phí VAT)</p>
        </div>
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Mã giảm giá</span>
          <form className="voucher-form">
            <input type="text" name="voucher" />
            <button type="submit">Áp dụng</button>
          </form>
        </div>
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Phí giao hàng</span>
          <span>{formatPrice(20000)} VND</span>
        </div>
        <div className="spacer" />

        <hr />
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Tổng tiền thanh toán</span>
          <span className="checkout-cart__total">
            {formatPrice(totalPrice + 20000)} VND
          </span>
        </div>
        <div className="spacer" />

        <hr />
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Thời gian giao hàng: 01/01/2001</span>
        </div>
        <div className="spacer" />
        <div className="checkout-cart__foot-info">
          <span>Đặt hàng qua điện thoại: 028-6272-4728</span>
        </div>
      </div>
    </div>
  );
}

export default CheckoutCart;
