import React, { useState, Fragment } from "react";
import CheckoutCheckbox from "./CheckoutCheckbox";
import PaymentCard from "./PaymentCard";
import Tooltip from "./Tooltip";
import CheckoutResultPopup from "./CheckoutResultPopup";

function CheckoutForm() {
  // Manage "Xuất hóa đơn điện tử" option
  let [genInvoice, setGenInvoice] = useState(false);

  let [checkbox1, setCheckbox1] = useState(false);
  let [checkbox2, setCheckbox2] = useState(false);

  let [resultActive, setResultActive] = useState(false);

  return (
    <form className="checkout-form--wrapper">
      <h2>Thông tin nhận hàng</h2>
      <div className="checkout-form">
        <label className="checkout-form__field">
          <div className="name required">
            <span>Họ và tên</span>
          </div>
          <input
            type="text"
            name="fullname"
            className="value"
            placeholder="Họ tên người nhận"
            required
          />
        </label>
        <label className="checkout-form__field">
          <div className="name">
            <span>Email</span>
          </div>
          <input
            type="text"
            name="email"
            className="value"
            placeholder="Email"
          />
        </label>
        <label className="checkout-form__field">
          <div className="name required">
            <span>Điện thoại 1</span>
          </div>
          <input
            type="text"
            name="phone1"
            className="value"
            inputMode="numeric"
            pattern="[0-9]*"
            placeholder="Vui lòng nhập chính xác SĐT"
            required
          />
        </label>
        <label className="checkout-form__field">
          <div className="name">
            <span>Điện thoại 2</span>
          </div>
          <input
            type="text"
            name="phone2"
            className="value"
            inputMode="numeric"
            pattern="[0-9]*"
            placeholder="Trường hợp SĐT 1 không liên lạc được hoặc bị sai."
          />
        </label>
        <label className="checkout-form__field">
          <div className="name required">
            <span>Tỉnh/Thành phố</span>
          </div>
          <select name="city" className="value" required>
            <option value="" disabled selected hidden>
              -- Chọn tỉnh thành --
            </option>
          </select>
        </label>
        <label className="checkout-form__field">
          <div className="name required">
            <span>Quận/Huyện</span>
          </div>
          <select name="district" className="value" required>
            <option value="" disabled selected hidden>
              -- Chọn quận/huyện --
            </option>
          </select>
        </label>
        <label className="checkout-form__field">
          <div className="name required">
            <span>Phường/Xã</span>
          </div>
          <select name="ward" className="value" required>
            <option value="" disabled selected hidden>
              -- Chọn phường/xã --
            </option>
          </select>
        </label>
        <label className="checkout-form__field">
          <div className="name required">
            <span>Địa chỉ chi tiết</span>
          </div>
          <input
            type="text"
            name="address"
            className="value"
            placeholder="Địa chỉ nhận hàng"
            required
          />
        </label>
        <label className="checkout-form__field">
          <div className="name shipping-tooltip">
            <span>Vận chuyển</span>
            <Tooltip>
              <p>Giao hàng nhanh</p>
              <p>
                Hà Nội hoặc Hồ Chí Minh sẽ được giao trong 3-4 tiếng. Đi tỉnh sẽ
                được giao hàng trong 1-2 ngày
              </p>
            </Tooltip>
          </div>
          <select name="shippingMethod" className="value">
            <option value={0}>Giao hàng tiêu chuẩn</option>
          </select>
        </label>
        <label className="checkout-form__field">
          <div className="name">
            <span>Ghi chú giao hàng</span>
          </div>
          <select name="shippingNote" className="value">
            <option>Chọn hoặc nhập ghi chú</option>
            <option>Giao giờ hành chính</option>
            <option>Đặt hàng hộ người thân</option>
            <option>Thêm ghi chú</option>
          </select>
        </label>
        <label className="checkout-form__field">
          <div className="name">
            <span>Xuất hóa đơn điện tử</span>
          </div>
          <div className="value--wrapper">
            <select
              name="electronicInvoice"
              className="value"
              value={genInvoice ? 1 : 0}
              onChange={(e) => setGenInvoice(e.target.value === "1")}>
              <option value="0">Không</option>
              <option value="1">Có</option>
            </select>
            {genInvoice && (
              <p className="description">
                Vui lòng điền thông tin xuất hóa đơn vào ô bên cạnh
              </p>
            )}
          </div>
        </label>
        {genInvoice && (
          <Fragment>
            <label className="checkout-form__field">
              <div className="name">
                <span>Tên công ty</span>
              </div>
              <input
                type="text"
                name="companyName"
                className="value"
                placeholder="Nhập đầy đủ tên công ty"
              />
            </label>
            <div className="checkout-checkboxes">
              <CheckoutCheckbox
                type="checkbox"
                checked={checkbox1}
                onChange={(e) => {
                  setCheckbox1((e.target as HTMLInputElement).checked);
                }}>
                <div className="checkbox-label">
                  Chọn vào đây nếu bạn muốn được gọi tư vấn
                  <Tooltip>
                    Nếu quý khách có bất cứ thắc mắc nào về đơn hàng hoặc chính
                    sách vận chuyển. Hãy chọn vào đây để chúng tôi liên lạc và
                    giải đáp thắc mắc của quý khách. Cảm ơn!
                  </Tooltip>
                </div>
              </CheckoutCheckbox>
              <CheckoutCheckbox
                checked={checkbox2}
                onChange={(e) => {
                  setCheckbox2((e.target as HTMLInputElement).checked);
                }}>
                <div className="checkbox-label">Lưu lại thông tin mua hàng</div>
              </CheckoutCheckbox>
            </div>
            <div className="checkout-form__company">
              <label className="checkout-form__field">
                <div className="name">
                  <span>Mã số thuế</span>
                </div>
                <input
                  type="text"
                  name="companyTaxId"
                  className="value"
                  placeholder="Mã số thuế"
                />
              </label>
              <label className="checkout-form__field">
                <div className="name">
                  <span>Email</span>
                </div>
                <input
                  type="text"
                  name="companyEmail"
                  className="value"
                  placeholder="Nhập email để nhận hóa đơn điện tử"
                />
              </label>
              <label className="checkout-form__field">
                <div className="name">
                  <span>Địa chỉ công ty</span>
                </div>
                <input
                  type="text"
                  name="companyAddress"
                  className="value"
                  placeholder="Nhập địa chỉ đầy đủ của công ty"
                />
              </label>
            </div>
          </Fragment>
        )}
      </div>
      {!genInvoice && (
        <div className="checkout-checkboxes">
          <CheckoutCheckbox
            type="checkbox"
            checked={checkbox1}
            onChange={(e) => {
              setCheckbox1((e.target as HTMLInputElement).checked);
            }}>
            <div className="checkbox-label">
              Chọn vào đây nếu bạn muốn được gọi tư vấn
              <Tooltip>
                Nếu quý khách có bất cứ thắc mắc nào về đơn hàng hoặc chính sách
                vận chuyển. Hãy chọn vào đây để chúng tôi liên lạc và giải đáp
                thắc mắc của quý khách. Cảm ơn!
              </Tooltip>
            </div>
          </CheckoutCheckbox>
          <CheckoutCheckbox
            checked={checkbox2}
            onChange={(e) => {
              setCheckbox2((e.target as HTMLInputElement).checked);
            }}>
            <div className="checkbox-label">Lưu lại thông tin mua hàng</div>
          </CheckoutCheckbox>
        </div>
      )}
      <div className="checkout-payment">
        <h2>Phương thức thanh toán</h2>
        <div className="checkout-payment-list">
          <PaymentCard
            imageUrl="/public/images/checkout-payment-01.svg"
            title="Thanh toán khi nhận hàng"
            description="Quý khách sẽ thanh toán bằng tiền mặt khi nhận hàng"
          />
          <PaymentCard
            imageUrl="/public/images/checkout-payment-02.svg"
            title="Thanh toán bằng chuyển khoản"
            description="Chuyển khoản trực tiếp đến tài khoản của Chillax"
          />
          <PaymentCard
            imageUrl="/public/images/checkout-payment-03.svg"
            title="Thanh toán online"
            description="Thanh toán trực tuyến qua cổng thanh toán VNPay"
          />
        </div>
      </div>
      <div className="checkout-action">
        <button type="button" onClick={() => (location.href = "/shop")}>
          <svg
            width="1.2rem"
            height="1.9rem"
            viewBox="0 0 12 19"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M0.444197 9.7804L9.34445 18.6805C9.5503 18.8865 9.8251 19 10.1181 19C10.4111 19 10.6859 18.8865 10.8918 18.6805L11.5472 18.0252C11.9737 17.5982 11.9737 16.9042 11.5472 16.4779L4.07344 9.00415L11.5555 1.52209C11.7613 1.31608 11.875 1.04145 11.875 0.748604C11.875 0.455435 11.7613 0.180801 11.5555 -0.0253753L10.9 -0.680492C10.694 -0.886505 10.4194 -1 10.1264 -1C9.83339 -1 9.55859 -0.886505 9.35274 -0.680492L0.444197 8.22773C0.237857 8.4344 0.124525 8.71033 0.125176 9.00366C0.124525 9.29813 0.237857 9.5739 0.444197 9.7804Z"
              fill="#111010"
            />
          </svg>
          TIẾP TỤC MUA HÀNG
        </button>
        <button type="submit" onClick={() => setResultActive(true)}>
          ĐẶT HÀNG
          <svg
            width="1.2rem"
            height="1.9rem"
            viewBox="0 0 12 19"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              d="M11.5558 9.7804L2.65555 18.6805C2.4497 18.8865 2.1749 19 1.8819 19C1.58889 19 1.3141 18.8865 1.10824 18.6805L0.452802 18.0252C0.0263019 17.5982 0.0263019 16.9042 0.452803 16.4779L7.92656 9.00415L0.444511 1.52209C0.238659 1.31608 0.125002 1.04145 0.125002 0.748604C0.125002 0.455435 0.238659 0.180801 0.444511 -0.0253753L1.09995 -0.680492C1.30597 -0.886505 1.5806 -1 1.87361 -1C2.16661 -1 2.44141 -0.886505 2.64726 -0.680492L11.5558 8.22773C11.7621 8.4344 11.8755 8.71033 11.8748 9.00366C11.8755 9.29813 11.7621 9.5739 11.5558 9.7804Z"
              fill="white"
            />
          </svg>
        </button>
      </div>
      <CheckoutResultPopup
        show={resultActive}
        onClose={() => setResultActive(false)}
      />
    </form>
  );
}

export default CheckoutForm;
