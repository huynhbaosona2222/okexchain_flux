import React from "react";

function ShopItemDescription({ description }: { description: string }) {
  return (
    <div className="shop-item__desc ">
      <h2>Description</h2>
      <p>{description}</p>
    </div>
  );
}

export default ShopItemDescription;
