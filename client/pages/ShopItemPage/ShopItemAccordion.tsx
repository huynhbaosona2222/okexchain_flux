import React from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";

function ShopItemAccordion() {
  return (
    <Accordion allowMultipleExpanded={true} allowZeroExpanded={true}>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>
            Chính sách hoàn trả và hoàn tiền
          </AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem a been the industry&apos;s standard dummy ever since
            the 1500s, when an unknown printer took a galley of type and
            scrambled make a type specimen book. It has survived only five
            centuries, but also the leap ielectronic typesetting, remaining
            essentially unchang. It was popularised in the 1960s with the of .
          </p>
        </AccordionItemPanel>
      </AccordionItem>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>Thông tin giao hàng</AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem a been the industry&apos;s standard dummy ever since
            the 1500s, when an unknown printer took a galley of type and
            scrambled make a type specimen book. It has survived only five
            centuries, but also the leap ielectronic typesetting, remaining
            essentially unchang. It was popularised in the 1960s with the of .
          </p>
        </AccordionItemPanel>
      </AccordionItem>
    </Accordion>
  );
}

export default ShopItemAccordion;
