import React, {
  Fragment,
  useState,
  useEffect,
  useContext,
  useCallback
} from "react";
import Product from "../../models/Product";
import formatPrice from "../../helpers/formatPrice";
import SizeTablePopup from "../../components/SizeTablePopup";
import computeTotal from "../../helpers/computeTotal";
import padNumber from "../../helpers/padNumber";
import CartContext from "../../contexts/CartContext";
import CartProduct from "../../models/CartProduct";
import ShopItemDescription from "./ShopItemDescription";
import ShopItemDiscountAccessories from "./ShopItemDiscountAccessories";

function ShopItemInfo({
  product,
  onColourChange
}: {
  product: Product;
  onColourChange: (cid: number) => void;
}) {
  let [sizeTableActive, setSizeTableActive] = useState(false);
  let { addProduct } = useContext(CartContext);

  let [colourIndex, setColourIndex] = useState(0);
  let [size, setSize] = useState("");
  let [count, setCount] = useState(1);
  useEffect(
    function() {
      onColourChange(colourIndex);
    },
    [onColourChange, colourIndex]
  );

  let handleBuy = useCallback(
    function() {
      // TODO: handle buy now
      let payload = {
        productId: product.id,
        colour: product.colourList[colourIndex].colour,
        size,
        quantity: count
      };
      console.log(payload);
    },
    [colourIndex, size, count, product]
  );

  return (
    <Fragment>
      <h2 className="shop-item__section-title productname">{product.name}</h2>
      <p className="shop-item__id">SKU: {product.id}</p>

      <div className="shop-item__price">
        {product.oldPrice && (
          <span className="old">
            {formatPrice(computeTotal(product.oldPrice, count, product))} VND
          </span>
        )}
        <span>
          {formatPrice(computeTotal(product.price, count, product))} VND
        </span>
      </div>

      <p className="shop-item__divider"></p>

      <div className="mobile-only">
        <ShopItemDescription description={product.description} />

        <p className="shop-item__divider"></p>
      </div>

      <h2 className="shop-item__section-title">Màu sắc</h2>
      <div className="shop-item__colour-list">
        {product.colourList.map(function({ colour }, idx) {
          let className =
            "shop-item__colour" + (idx === colourIndex ? " active" : "");
          return (
            <button
              type="button"
              key={colour}
              className={className}
              style={{ backgroundColor: colour }}
              onClick={() => setColourIndex(idx)}
            />
          );
        })}
      </div>

      <div className="shop-item__size">
        <h2 className="shop-item__section-title">Size</h2>
        <button
          type="button"
          className="shop-item__size-table-trigger"
          onClick={() => setSizeTableActive(true)}>
          Bảng size
        </button>
        <SizeTablePopup
          show={sizeTableActive}
          onClose={() => setSizeTableActive(false)}
        />
      </div>
      <select
        name="size"
        onChange={(e) => {
          setSize(e.target.value);
        }}>
        <option value="" disabled hidden selected>
          Select
        </option>
        {product.sizeList.map(function(_size) {
          return (
            <option key={_size} value={_size} selected={_size === size}>
              {_size}
            </option>
          );
        })}
      </select>

      <h2 className="shop-item__section-title">Số lượng</h2>
      <div className="shop-item__count">
        <button
          type="button"
          onClick={() => setCount((p) => p - 1)}
          disabled={count === 1}>
          -
        </button>
        <span>{padNumber(count)}</span>
        <button
          type="button"
          onClick={() => setCount((p) => p + 1)}
          disabled={count === product.remainCount}>
          +
        </button>
      </div>

      <div className="shop-item__discount-number--wrapper">
        <div className="shop-item__discount-number">
          {product.discountByNumber.map(function({ count, discount }, idx) {
            return (
              <p key={idx}>
                <span>GIẢM {discount}%</span>
                <span>Khi mua {count} sản phẩm</span>
              </p>
            );
          })}
        </div>
      </div>

      <div className="mobile-only">
        {product.discountByAccessories && (
          <ShopItemDiscountAccessories {...product.discountByAccessories} />
        )}
      </div>

      <div className="shop-item__action">
        <button
          type="button"
          disabled={size === ""}
          onClick={() => {
            addProduct(
              new CartProduct({
                product,
                colour: product.colourList[colourIndex].colour,
                size,
                quantity: count
              })
            );
          }}>
          THÊM VÀO GIỎ
        </button>
        <button type="button" onClick={handleBuy}>
          MUA NGAY
        </button>
      </div>
    </Fragment>
  );
}

export default ShopItemInfo;
