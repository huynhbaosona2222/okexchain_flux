import React, { useState } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Product from "../../models/Product";

import ProductSlider from "../../components/ProductSlider";
import ShopItemPreviewDesc from "./ShopItemPreviewDesc";
import ProductRaw from "../../models/ProductRaw";
import ShopItemInfo from "./ShopItemInfo";
import ShopItemAccordion from "./ShopItemAccordion";
import App from "../../components/App";
import ShopItemDiscountAccessories from "./ShopItemDiscountAccessories";
import ShopItemDescription from "./ShopItemDescription";

declare global {
  interface Window {
    __PRELOADED_PRODUCT__: ProductRaw;
    __PRELOADED_RELATED_ITEM_LIST: ProductRaw[];
  }
}

interface ShopItemProps {
  product?: ProductRaw;
  relatedItemList?: ProductRaw[];
}

function ShopItemPage({
  product: preloadedProduct,
  relatedItemList: preloadedRelatedItemList
}: ShopItemProps) {
  let [product] = useState<Product>(function() {
    if (typeof preloadedProduct !== "undefined") {
      return new Product(preloadedProduct);
    }
    return new Product(window.__PRELOADED_PRODUCT__);
  });

  let [relateProductList] = useState<Product[]>(function() {
    let arr = [];
    if (typeof preloadedRelatedItemList !== "undefined") {
      arr = preloadedRelatedItemList;
    } else {
      arr = window.__PRELOADED_RELATED_ITEM_LIST;
    }
    return arr.map(function(product) {
      return new Product(product);
    });
  });

  let [colourIndex, setColourIndex] = useState(0);

  return (
    <App>
      <Header route="/shop" />
      <article className="shop-item--wrapper">
        <nav className="shop-item__breadcrumb">
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <span>/</span>
            <li>
              {/* TODO: modify this URL to reflect the product type */}
              <a href="/shop">Pyjama - Đồ bộ</a>
            </li>
            <span>/</span>
            <li>
              <a href="#">{product.name}</a>
            </li>
          </ul>
        </nav>
        <section className="shop-item">
          <div>
            <ShopItemPreviewDesc product={product} colourIndex={colourIndex} />
            <div className="desktop-only">
              {product.discountByAccessories && (
                <ShopItemDiscountAccessories
                  {...product.discountByAccessories}
                />
              )}
              <ShopItemDescription description={product.description} />
            </div>
          </div>
          <div className="shop-item__info">
            <ShopItemInfo product={product} onColourChange={setColourIndex} />
            <ShopItemAccordion />
          </div>
        </section>
        <hr className="shop-item__related-divider" />
        <section className="related-item-list">
          <h2 className="related-item-list__title">CÓ THỂ BẠN SẼ THÍCH</h2>
          <ProductSlider productList={relateProductList} />
        </section>
        <img
          src="/public/images/shop-item__footer-image.png"
          alt=""
          className="shop-item__footer-image"
        />
      </article>
      <Footer />
    </App>
  );
}

export default ShopItemPage;
