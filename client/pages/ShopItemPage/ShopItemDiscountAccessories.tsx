import React from "react";

interface ShopItemDiscountAccessoriesProps {
  discount: number;
  accessoryList: Array<{
    id: string;
    slug: string;
    previewURL: string;
  }>;
}

function ShopItemDiscountAccessories({
  discount,
  accessoryList
}: ShopItemDiscountAccessoriesProps) {
  return (
    <div className="shop-item__discount-acc">
      <p className="shop-item__discount-acc__info">
        -{discount}% khi mua kèm với 1 trong những phụ kiện dưới
      </p>
      <div className="shop-item__discount-acc__item-list">
        {accessoryList.map(function({ previewURL, slug }) {
          return (
            <a
              key={slug}
              href="/shop-item"
              className="shop-item__discount-acc__item">
              <img src={previewURL} alt="" />
            </a>
          );
        })}
      </div>
    </div>
  );
}

export default ShopItemDiscountAccessories;
