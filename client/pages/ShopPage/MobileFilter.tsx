import React from "react";

function MobileFilter({
  activateFilterModal,
  defaultSearch
}: {
  activateFilterModal: () => void;
  defaultSearch: string;
}) {
  return (
    <div className="mobile-filter">
      <div className="mobile-filter__triggers">
        <button type="button" onClick={activateFilterModal}>
          <svg width="32" height="32" viewBox="0 0 32 32">
            <path
              d="M26,6v0.233l-8.487,9.43L17,16.233V17v7.764l-2-1V17v-0.767l-0.513-0.57L6,6.233V6H26 M28,4H4v3  l9,10v8l6,3V17l9-10V4L28,4z"
              style={{ fill: "#727272" }}
            />
          </svg>
        </button>
        <div className="mobile-filter__search">
          <input
            type="text"
            name="search"
            placeholder="Tìm kiếm"
            defaultValue={defaultSearch}
          />
          <svg width="26" height="26" viewBox="0 0 26 26" fill="none">
            <path
              d="M10.9828 0C4.92685 0 0 4.92685 0 10.9828C0 17.0387 4.92685 21.9655 10.9828 21.9655C17.0387 21.9655 21.9655 17.0387 21.9655 10.9828C21.9655 4.92685 17.0386 0 10.9828 0ZM10.9828 20.0443C5.98609 20.0443 1.92121 15.9794 1.92121 10.9828C1.92121 5.98609 5.98609 1.92121 10.9828 1.92121C15.9791 1.92121 20.0443 5.98609 20.0443 10.9828C20.0443 15.9794 15.9794 20.0443 10.9828 20.0443Z"
              fill="#727272"
            />
            <path
              d="M25.7185 24.3603L18.7381 17.38C18.3629 17.0047 17.7551 17.0047 17.3798 17.38C17.0046 17.7549 17.0046 18.3633 17.3798 18.7383L24.3602 25.7186C24.5478 25.9062 24.7934 26 25.0393 26C25.2853 26 25.5308 25.9062 25.7185 25.7186C26.0937 25.3436 26.0937 24.7352 25.7185 24.3603Z"
              fill="#727272"
            />
          </svg>
        </div>
      </div>
    </div>
  );
}

export default MobileFilter;
