import React from "react";
import Product from "../../models/Product";
import ProductCard from "../../components/ProductCard";

function ShopList({ productList }: { productList: Product[] }) {
  return (
    <div className="shop-list">
      {productList.map(function (product) {
        return <ProductCard key={product.id} product={product} />;
      })}
    </div>
  );
}

export default ShopList;
