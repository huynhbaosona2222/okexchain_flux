import { ChangeEvent } from "react";

function submitForm(e: ChangeEvent) {
  (e.target as HTMLInputElement).form?.submit();
}

export default submitForm;
