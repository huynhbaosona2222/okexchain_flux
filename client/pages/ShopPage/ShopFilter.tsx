import React from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";
import {
  SORT_OPTION,
  COLLECTIONS,
  PRICES,
  SIZES,
  COLOURS
} from "../../data/filter-values";
import Filter from "./Filter";
import submitForm from "./submitForm";

interface ShopFilterProps {
  filter: Filter;
}

function ShopFilter({ filter }: ShopFilterProps) {
  return (
    <div className="shop-filter">
      <h2 className="shop-filter__section-title">LỌC SẢN PHẨM</h2>
      <hr className="shop-filter__section-title__divider"></hr>
      <div>
        <select name="sort" onChange={submitForm}>
          <option
            value=""
            selected={!Object.values(SORT_OPTION).includes(filter.sort)} // if no `sort` is available, disable the placeholder
            disabled
            hidden>
            Xếp theo
          </option>
          <option
            selected={SORT_OPTION.HIGH_PRICE === filter.sort}
            value={SORT_OPTION.HIGH_PRICE}>
            Giá cao nhất
          </option>
          <option
            selected={SORT_OPTION.LOW_PRICE === filter.sort}
            value={SORT_OPTION.LOW_PRICE}>
            Giá thấp nhất
          </option>
          <option
            selected={SORT_OPTION.NEWEST === filter.sort}
            value={SORT_OPTION.NEWEST}>
            Mới nhất
          </option>
        </select>
        <Accordion
          allowMultipleExpanded={true}
          allowZeroExpanded={true}
          preExpanded={[0, 1, 2, 3]}>
          <AccordionItem uuid={0}>
            <AccordionItemHeading>
              <AccordionItemButton>Bộ Sưu Tập</AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              {COLLECTIONS.map(function({ value, text }) {
                return (
                  <label key={value}>
                    <input
                      type="radio"
                      name="collectionId"
                      defaultChecked={value === filter.collectionId}
                      onChange={submitForm}
                      value={value}
                    />
                    <span>{text}</span>
                  </label>
                );
              })}
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid={1}>
            <AccordionItemHeading>
              <AccordionItemButton>Giá</AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              {PRICES.map(function({ value, text }) {
                return (
                  <label key={value}>
                    <input
                      type="checkbox"
                      name="price"
                      defaultChecked={value === filter.price}
                      onChange={submitForm}
                      value={value}
                    />
                    <span>{text}</span>
                    <div className="shop-filter__checkbox">
                      {/* prettier-ignore */}
                      <svg width="12" height="10" viewBox="0 0 12 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M4.31248 7.12009L2.07119 4.87881L1.94998 4.75759L1.82876 4.87881L1.04126 5.66631L0.920039 5.78752L1.04126 5.90874L4.19126 9.05874L4.31248 9.17996L4.43369 9.05874L11.1837 2.30874L11.3049 2.18752L11.1837 2.06631L10.3962 1.27881L10.275 1.15759L10.1538 1.27881L4.31248 7.12009Z"
                          fill="white"
                          stroke="white"
                          strokeWidth="0.342857"
                        />
                      </svg>
                    </div>
                  </label>
                );
              })}
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid={2}>
            <AccordionItemHeading>
              <AccordionItemButton>Size</AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              {SIZES.map(function({ value, text }) {
                return (
                  <label key={value}>
                    <input
                      type="checkbox"
                      name="size"
                      defaultChecked={value === filter.size}
                      onChange={submitForm}
                      value={value}
                    />
                    <span>{text}</span>
                    <div className="shop-filter__checkbox">
                      {/* prettier-ignore */}
                      <svg width="12" height="10" viewBox="0 0 12 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M4.31248 7.12009L2.07119 4.87881L1.94998 4.75759L1.82876 4.87881L1.04126 5.66631L0.920039 5.78752L1.04126 5.90874L4.19126 9.05874L4.31248 9.17996L4.43369 9.05874L11.1837 2.30874L11.3049 2.18752L11.1837 2.06631L10.3962 1.27881L10.275 1.15759L10.1538 1.27881L4.31248 7.12009Z"
                          fill="white"
                          stroke="white"
                          strokeWidth="0.342857"
                        />
                      </svg>
                    </div>
                  </label>
                );
              })}
            </AccordionItemPanel>
          </AccordionItem>
          <AccordionItem uuid={3}>
            <AccordionItemHeading>
              <AccordionItemButton>Màu sắc</AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <div className="colour-filter">
                {COLOURS.map(function({ value, text }) {
                  return (
                    <label key={value}>
                      <input
                        type="checkbox"
                        name="colour"
                        defaultChecked={value === filter.colour}
                        onChange={submitForm}
                        value={value}
                      />
                      <div className="colour-filter__item--wrapper-out">
                        <div className="colour-filter__item--wrapper-in">
                          <div
                            className="colour-filter__item"
                            style={{ backgroundColor: text }}>
                            {/* prettier-ignore */}
                            <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                              d="M6.40909 10.9962L2.47349 7.0606L2.35227 6.93938L2.23105 7.0606L0.878782 8.41287L0.757563 8.53409L0.878782 8.65531L6.28787 14.0644L6.40909 14.1856L6.53031 14.0644L18.1212 2.47349L18.2424 2.35227L18.1212 2.23105L16.7689 0.878782L16.6477 0.757563L16.5265 0.878782L6.40909 10.9962Z"
                              fill="white"
                              stroke="white"
                              strokeWidth="0.342857"
                            />
                          </svg>
                          </div>
                        </div>
                      </div>
                    </label>
                  );
                })}
              </div>
            </AccordionItemPanel>
          </AccordionItem>
        </Accordion>
      </div>
    </div>
  );
}

export default ShopFilter;
