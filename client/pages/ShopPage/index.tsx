import React, { useState, useMemo, Fragment } from "react";

import Header from "../../components/Header";
import ShopFilter from "./ShopFilter";
import ShopList from "./ShopList";
import ShopSearch from "./ShopSearch";
import Product from "../../models/Product";
import Pagination from "../../components/Pagination";
import Footer from "../../components/Footer";
import Filter, { toQsInput } from "./Filter";
import ProductRaw from "../../models/ProductRaw";
import App from "../../components/App";
import MobileFilter from "./MobileFilter";
import useMediaQuery from "../../hooks/useMediaQuery";

/*
TODO: during SSR:
Create a <script> in <head> with the following variables initialised.
*/
declare global {
  interface Window {
    __PRELOADED_PAGE_LIMIT: number;
    __PRELOADED_FILTER__: Filter;
    __PRELOADED_PRODUCT_LIST__: ProductRaw[];
  }
}

interface ShopProps {
  pageLimit?: number;
  filter?: Filter;
  productList?: ProductRaw[];
}

function ShopPage({
  filter: preloadedFilter,
  pageLimit: preloadedPageLimit,
  productList: preloadedProductList
}: ShopProps) {
  let [filter] = useState(function() {
    if (typeof preloadedFilter !== "undefined") {
      return preloadedFilter;
    }
    return window.__PRELOADED_FILTER__;
  });

  let [pageLimit] = useState(function() {
    if (typeof preloadedPageLimit !== "undefined") {
      return preloadedPageLimit;
    }
    return window.__PRELOADED_PAGE_LIMIT;
  });

  let [productList] = useState<Product[]>(function() {
    let result: ProductRaw[];
    if (typeof preloadedProductList !== "undefined") {
      result = preloadedProductList;
    } else {
      result = window.__PRELOADED_PRODUCT_LIST__;
    }
    return result.map(product => new Product(product));
  });

  let [modalActive, setModalActive] = useState(false);
  let filterQsInput = useMemo(() => toQsInput(filter), [filter]);

  let onMobile = useMediaQuery("(max-width: 767px)");

  return (
    <App>
      <Header route="/shop" />
      <form className="shop">
        {!onMobile ? (
          <Fragment>
            <ShopFilter filter={filter} />
            <section>
              <div className="shop-search--wrapper">
                <h2 className="shop__collection-title">Tất cả sản phẩm</h2>
                <ShopSearch defaultSearch={filter.search} />
              </div>
              <img
                className="shop__header-image"
                src="/public/images/shop_header_image.webp"
                alt=""
              />
              <ShopList productList={productList} />
              <Pagination
                page={filter.page}
                limit={pageLimit}
                query={filterQsInput}
                baseUrl="/shop"
              />
            </section>
          </Fragment>
        ) : (
          <Fragment>
            <div>
              <div
                className={
                  "mobile-filter__modal-wrapper" +
                  (modalActive ? " active" : "")
                }>
                <div className="spacer" onClick={() => setModalActive(false)} />
                <ShopFilter filter={filter} />
              </div>
              <MobileFilter
                activateFilterModal={() => setModalActive(true)}
                defaultSearch={filter.search}
              />
            </div>
            <ShopList productList={productList} />
            <Pagination
              page={filter.page}
              limit={pageLimit}
              query={filterQsInput}
              baseUrl="/shop"
            />
          </Fragment>
        )}
      </form>
      <Footer />
    </App>
  );
}

export default ShopPage;
