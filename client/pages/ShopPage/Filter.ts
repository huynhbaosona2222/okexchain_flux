import qs from "querystring";

interface Filter {
  page: number;
  sort: number;
  search: string;
  collectionId: number;
  price: number;
  size: number;
  colour: number;
}

function toQsInput(filter: Filter): qs.ParsedUrlQueryInput {
  return {
    page: filter.page.toString(),
    sort: filter.sort.toString(),
    search: filter.search,
    collectionId: filter.collectionId.toString(),
    price: filter.price.toString(),
    size: filter.size.toString(),
    colour: filter.colour.toString()
  };
}

export default Filter;
export { toQsInput };
