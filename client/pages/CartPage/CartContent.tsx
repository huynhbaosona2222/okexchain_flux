import React, { useContext } from "react";
import CartContext from "../../contexts/CartContext";
import CartProductCard from "../../components/CartProductCard";
import formatPrice from "../../helpers/formatPrice";
import computeTotal from "../../helpers/computeTotal";

function CartContent() {
  let { cartProductList } = useContext(CartContext);
  let totalPrice = 0;
  for (let cartProduct of cartProductList) {
    totalPrice += computeTotal(
      cartProduct.product.price,
      cartProduct.quantity,
      cartProduct.product
    );
  }
  return (
    <div className="cart">
      {cartProductList.map(function (cartProduct, idx) {
        return <CartProductCard key={idx} cartProduct={cartProduct} />;
      })}
      <hr />
      <div className="cart__total">
        <h2>TỔNG CỘNG</h2>
        <p>{formatPrice(totalPrice)}</p>
      </div>
      <a href="/checkout">
        <button type="button" className="cart__checkout-btn">
          THANH TOÁN
        </button>
      </a>
    </div>
  );
}

export default CartContent;
