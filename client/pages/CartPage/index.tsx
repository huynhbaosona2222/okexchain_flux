import React from "react";
import App from "../../components/App";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import CartContent from "./CartContent";

function CartPage() {
  return (
    <App>
      <Header route="" />
      <CartContent />
      <Footer />
    </App>
  );
}

export default CartPage;
