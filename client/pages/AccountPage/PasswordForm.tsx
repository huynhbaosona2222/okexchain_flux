import React, { Fragment } from "react";

function PasswordForm() {
  return (
    <Fragment>
      <label className="account-form__field">
        <span className="field-name">Mật khẩu cũ</span>
        <input
          type="password"
          name="oldPassword"
          className="field-value"
          placeholder="Nhập mật khẩu cũ"
        />
      </label>
      <label className="account-form__field">
        <span className="field-name">Mật khẩu mới</span>
        <input
          type="password"
          name="newPassword"
          className="field-value"
          placeholder="Mật khẩu từ 6-12 ký tự"
        />
      </label>
      <label className="account-form__field">
        <span className="field-name">Nhập lại mật khẩu mới</span>
        <input
          type="password"
          name="confirmPassword"
          className="field-value"
          placeholder="Nhập lại mật khẩu mới"
        />
      </label>
    </Fragment>
  );
}

export default PasswordForm;
