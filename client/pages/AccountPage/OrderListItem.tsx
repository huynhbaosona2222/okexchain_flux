import React, { useState, Fragment, ReactNode } from "react";
import padNumber from "../../helpers/padNumber";
import Order from "../../models/Order";
import shippingMethodList from "../../data/shippingMethodList";
import computeTotal from "../../helpers/computeTotal";
import formatPrice from "../../helpers/formatPrice";

function displayDate(date: Date): ReactNode {
  let d = date.getDate();
  let m = date.getMonth();
  let y = date.getFullYear();
  return (
    <Fragment>
      <span>Ngày </span>
      <span>
        {padNumber(d)}/{padNumber(m)}/{padNumber(y, 4)}
      </span>
    </Fragment>
  );
}

function displayDateString(date: Date): string {
  let d = date.getDate();
  let m = date.getMonth();
  let y = date.getFullYear();
  return `${padNumber(d)}/${padNumber(m)}/${padNumber(y, 4)}`;
}

function computeOrderTotal(order: Order): number {
  let ans = 0;
  for (let { product, quantity } of order.productList) {
    ans += computeTotal(product.price, quantity, product);
  }
  return ans;
}

function OrderListItem({ order }: { order: Order }) {
  let [showDetail, setShowDetail] = useState(false);

  return (
    <div className="order">
      <div className="order-header">
        <h3>{displayDate(order.date)}</h3>
        <button type="button" onClick={() => setShowDetail((p) => !p)}>
          <span>{showDetail ? "Ẩn" : "Xem"} chi tiết </span>
          <span>đơn hàng</span>
        </button>
      </div>
      <p className="order-id">Sản phẩm đã mua | Mã đơn hàng {order.id}</p>
      <div className="order-plist">
        {order.productList.map(function(
          { product, previewImage, colour, size, quantity },
          idx
        ) {
          if (!previewImage) {
            return null;
          }
          return (
            <div key={idx} className="order-pcard">
              <div className="order-pcard__img-wrapper">
                <img src={previewImage} alt="" />
              </div>
              <h4>{product.name}</h4>
              <ul>
                <li>
                  Colour:{" "}
                  <span
                    className="order-colour"
                    style={{ backgroundColor: colour }}></span>
                </li>
                <li>Size: {size}</li>
                <li>Quantity: {padNumber(quantity)}</li>
                <li>
                  {formatPrice(computeTotal(product.price, quantity, product))}{" "}
                  VND
                </li>
              </ul>
            </div>
          );
        })}
      </div>
      {showDetail && (
        <Fragment>
          <hr />
          <ul>
            <li>
              <span>Phí giao hàng</span>
              <span>20,000 VND</span>
            </li>
            <li>
              <span>Tổng tiền hàng</span>
              <span>{formatPrice(computeOrderTotal(order))} VND</span>
            </li>
            <li>
              <span>Thời gian giao hàng</span>
              <span>
                {order.shippingDate.map(displayDateString).join(" - ")}
              </span>
            </li>
            <li>
              <span>Phương thức giao hàng</span>
              <span>
                {shippingMethodList[order.shippingInfo.shippingMethod]}
              </span>
            </li>
          </ul>
          <hr />
          <ul>
            <li>
              <span>Họ tên</span>
              <span>{order.shippingInfo.fullname}</span>
            </li>
            <li>
              <span>Địa chỉ nhận hàng</span>
              <span>
                {order.shippingInfo.address}, {order.shippingInfo.ward},{" "}
                {order.shippingInfo.district}, {order.shippingInfo.city}
              </span>
            </li>
            <li>
              <span>Số điện thoại</span>
              <span>
                {order.shippingInfo.phone1}
                {order.shippingInfo.phone2
                  ? " - " + order.shippingInfo.phone2
                  : ""}
              </span>
            </li>
          </ul>
        </Fragment>
      )}
    </div>
  );
}

export default OrderListItem;
