import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import OrderList from "./OrderList";
import App from "../../components/App";
import AccountForm from "./AccountForm";

function AccountPage() {
  return (
    <App>
      <Header route="/account" />
      <section className="account-info">
        <h2 className="account-info__section-title">Thông tin tài khoản</h2>
        <AccountForm />
        <div className="account-info__action">
          <button type="button" className="account-info__action-cancel">
            HỦY
          </button>
          <button
            type="submit"
            className="account-info__action-submit"
            form="account-form">
            CẬP NHẬT
          </button>
        </div>
      </section>
      <hr className="page-divider" />
      <h2 className="order-section__title">Đơn hàng của tôi</h2>
      <hr className="order-section__divider" />
      <OrderList />
      <Footer />
    </App>
  );
}

export default AccountPage;
