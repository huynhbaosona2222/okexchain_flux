import React, { useState, FormEvent, useContext } from "react";
import PasswordForm from "./PasswordForm";
import AppContext from "../../contexts/AppContext";

interface AccountFormElement extends HTMLFormElement {
  fullname: HTMLInputElement;
  phone: HTMLInputElement;
  gender: HTMLInputElement;
  address: HTMLInputElement;
  email: HTMLInputElement;
  oldPassword?: HTMLInputElement;
  newPassword?: HTMLInputElement;
  confirmPassword?: HTMLInputElement;
}

function AccountForm() {
  let [changePassword, setChangePassword] = useState(false);
  let { user } = useContext(AppContext);

  function updateAccount(e: FormEvent) {
    e.preventDefault();
    let form = e.target as AccountFormElement;

    let name = form.fullname.value;
    let phone = form.phone.value;
    let gender = form.gender.value;
    let address = form.address.value;
    let email = form.email.value;

    // TODO: do something with these information
    console.log({ name, phone, gender, address, email });

    if (form.oldPassword) {
      let oldPassword = form.oldPassword.value;
      let newPassword = form.newPassword!.value;
      let confirmPassword = form.confirmPassword!.value;

      // TODO: do something with these passwords
      console.log({ oldPassword, newPassword, confirmPassword });
    }
  }
  return (
    <form id="account-form" onSubmit={updateAccount}>
      <div>
        <label className="account-form__field">
          <span className="field-name">Họ và tên</span>
          <input
            type="text"
            name="fullname"
            className="field-value"
            defaultValue={user?.fullname}
          />
        </label>
        <label className="account-form__field">
          <span className="field-name">Số điện thoại</span>
          <input
            type="text"
            name="phone"
            className="field-value"
            inputMode="numeric"
            pattern="[0-9]*"
            defaultValue={user?.phone}
          />
        </label>
        <div className="account-form__field horizontal">
          <span className="field-name">Giới tính</span>
          <div className="field-value">
            <label className="checkbox">
              <input
                type="radio"
                name="gender"
                defaultChecked={user?.gender === 0}
              />
              <span className="checkbox-show">
                <span></span>
              </span>
              <span className="checkbox-label">Nam</span>
            </label>
            <label className="checkbox">
              <input
                type="radio"
                name="gender"
                defaultChecked={user?.gender === 1}
              />
              <span className="checkbox-show">
                <span></span>
              </span>
              <span className="checkbox-label">Nữ</span>
            </label>
          </div>
        </div>
      </div>
      <div>
        <label className="account-form__field">
          <span className="field-name">Địa chỉ</span>
          <input
            type="text"
            name="address"
            className="field-value"
            defaultValue={user?.address}
          />
        </label>
        <label className="account-form__field">
          <span className="field-name">Email</span>
          <input
            type="text"
            name="email"
            className="field-value"
            disabled
            defaultValue={user?.email}
          />
        </label>
        {!changePassword ? (
          <button
            type="button"
            className="account-form__trigger-change-password"
            onClick={() => setChangePassword(true)}>
            Đổi mật khẩu
          </button>
        ) : (
          <PasswordForm />
        )}
      </div>
    </form>
  );
}

export default AccountForm;
