import React, { useState } from "react";
import dummyOrderList from "../../data/dummyOrderList";
import Order from "../../models/Order";
import OrderListItem from "./OrderListItem";

function OrderList() {
  let [orderList] = useState(function () {
    return dummyOrderList.map((order) => new Order(order));
  });

  return (
    <section className="order-list">
      {orderList.map(function (order) {
        return <OrderListItem key={order.id} order={order} />;
      })}
    </section>
  );
}

export default OrderList;
