import React, { Fragment } from "react";

function HomePageDesktop() {
  return (
    <Fragment>
      <div className="homepage__head-img1">
        <img src="/public/images/homepage__head-img1.png" alt="" />
      </div>

      <div style={{ height: "7.5rem" }} />

      <div className="homepage__head-img2">
        <img src="/public/images/homepage__head-img21.png" alt="" />
        <img src="/public/images/homepage__head-img22.png" alt="" />
      </div>

      <div style={{ height: "8.4rem" }} />

      <div className="new-arrival">
        <h2>New Arrival</h2>
        <div>
          <p>
            Quốc tế Phụ nữ 8/3
            <br />
            Mùa yêu thương
          </p>
          <div className="spacer" />
          <a href="/shop" className="homepage__button">
            <button type="button">Xem ngay BST mới</button>
          </a>
        </div>
      </div>

      <div style={{ height: "20rem" }} />

      <div className="collection">
        <div>
          <h2>
            Pyjama
            <br />
            Collection
          </h2>
        </div>
        <div className="collection-description">
          <img
            className="collection-description__dots"
            src="/public/images/homepage__dots-blue.svg"
            alt=""
          />
          <img src="/public/images/homepage__collection-img1.png" alt="" />
          <div />
          <div className="collection-content">
            <h3>Tiệc ngủ cho hội cạ cứng</h3>
            <div style={{ height: "2.5rem" }} />
            <div className="divider" />
            <div style={{ height: "2.5rem" }} />
            <p>
              Nếu bạn thường kết thúc một ngày của mình bằng việc… leo lên
              giường đi ngủ, than vãn rằng một ngày đã qua thật nhạt nhẽo, hoặc
              đang mệt mỏi với những công việc dồn ứ vào ngày cuối năm, vậy thì
              hãy tổ chức một party nho nhỏ cho mình cùng bạn bè đi nào.
            </p>
            <div style={{ height: "3.7rem" }} />
            <a href="/shop" className="homepage__button">
              <button type="button">Xem ngay BST mới</button>
            </a>
          </div>
          <img src="/public/images/homepage__collection-img2.png" alt="" />
        </div>
      </div>

      <div style={{ height: "27rem" }} />

      <div className="best-seller">
        <div>
          <img src="/public/images/homepage__best-seller-1.png" alt="" />
          <img className="dots" src="/public/images/homepage__dots-red.svg" />
        </div>
        <div>
          <div className="best-seller__rect--wrapper">
            <img src="/public/images/homepage__best-seller-2.png" alt="" />
            <div className="best-seller__rect--wrapper1">
              <div className="best-seller__rect">
                <h2>BEST SELLER</h2>
                <div>
                  <a href="/shop">Các sản phẩm bán chạy</a>
                </div>
              </div>
            </div>
          </div>
          <div style={{ height: "4.2rem" }} />
          <img
            style={{ width: "50%" }}
            src="/public/images/homepage__best-seller-3.png"
            alt=""
          />
        </div>
      </div>

      <div style={{ height: "15rem" }} />

      <div className="sales">
        <div>
          <h2>
            SALE UP TO
            <br />
            <strong>50%</strong>
          </h2>
          <p>Thời gian khuyến mãi từ 10-13/03/2020</p>
          <a href="/shop" className="homepage__button">
            <button type="button">Xem ngay</button>
          </a>
        </div>
        <div>
          <img src="/public/images/homepage__sales.png" alt="" />
        </div>
      </div>

      <div style={{ height: "15rem" }} />

      <div className="stay-home-msg">
        <h2>
          <span>Stay home!</span> <span>Wear Chillax</span>
        </h2>
      </div>

      <div className="stay-home-after">
        <div className="stay-home-after-card">
          <img src="/public/images/homepage__stay-1.png" alt="" />
          <div style={{ height: "1.5rem" }} />
          <div className="link-container">
            <a href="/shop">Pyjama - Đồ bộ</a>
          </div>
        </div>
        <div className="stay-home-after-card">
          <img src="/public/images/homepage__stay-2.png" alt="" />
          <div style={{ height: "1.5rem" }} />
          <div className="link-container">
            <a href="/shop">Sleep dress - Váy ngủ</a>
          </div>
        </div>
        <div className="stay-home-after-card">
          <img src="/public/images/homepage__stay-3.png" alt="" />
          <div style={{ height: "1.5rem" }} />
          <div className="link-container">
            <a href="/shop">Pajamas - Quần ngủ</a>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default HomePageDesktop;
