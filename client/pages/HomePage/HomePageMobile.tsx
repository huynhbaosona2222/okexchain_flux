import React, { Fragment } from "react";

function RightArrow({ className = "" }: { className?: string }) {
  return (
    <svg
      className={className}
      width="15"
      height="11"
      viewBox="0 0 15 11"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.9084 5.27906L10.2209 0.591564C10.1312 0.502443 9.99785 0.475285 9.88037 0.523801C9.76348 0.572023 9.68751 0.686164 9.68751 0.812492V1.43748C9.68751 1.52048 9.72047 1.59985 9.77906 1.65844L12.9956 4.875H0.31251C0.139775 4.87503 0 5.01478 0 5.18751V5.8125C0 5.98524 0.139775 6.12501 0.31251 6.12501H12.9956L9.77906 9.34158C9.72047 9.40017 9.68751 9.47951 9.68751 9.56253V10.1875C9.68751 10.3139 9.76351 10.428 9.88037 10.4762C9.91913 10.4924 9.95971 10.5 9.99999 10.5C10.0812 10.5 10.1611 10.4683 10.2209 10.4085L14.9084 5.72098C15.0305 5.5989 15.0305 5.40114 14.9084 5.27906Z"
        fill="white"
      />
    </svg>
  );
}

function HomePageMobile() {
  return (
    <Fragment>
      <div className="new-arrival">
        <h2>New Arrival</h2>
        <p>
          Quốc tế Phụ nữ 8/3
          <br />
          Mùa yêu thương
        </p>
      </div>

      <div>
        <img
          src="/public/images/homepage__head-img21.png"
          alt=""
          className="w-full"
        />
        <button
          type="button"
          className="flex rounded-l-8 bg-c-red-100 float-right transform -translate-y-1/2 py-4 pl-16 pr-8 text-c-14 leading-c-17 text-white items-center font-medium">
          Xem ngay <RightArrow className="ml-4" />
        </button>
      </div>

      <div className="h-20" />

      <div className="flex justify-between">
        <h2
          className="text-c-red-100 text-c-40 leading-c-50 font-bold writin transform rotate-180"
          style={{ writingMode: "vertical-rl", paddingTop: "20%" }}>
          Pyjama
          <br />
          Collection
        </h2>
        <div className="flex-grow">
          <img
            src="/public/images/homepage__collection-img1.png"
            alt=""
            className="w-full"
          />
        </div>
      </div>

      <div className="h-12" />

      <div className="pr-40">
        <img
          src="/public/images/homepage__collection-img2.png"
          alt=""
          className="w-full"
        />
      </div>

      <div className="h-10" />

      <div className="flex">
        <div className="pt-4 pl-8 pr-3">
          <h2 className="text-c-18 leading-c-22 font-medium text-black">
            Tiệc ngủ cho hội cạ cứng
          </h2>
          <hr className="w-20 my-5 border-c-black-100" />
          <p className="text-c-12 leading-c-20 text-c-black-100">
            Nếu bạn thường kết thúc một ngày của mình bằng việc… leo lên giường
            đi ngủ, than vãn rằng một ngày đã qua thật nhạt nhẽo, hoặc đang mệt
            mỏi với những công việc dồn ứ vào ngày cuối năm, vậy thì hãy tổ chức
            một party nho nhỏ cho mình cùng bạn bè đi nào.
          </p>
        </div>
        <img
          src="/public/images/homepage__random-img-1.png"
          alt=""
          className="w-56 flex-grow"
        />
      </div>
      <div className="py-10 pl-8">
        <button
          type="button"
          className="rounded-8 bg-c-red-100 w-48 text-c-14 leading-c-17 py-3 text-white">
          Xem ngay
        </button>
      </div>

      <h2 className="py-4 font-medium bg-c-orange-200 text-c-orange-100 text-c-30 leading-c-36 text-center">
        Stay home! Wear Chillax
      </h2>

      <div className="pl-8 pt-4 pb-6">
        <h2 className="font-bold text-c-20 leading-c-25 text-c-blue-100">
          BEST SELLER
        </h2>
        <p className="text-c-14 leading-c-17 text-c-black-100 mt-2">
          Các sản phẩm bán chạy
        </p>
      </div>

      <div>
        <div className="pr-40">
          <img
            src="/public/images/homepage__best-seller-3.png"
            alt=""
            className="w-full"
          />
        </div>
        <button
          type="button"
          className="flex rounded-l-8 bg-c-red-100 float-right transform -translate-y-1/2 py-4 pl-16 pr-8 text-c-14 leading-c-17 text-white items-center font-medium">
          Xem ngay <RightArrow />
        </button>
      </div>

      <div className="h-24" />

      <div>
        <img src="/public/images/homepage__random-img-2.png" alt="" />
      </div>
      <div className="inline-block transform -translate-y-1/2">
        <h2 className="bg-c-yellow-100 text-c-red-100 text-center font-bold text-c-30 leading-c-36 py-3 px-10">
          SALE UP TO
        </h2>
        <h2 className="text-center text-c-80 leading-c-100 text-c-red-100 font-bold">
          50%
        </h2>
        <p className="text-c-14 leading-c-17 text-c-black-100 text-center">
          Từ ngày 10-13/03/2020
        </p>
      </div>
    </Fragment>
  );
}

export default HomePageMobile;
