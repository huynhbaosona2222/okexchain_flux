import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import App from "../../components/App";
import HomePageDesktop from "./HomePageDesktop";
import HomePageMobile from "./HomePageMobile";

function Homepage() {
  return (
    <App>
      <Header route="/" />
      <section className="homepage desktop-only">
        <HomePageDesktop />
      </section>
      <section className="homepage mobile-only -mb-24">
        <HomePageMobile />
      </section>
      <Footer />
    </App>
  );
}

export default Homepage;
