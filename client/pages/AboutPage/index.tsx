import React from "react";
import App from "../../components/App";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import AboutPageDesktop from "./AboutPageDesktop";
import AboutPageMobile from "./AboutPageMobile";

function AboutPage() {
  return (
    <App>
      <Header route="/about" />
      <div className="desktop-only">
        <AboutPageDesktop />
      </div>
      <div className="mobile-only">
        <AboutPageMobile />
      </div>
      <Footer />
    </App>
  );
}

export default AboutPage;
