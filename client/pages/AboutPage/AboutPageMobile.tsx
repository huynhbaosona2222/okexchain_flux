import React, { Fragment } from "react";

function AboutPageMobile() {
  return (
    <Fragment>
      <h1 className="text-c-40 leading-c-50 text-c-black-100 font-medium pt-8 pl-8">
        Behind
        <br />
        <span className="pl-12">the Brand</span>
      </h1>
      <div className="h-8" />
      <div className="pl-24">
        <img src="/public/images/about__img-1.png" alt="" />
      </div>
      <div className="h-16" />
      <div className="pr-24 relative">
        <img src="/public/images/homepage__head-img21.png" alt="" />
        <h2
          className="bg-c-yellow-100 px-5 pt-16 pb-6 text-c-30 leading-c-34 text-c-blue-200 font-medium absolute right-0"
          style={{ writingMode: "vertical-rl", bottom: "-4.2rem" }}>
          Stay home!
          <br />
          <span>Wear Chillax</span>
        </h2>
      </div>
      <div className="h-20" />
      <p className="px-8 text-c-12 leading-c-20 text-c-black-100 pb-12">
        Luxury Ready-to-wear label that embodies the classic and the
        contemporary. Drawing there inspiration from art, architecture, and
        global culture, combines straight-lined designs with elegant
        sophistication a refined statement that beautifies women and glorifies
        their personality. A combination of high quality fibers and elaborate
        crafts man ship each design exudes a spirit of true light.
      </p>
      <img src="/public/images/about__img-5.png" alt="" className="w-full" />
      <div className="h-12" />
      <div className="pl-8 grid grid-cols-2">
        <h2 className="text-c-red-100 text-c-30 leading-c-30 font-medium pt-12">
          The
          <br />
          Philosophy
        </h2>
        <div>
          <img src="/public/images/about__img-4.png" alt="" />
        </div>
      </div>
      <p className="text-c-12 leading-c-20 text-c-black-100 pt-6 px-8">
        At the center of Chillax in an ethos of minimalist sophistic and
        feminine elegance for today’s women. The designs merge the latest global
        trends to Middle Eastern fashion. Inspired by Art, Architecture and
        Global Culture, the signature designs lie in highly modern silhouettes,
        bold lines, geometric precision and great attention to details.
      </p>
    </Fragment>
  );
}

export default AboutPageMobile;
