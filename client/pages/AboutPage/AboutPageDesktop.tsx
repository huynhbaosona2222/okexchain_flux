import React, { Fragment } from "react";

function AboutPageDesktop() {
  return (
    <Fragment>
      <div className="h-120 xl:h-64" />
      <div className="px-p-15 flex items-end justify-between xl:px-p-12 lg:px-48">
        <div>
          <p className="text-c-26 leading-c-32 text-c-black-100">Giới thiệu</p>
          <hr className="w-20 border-c-black-100 mt-8 mb-6" />
          <h1 className="text-c-120 leading-c-150 text-c-black-100 font-medium">
            Behind
            <br />
            <span className="pl-16">the Brand</span>
          </h1>
        </div>
        <div className="ml-8 md2:hidden">
          <img
            src="/public/images/about__img-1.png"
            alt=""
            className="w-full"
          />
        </div>
      </div>
      <div className="h-240 xl:h-120" />
      <div className="pl-p-15 pr-p-12 grid grid-cols-2 xl:pl-p-12 lg:px-48 md2:grid-cols-3">
        <div className="pt-24 mr-40 md2:col-span-2 md2:pt-0">
          <h2 className="text-c-80 leading-c-100 text-c-black-100 font-medium">
            Stay home!
            <br />
            Wear Chillax
          </h2>
          <hr className="mt-6 mb-20 w-40 border-c-black-200" />
          <p className="text-c-18 leading-c-38">
            Luxury Ready-to-wear label that embodies the classic and the
            contemporary. Drawing there inspiration from art, architecture, and
            global culture, combines straight-lined designs with elegant
            sophistication; a refined statement that beautifies women and
            glorifies their personality. A combination of high quality fibers
            and elaborate craftsmanship—each design exudes a spirit of true
            lightness.
          </p>
        </div>
        <img src="/public/images/about__img-2.png" alt="" />
      </div>
      <div className="h-40" />
      <div className="pl-48 xl:pr-48">
        <img src="/public/images/about__img-3.png" alt="" className="w-full" />
      </div>
      <div className="h-40" />
      <div className="pl-p-15 pr-40 grid grid-cols-2 lg:px-48">
        <div className="pt-120 w-160 lg:pt-64">
          <h2 className="text-c-80 leading-c-100 text-c-black-100 font-medium">
            The
            <br />
            Philosophy
          </h2>
          <hr className="w-40 mt-4 mb-24 border-c-black-200" />
          <p className="text-c-18 leading-c-38 text-c-black-100">
            At the center of Chillax in an ethos of minimalist sophistication
            and feminine elegance for today’s women. The designs merge the
            latest global trends to Middle Eastern fashion. Inspired by Art,
            Architecture and Global Culture, the signature designs lie in highly
            modern silhouettes, bold lines, geometric precision and great
            attention to details.
          </p>
        </div>
        <img src="/public/images/about__img-4.png" alt="" />
      </div>
    </Fragment>
  );
}

export default AboutPageDesktop;
