import { createContext } from "react";
import CartProduct from "../models/CartProduct";

interface CartContextInterface {
  cartProductList: CartProduct[];
  addProduct: (product: CartProduct) => void;
  removeProduct: (product: string) => void;
}

let warning: CartContextInterface = {
  get cartProductList(): CartProduct[] {
    throw new Error("CartContext.Provider is required");
  },
  get addProduct(): (product: CartProduct) => void {
    throw new Error("CartContext.Provider is required");
  },
  get removeProduct(): (product: string) => void {
    throw new Error("CartContext.Provider is required");
  }
};

let CartContext = createContext<CartContextInterface>(warning);
export default CartContext;
