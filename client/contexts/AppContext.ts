import { createContext } from "react";
import User from "../models/User";

interface AppContextInterface {
  user: User | null;
  login: () => void;
  signup: () => void;
  forgotPassword: () => void;
}

let warning: AppContextInterface = {
  get user(): User | null {
    throw new Error("AppContext.Provider is required");
  },
  get login(): () => void {
    throw new Error("AppContext.Provider is required");
  },
  get signup(): () => void {
    throw new Error("AppContext.Provider is required");
  },
  get forgotPassword(): () => void {
    throw new Error("AppContext.Provider is required");
  }
};

let AppContext = createContext<AppContextInterface>(warning);
export default AppContext;
