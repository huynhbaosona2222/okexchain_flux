import Product from "../models/Product";

function computeTotal(price: number, count: number, product: Product): number {
  let total = price * count;
  // Find largest discount eligible
  for (let i = product.discountByNumber.length - 1; i >= 0; --i) {
    if (count >= product.discountByNumber[i].count) {
      return total * (1 - product.discountByNumber[i].discount / 100);
    }
  }
  return total;
}

export default computeTotal;
