import numeral from "numeral";

function formatPrice(x: number) {
  return numeral(x).format("0,0");
}

export default formatPrice;
