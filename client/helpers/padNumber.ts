function padNumber(x: number, len = 2): string {
  let ans = String(x);
  if (ans.length >= len) {
    return ans;
  }
  return Array(len - ans.length + 1).join("0") + ans;
}

export default padNumber;
