import React from "react";
import ReactDOM from "react-dom";
import Contact from "../pages/Contact";
import "../styles-page/_contact.scss";

ReactDOM.render(<Contact />, document.getElementById("root"));
