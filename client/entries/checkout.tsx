import React from "react";
import ReactDOM from "react-dom";
import CheckoutPage from "../pages/CheckoutPage";
import "../styles-page/_checkout.scss";

ReactDOM.render(<CheckoutPage />, document.getElementById("root"));
