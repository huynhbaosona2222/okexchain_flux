import React from "react";
import ReactDOM from "react-dom";
import qs from "querystring";

import ShopPage from "../pages/ShopPage";
import "../styles-page/_shop.scss";
import dummyProductList from "../data/dummyProductList";

// TODO: remove these lines
function parse(x: string, fallback: number) {
  let n = parseInt(x);
  return isNaN(n) ? fallback : n;
}
let query = qs.parse(location.search.slice(1));
window.__PRELOADED_FILTER__ = {
  page: parse(query.page as string, 1),
  search: query.search as string,
  sort: parse(query.sort as string, 0),
  collectionId: parse(query.collectionId as string, 1),
  colour: parse(query.colour as string, 1),
  price: parse(query.price as string, 1),
  size: parse(query.size as string, 1)
};
window.__PRELOADED_PAGE_LIMIT = 3;
window.__PRELOADED_PRODUCT_LIST__ = dummyProductList.slice(
  12 * (window.__PRELOADED_FILTER__.page - 1),
  12 * window.__PRELOADED_FILTER__.page
);

ReactDOM.render(<ShopPage />, document.getElementById("root"));
