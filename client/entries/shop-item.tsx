import React from "react";
import ReactDOM from "react-dom";
import "../styles-page/_shop-item.scss";
import ShopItemPage from "../pages/ShopItemPage";
import dummyProductList from "../data/dummyProductList";

// TODO: remove these lines
window.__PRELOADED_PRODUCT__ = dummyProductList[0];
window.__PRELOADED_RELATED_ITEM_LIST = dummyProductList.slice(0, 8);

ReactDOM.render(<ShopItemPage />, document.getElementById("root"));
