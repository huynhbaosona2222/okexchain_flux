import React from "react";
import ReactDOM from "react-dom";
import AccountPage from "../pages/AccountPage";
import "../styles-page/_account.scss";

ReactDOM.render(<AccountPage />, document.getElementById("root"));
