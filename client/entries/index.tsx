import React from "react";
import ReactDOM from "react-dom";
import Homepage from "../pages/HomePage";
import "../styles-page/_homepage.scss";

ReactDOM.render(<Homepage />, document.getElementById("root"));
