import React from "react";
import ReactDOM from "react-dom";
import CartPage from "../pages/CartPage";
import "../styles-page/_cart.scss";

ReactDOM.render(<CartPage />, document.getElementById("root"));
