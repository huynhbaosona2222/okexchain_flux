import React from "react";
import ReactDOM from "react-dom";
import AboutPage from "../pages/AboutPage";
import "../styles-page/_about.scss";

ReactDOM.render(<AboutPage />, document.getElementById("root"));
