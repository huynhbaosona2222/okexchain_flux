import OrderRaw from "../models/OrderRaw";
import dummyCartProductList from "./dummyCartProductList";

const dummyOrderList: OrderRaw[] = [
  {
    id: "SA30001",
    date: new Date(2020, 3, 1),
    productList: [dummyCartProductList[0], dummyCartProductList[2]],
    shippingInfo: {
      fullname: "Tran Vy",
      phone1: "0349000123",
      phone2: "0349000976",
      city: "HCM",
      district: "Quan Binh Thanh",
      ward: "Phuong whatever",
      address: "135 Nguyen Huu Canh",
      shippingMethod: 0,
      shippingNote: "",
      paymentMethod: 0
    }
  },
  {
    id: "SA30002",
    date: new Date(2020, 2, 28),
    productList: [
      dummyCartProductList[1],
      dummyCartProductList[0],
      dummyCartProductList[2]
    ],
    shippingInfo: {
      fullname: "Tran Vy",
      phone1: "0349000123",
      phone2: "0349000976",
      city: "HCM",
      district: "Quan Binh Thanh",
      ward: "Phuong whatever",
      address: "135 Nguyen Huu Canh",
      shippingMethod: 0,
      shippingNote: "",
      paymentMethod: 0
    }
  }
];

export default dummyOrderList;
