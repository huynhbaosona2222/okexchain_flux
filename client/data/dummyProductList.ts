import ProductRaw from "../models/ProductRaw";

let dummyProductList: ProductRaw[] = [
  {
    id: "PROD1",
    slug: "product-1",
    name: "Embroidered Stripe 1",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: [
          "/public/images/dummy_product_01.png",
          "/public/images/dummy_product_02.png",
          "/public/images/dummy_product_03.png",
          "/public/images/dummy_product_04.png"
        ]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_02.png"]
      }
    ],
    oldPrice: 1500000,
    price: 499000,
    remainCount: 10,
    sizeList: ["S", "M", "L"],
    discountByNumber: [
      { count: 2, discount: 5 },
      { count: 3, discount: 10 },
      { count: 4, discount: 15 },
      { count: 5, discount: 20 }
    ],
    discountByAccessories: {
      discount: 10,
      accessoryList: [
        {
          id: "PROD2",
          slug: "product-2",
          previewURL: "/public/images/dummy_product_03.png"
        },
        {
          id: "PROD3",
          slug: "product-3",
          previewURL: "/public/images/dummy_product_05.png"
        }
      ]
    }
  },
  {
    id: "PROD2",
    slug: "product-2",
    name: "Embroidered Stripe 2",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_03.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_04.png"]
      }
    ],
    price: 399000,
    remainCount: 2,
    sizeList: ["S", "M"],
    discountByNumber: []
  },
  {
    id: "PROD3",
    slug: "product-3",
    name: "Embroidered Stripe 3",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_05.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_06.png"]
      }
    ],
    price: 499000,
    oldPrice: 599000,
    remainCount: 10,
    sizeList: ["M", "L"],
    discountByNumber: []
  },
  {
    id: "PROD4",
    slug: "product-4",
    name: "Embroidered Stripe 4",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_07.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_08.png"]
      }
    ],
    price: 499000,
    remainCount: 10,
    sizeList: ["S", "M", "L"],
    discountByNumber: []
  },
  {
    id: "PROD5",
    slug: "product-5",
    name: "Embroidered Stripe 5",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_09.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_10.png"]
      }
    ],
    price: 499000,
    remainCount: 10,
    sizeList: ["S", "L"],
    discountByNumber: []
  },
  {
    id: "PROD6",
    slug: "product-6",
    name: "Embroidered Stripe 6",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_11.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_12.png"]
      }
    ],
    price: 499000,
    remainCount: 10,
    sizeList: ["S", "M", "L"],
    discountByNumber: []
  },
  {
    id: "PROD7",
    slug: "product-7",
    name: "Embroidered Stripe 7",
    description: `Your new cozy uniform (translation: you’ll want to wear this sweater and jogger duo nonstop).
    Relaxed fit.
    Soft & drapey. Top has boat neck`,
    colourList: [
      {
        colour: "#F2B75E",
        imageList: ["/public/images/dummy_product_13.png"]
      },
      {
        colour: "#FDB0FF",
        imageList: ["/public/images/dummy_product_14.png"]
      }
    ],
    price: 459000,
    oldPrice: 1000000,
    remainCount: 10,
    sizeList: ["S", "M", "L"],
    discountByNumber: [
      { count: 2, discount: 5 },
      { count: 3, discount: 10 }
    ],
    discountByAccessories: {
      discount: 10,
      accessoryList: [
        {
          id: "PROD1",
          slug: "product-1",
          previewURL: "/public/images/dummy_product_01.png"
        },
        {
          id: "PROD2",
          slug: "product-2",
          previewURL: "/public/images/dummy_product_03.png"
        }
      ]
    }
  }
];

dummyProductList.push(...dummyProductList);
dummyProductList.push(...dummyProductList);
dummyProductList = dummyProductList.map(function(product, idx) {
  return {
    ...product,
    id: "PROD" + (idx + 1)
  };
});

export default dummyProductList;
