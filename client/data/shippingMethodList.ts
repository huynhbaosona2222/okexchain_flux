const shippingMethodList = [
  "Thanh toán khi nhận hàng",
  "Thanh toán bằng chuyển khoản",
  "Thanh toán online"
];

export default shippingMethodList;
