const SORT_OPTION = {
  HIGH_PRICE: 0,
  LOW_PRICE: 1,
  NEWEST: 2
};

const COLLECTIONS = [
  { value: 0, text: "Sản phẩm mới nhất" },
  { value: 1, text: "Tất cả sản phẩm" },
  { value: 2, text: "Pyjama - Đồ bộ" },
  { value: 3, text: "Sleep dress - Váy ngủ" },
  { value: 4, text: "Pajamas - Quần ngủ" }
];

const PRICES = [
  { value: 0, text: "100.000 - 200.000" },
  { value: 1, text: "200.000 - 300.000" },
  { value: 2, text: "300.000 - 400.000" },
  { value: 3, text: "400.000 - 500.000" }
];

const SIZES = [
  { value: 0, text: "XS" },
  { value: 1, text: "S" },
  { value: 2, text: "M" },
  { value: 3, text: "XL" },
  { value: 4, text: "XXL" }
];

const COLOURS = [
  { value: 0, text: "#DFB9C0" },
  { value: 1, text: "#F2B75E" },
  { value: 2, text: "#FDB0FF" },
  { value: 3, text: "#B1D5F5" },
  { value: 4, text: "#F7905A" },
  { value: 5, text: "#FF492E" },
  { value: 6, text: "#D47C61" },
  { value: 7, text: "#146AA8" },
  { value: 8, text: "#A4A4A4" },
  { value: 9, text: "#111010" },
  { value: 10, text: "#87CED9" },
  { value: 11, text: "#7024C4" }
];

export { COLLECTIONS, SORT_OPTION, SIZES, PRICES, COLOURS };
