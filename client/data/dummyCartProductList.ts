import dummyProductList from "./dummyProductList";
import CartProductRaw from "../models/CartProductRaw";

let dummyCartProductList: CartProductRaw[] = [
  {
    product: dummyProductList[0],
    colour: dummyProductList[0].colourList[1].colour,
    size: dummyProductList[0].sizeList[0],
    quantity: 1
  },
  {
    product: dummyProductList[1],
    colour: dummyProductList[1].colourList[0].colour,
    size: dummyProductList[1].sizeList[1],
    quantity: 2
  },
  {
    product: dummyProductList[2],
    colour: dummyProductList[2].colourList[0].colour,
    size: dummyProductList[2].sizeList[0],
    quantity: 1
  }
];

export default dummyCartProductList;
