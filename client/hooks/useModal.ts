import { useRef, useEffect } from "react";

function useModal(onClose: () => void) {
  let ref = useRef<HTMLDivElement>(null);
  useEffect(
    function () {
      function handleClick(e: MouseEvent) {
        if (e.target == ref.current) {
          onClose();
        }
      }
      addEventListener("click", handleClick);
    },
    [onClose]
  );
  return ref;
}

export default useModal;
