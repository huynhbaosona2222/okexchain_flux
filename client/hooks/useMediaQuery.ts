import { useState, useEffect } from "react";

function useMediaQuery(query: string): boolean {
  let [fit, setFit] = useState(function () {
    return window.matchMedia(query).matches;
  });
  useEffect(
    function () {
      function listener(e: MediaQueryListEvent) {
        setFit(e.matches);
      }
      let mql = window.matchMedia(query);
      mql.addListener(listener);
      return function () {
        mql.removeListener(listener);
      };
    },
    [query]
  );

  return fit;
}

export default useMediaQuery;
