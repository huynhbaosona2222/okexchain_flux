import React, { useContext } from "react";
import ModalProps from "../models/ModalProps";
import useModal from "../hooks/useModal";
import computeTotal from "../helpers/computeTotal";
import formatPrice from "../helpers/formatPrice";
import CartProductCard from "./CartProductCard";
import CartContext from "../contexts/CartContext";

function CartSidebar({ show, onClose }: ModalProps) {
  let ref = useModal(onClose);
  let { cartProductList } = useContext(CartContext);

  let totalPrice = 0;
  for (let cartProduct of cartProductList) {
    totalPrice += computeTotal(
      cartProduct.product.price,
      cartProduct.quantity,
      cartProduct.product
    );
  }
  return (
    <div
      className={"cart-sidebar--wrapper" + (show ? " active" : "")}
      ref={ref}>
      <div className="cart-sidebar">
        <div className="cart-sidebar__header">
          <button type="button" onClick={onClose}>
            {/* prettier-ignore */}
            <svg width="12" height="19" viewBox="0 0 12 19" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.5558 9.7804L2.65555 18.6805C2.4497 18.8865 2.1749 19 1.8819 19C1.58889 19 1.3141 18.8865 1.10824 18.6805L0.452802 18.0252C0.0263019 17.5982 0.0263019 16.9042 0.452803 16.4779L7.92656 9.00415L0.444511 1.52209C0.238659 1.31608 0.125002 1.04145 0.125002 0.748604C0.125002 0.455435 0.238659 0.180801 0.444511 -0.0253753L1.09995 -0.680492C1.30597 -0.886505 1.5806 -1 1.87361 -1C2.16661 -1 2.44141 -0.886505 2.64726 -0.680492L11.5558 8.22773C11.7621 8.4344 11.8755 8.71033 11.8748 9.00366C11.8755 9.29813 11.7621 9.5739 11.5558 9.7804Z" fill="white"/>
            </svg>
          </button>
          <h2>Giỏ hàng</h2>
        </div>
        <div className="cart__body">
          {cartProductList.map(function (cartProduct, idx) {
            return <CartProductCard key={idx} cartProduct={cartProduct} />;
          })}
          <hr />
          <div className="cart__total">
            <h2>TỔNG CỘNG</h2>
            <p>{formatPrice(totalPrice)}</p>
          </div>
          <a href="/checkout">
            <button type="button" className="cart__checkout-btn">
              THANH TOÁN
            </button>
          </a>
        </div>
      </div>
    </div>
  );
}

export default CartSidebar;
