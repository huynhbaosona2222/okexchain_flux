import React, { ReactNode, useState, useCallback } from "react";
import AppContext from "../contexts/AppContext";
import LoginPopup from "./LoginPopup";
import SignupPopup from "./SignupPopup";
import ForgotPasswordPopup from "./ForgotPasswordPopup";
import CartProduct from "../models/CartProduct";
import CartContext from "../contexts/CartContext";
import User from "../models/User";
import UserRaw from "../models/UserRaw";
import dummyProductList from "../data/dummyProductList";

declare global {
  interface Window {
    __PRELOADED_USER__: UserRaw | null;
  }
}

const dummyUser: UserRaw = {
  name: "Hello there",
  email: "abc@gmail.com",
  address: "Hello, there",
  sex: 1,
  phone_number: "0123456789"
};

function App({
  children,
  preloadedUser
}: {
  children: ReactNode;
  preloadedUser?: UserRaw;
}) {
  let [loginActive, setLoginActive] = useState(false);
  let [signupActive, setSignupActive] = useState(false);
  let [forgotActive, setForgotActive] = useState(false);

  // Hooks for managing cart
  let [cartProductList, setCartProductList] = useState<CartProduct[]>(
    function() {
      // TODO: fetch cart's product list from SSR
      return [
        new CartProduct({
          product: dummyProductList[0],
          colour: dummyProductList[0].colourList[0].colour,
          size: "S",
          quantity: 1
        }),
        new CartProduct({
          product: dummyProductList[0],
          colour: dummyProductList[0].colourList[0].colour,
          size: "S",
          quantity: 1
        })
      ];
    }
  );
  let addProduct = useCallback(function(product: CartProduct) {
    setCartProductList(p => p.concat(product));
  }, []);
  let removeProduct = useCallback(function(productId: string) {
    setCartProductList(p =>
      p.filter(function({ product }) {
        return product.id !== productId;
      })
    );
  }, []);

  // Hooks for authentication
  let [user, setUser] = useState<User | null>(function() {
    if (typeof preloadedUser !== "undefined") {
      return preloadedUser ? new User(preloadedUser) : null;
    }
    if (window.__PRELOADED_USER__) {
      return new User(window.__PRELOADED_USER__);
    }
    return null;
  });
  let login = useCallback(function() {
    setLoginActive(true);
    setSignupActive(false);
    setForgotActive(false);
  }, []);
  let signup = useCallback(function() {
    setLoginActive(false);
    setSignupActive(true);
    setForgotActive(false);
  }, []);
  let forgotPassword = useCallback(function() {
    setLoginActive(false);
    setSignupActive(false);
    setForgotActive(true);
  }, []);

  return (
    <AppContext.Provider
      value={{
        user,
        login,
        signup,
        forgotPassword
      }}>
      <CartContext.Provider
        value={{
          cartProductList,
          addProduct,
          removeProduct
        }}>
        {children}
      </CartContext.Provider>
      <LoginPopup show={loginActive} onClose={() => setLoginActive(false)} />
      <SignupPopup show={signupActive} onClose={() => setSignupActive(false)} />
      <ForgotPasswordPopup
        show={forgotActive}
        onClose={() => setForgotActive(false)}
      />
    </AppContext.Provider>
  );
}

export default App;
