import React from "react";
import Product from "../models/Product";
import Slider from "react-slick";
import ProductCard from "./ProductCard";

/* eslint-disable @typescript-eslint/no-explicit-any */
function PrevArrow(props: any) {
  return (
    <button
      {...props}
      className={"product-slider__arrow prev " + props.className}>
      {/* prettier-ignore */}
      <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.444197 10.7804L9.34445 19.6805C9.5503 19.8865 9.8251 20 10.1181 20C10.4111 20 10.6859 19.8865 10.8918 19.6805L11.5472 19.0252C11.9737 18.5982 11.9737 17.9042 11.5472 17.4779L4.07344 10.0041L11.5555 2.52209C11.7613 2.31608 11.875 2.04145 11.875 1.7486C11.875 1.45543 11.7613 1.1808 11.5555 0.974625L10.9 0.319508C10.694 0.113495 10.4194 1.27252e-07 10.1264 1.52868e-07C9.83339 1.78483e-07 9.55859 0.113495 9.35274 0.319508L0.444197 9.22773C0.237857 9.4344 0.124525 9.71033 0.125176 10.0037C0.124525 10.2981 0.237857 10.5739 0.444197 10.7804Z" fill="#D47C61"/>
      </svg>
    </button>
  );
}

/* eslint-disable @typescript-eslint/no-explicit-any */
function NextArrow(props: any) {
  return (
    <button
      {...props}
      className={"product-slider__arrow next " + props.className}>
      {/* prettier-ignore */}
      <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.5558 10.7804L2.65555 19.6805C2.4497 19.8865 2.1749 20 1.8819 20C1.58889 20 1.3141 19.8865 1.10824 19.6805L0.452802 19.0252C0.0263019 18.5982 0.0263019 17.9042 0.452803 17.4779L7.92656 10.0041L0.444511 2.52209C0.238659 2.31608 0.125002 2.04145 0.125002 1.7486C0.125002 1.45543 0.238659 1.1808 0.444511 0.974625L1.09995 0.319508C1.30597 0.113495 1.5806 1.27252e-07 1.87361 1.52868e-07C2.16661 1.78483e-07 2.44141 0.113495 2.64726 0.319508L11.5558 9.22773C11.7621 9.4344 11.8755 9.71033 11.8748 10.0037C11.8755 10.2981 11.7621 10.5739 11.5558 10.7804Z" fill="#D47C61"/>
      </svg>
    </button>
  );
}

function ProductSlider({ productList }: { productList: Product[] }) {
  return (
    <div className="product-slider">
      <Slider
        dots={false}
        slidesToShow={4}
        infinite={true}
        prevArrow={<PrevArrow />}
        nextArrow={<NextArrow />}
        responsive={[{ breakpoint: 767, settings: { slidesToShow: 1 } }]}>
        {productList.map(function(product) {
          return (
            <div className="product-slider__item" key={product.id}>
              <ProductCard product={product} />
            </div>
          );
        })}
      </Slider>
    </div>
  );
}

export default ProductSlider;
