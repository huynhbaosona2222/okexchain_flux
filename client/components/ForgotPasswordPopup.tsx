import React, { useCallback, FormEvent } from "react";
import ModalProps from "../models/ModalProps";
import useModal from "../hooks/useModal";

interface ForgotPasswordFormElement extends HTMLFormElement {
  email: HTMLInputElement;
}

function ForgotPasswordPopup({ show, onClose }: ModalProps) {
  let ref = useModal(onClose);

  let handleSubmit = useCallback(function (e: FormEvent) {
    e.preventDefault();
    let form = e.target as ForgotPasswordFormElement;

    // TODO: do something with `email`
    let email = form.email.value;
    alert(email);
  }, []);

  return (
    <div className={"my-modal--wrapper" + (show ? " active" : "")} ref={ref}>
      <div className="my-modal auth">
        <button type="button" className="auth__close-btn" onClick={onClose}>
          &times;
        </button>
        <h1 className="auth__title">Đặt lại mật khẩu</h1>
        <p className="auth__tutorial">
          Chúng tôi sẽ gửi cho bạn một email để thiết lập lại mật khẩu của bạn.
        </p>
        <form className="auth-form" onSubmit={handleSubmit}>
          <label className="auth-form__field">
            <span className="auth-form__field-name">Email</span>
            <input
              type="text"
              className="auth-form__field-value"
              name="email"
              placeholder="Nhập địa chỉ email"
              required
            />
          </label>
          <button type="submit" className="auth-form__submit-btn">
            GỬI ĐI
          </button>
        </form>
      </div>
    </div>
  );
}

export default ForgotPasswordPopup;
