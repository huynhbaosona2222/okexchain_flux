import React, { FormEvent } from "react";

interface FooterFormElement extends HTMLFormElement {
  email: HTMLInputElement;
}

function Footer() {
  function handleSubmit(e: FormEvent) {
    e.preventDefault();
    let form = e.target as FooterFormElement;

    let email = form.email.value;
    // TODO: do something with email
    alert(email);
  }

  return (
    <footer className="footer">
      <div>
        <div className="footer-info">
          <div>
            <h2>Chillax Store</h2>
            <ul>
              <li>Lầu 2, 277E Lê Thánh Tôn, Quận 1, Thành phố Hồ Chí Minh</li>
              <li>028-6272-4728</li>
              <li>Giờ 10:00 - 21:30</li>
              <li>cskh@chillaxhomewear.vn</li>
            </ul>
          </div>
          <div>
            <h2>Chăm sóc khách hàng</h2>
            <ul>
              <li>Chính sách hoàn trả và hoàn tiền</li>
              <li>Chính sách của cửa hàng</li>
              <li>Các câu hỏi thường gặp</li>
            </ul>
          </div>
        </div>
        <form className="footer-email" onSubmit={handleSubmit}>
          <h2>Keep it Trendy</h2>
          <p>Đăng kí để không bỏ lỡ tin mới từ chúng tôi</p>
          <div>
            <input type="text" name="email" placeholder="Nhập email" />
            <button type="submit">Nhận tin</button>
          </div>
        </form>
      </div>
      <p>© 2020 Chillax Homewear Limited</p>
    </footer>
  );
}

export default Footer;
