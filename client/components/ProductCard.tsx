import React from "react";
import Product from "../models/Product";
import formatPrice from "../helpers/formatPrice";

function ProductCard({ product }: { product: Product }) {
  return (
    <div className="product-card">
      {/* TODO: change this url to something related to `product` */}
      <a href="/shop-item" className="product-card__img">
        <img src={product.colourList[0].imageList[0]} alt={product.name} />
      </a>
      <h3 className="product-card__name">{product.name}</h3>
      <div className="product-card__colour-list">
        {product.colourList.map(function({ colour }) {
          return (
            <div
              key={colour}
              className="product-card__colour"
              style={{ backgroundColor: colour }}
            />
          );
        })}
      </div>
      <div className="product-card__price">
        {product.oldPrice && (
          <span className="old" aria-label="Giá cũ">
            {formatPrice(product.oldPrice)}
          </span>
        )}
        <span aria-label="Giá">{formatPrice(product.price)}</span>
      </div>
      {product.discountByAccessories && (
        <div className="product-card__discount">
          -{product.discountByAccessories.discount}% khi mua kèm phụ kiện
        </div>
      )}
    </div>
  );
}

export default ProductCard;
