import React from "react";
import ModalProps from "../models/ModalProps";
import useModal from "../hooks/useModal";

function SizeTablePopup({ show, onClose }: ModalProps) {
  let ref = useModal(onClose);
  return (
    <div className={"my-modal--wrapper" + (show ? " active" : "")} ref={ref}>
      <div className="my-modal size-table">
        <button type="button" onClick={onClose}>
          &times;
        </button>
        <img src="/public/images/size-table.png" alt="Bảng size" />
      </div>
    </div>
  );
}

export default SizeTablePopup;
