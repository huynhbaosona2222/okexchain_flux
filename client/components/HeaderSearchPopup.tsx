import React, {
  useCallback,
  FormEvent,
  Dispatch,
  useRef,
  useEffect
} from "react";

interface HeaderSearchPopupForm extends HTMLFormElement {
  phone: HTMLInputElement;
}

interface HeaderSearchPopupProps {
  active: boolean;
  setActive: Dispatch<boolean | ((prev: boolean) => boolean)>;
}

function HeaderSearchPopup({ active, setActive }: HeaderSearchPopupProps) {
  let handleSearch = useCallback(function (e: FormEvent) {
    e.preventDefault();
    let form = e.target as HeaderSearchPopupForm;

    let phone = form.phone.value;
    // TODO: do some thing with `phone`
    alert(phone);
  }, []);

  let ref = useRef<HTMLFormElement>(null);
  useEffect(
    function () {
      function clickListener(e: MouseEvent) {
        if (active && !ref.current?.contains(e.target as Node)) {
          setActive(false);
        }
      }
      addEventListener("click", clickListener);
      return function () {
        removeEventListener("click", clickListener);
      };
    },
    [active, setActive]
  );

  return (
    <form
      ref={ref}
      className={"header__search-popup " + (active ? "active" : "")}
      onSubmit={handleSearch}>
      <div>
        <label>
          <span>Số điện thoại</span>
          <input
            type="text"
            name="phone"
            inputMode="numeric"
            pattern="[0-9]*"
            placeholder="Nhập số điện thoại đặt hàng"
            required
          />
        </label>
        <button type="submit" className="header__search-popup__submit-btn">
          Tra cứu
        </button>
        <button
          type="button"
          className="header__search-popup__close-btn"
          aria-label="Đóng form"
          onClick={() => setActive(false)}>
          {/* prettier-ignore */}
          <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M15.8347 0.834381L9.00034 7.67812L2.16597 0.834381L0.834717 2.16563L7.67847 9L0.834717 15.8344L2.16597 17.1656L9.00034 10.3219L15.8347 17.1656L17.166 15.8344L10.3222 9L17.166 2.16563L15.8347 0.834381Z" fill="#727272"/>
        </svg>
        </button>
      </div>
    </form>
  );
}

export default HeaderSearchPopup;
