import React, { Fragment, ReactNode } from "react";
import qs from "querystring";

interface PaginationButtonProps {
  active?: boolean;
  page: number;
  children: ReactNode;
  query: qs.ParsedUrlQueryInput;
  baseUrl: string;
}

interface PaginationProps {
  page: number;
  limit: number;
  baseUrl: string;
  query?: qs.ParsedUrlQueryInput;
}

const PAGE_LIMIT = 2;

function PaginationButton({
  active = false,
  page,
  children,
  query,
  baseUrl
}: PaginationButtonProps) {
  let url = baseUrl + "?" + qs.stringify({ ...query, page });
  return (
    <a href={url} className={"pagination-button" + (active ? " active" : "")}>
      {children}
    </a>
  );
}

function Pagination({ page, limit, baseUrl, query = {} }: PaginationProps) {
  let right = Math.min(page + PAGE_LIMIT - 1, limit);
  let left = Math.max(1, right - PAGE_LIMIT + 1);

  let arr = [];
  for (let i = left; i <= right; ++i) {
    arr.push(
      <PaginationButton
        key={i}
        active={i === page}
        page={i}
        query={query}
        baseUrl={baseUrl}>
        {i}
      </PaginationButton>
    );
  }

  return (
    <div className="pagination">
      {limit > PAGE_LIMIT && (
        <Fragment>
          <PaginationButton page={1} query={query} baseUrl={baseUrl}>
            {/* prettier-ignore */}
            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0.421484 7.08782L8.59974 0L9.31396 0.824355L1.61114 7.5L9.31396 14.1756L8.59974 15L0.421484 7.91218C0.30207 7.80858 0.233398 7.65812 0.233398 7.5C0.233398 7.34188 0.30207 7.19142 0.421484 7.08782Z" fill="black"/>
              <path d="M5.87409 7.08782L14.0523 0L14.7666 0.824355L7.06378 7.5L14.7666 14.1756L14.0523 15L5.87409 7.91218C5.75468 7.80803 5.68601 7.65812 5.68601 7.5C5.68601 7.34188 5.75471 7.19194 5.87409 7.08782Z" fill="black"/>
            </svg>
          </PaginationButton>
          <PaginationButton
            page={Math.max(1, page - 1)}
            query={query}
            baseUrl={baseUrl}>
            {/* prettier-ignore */}
            <svg width="10" height="15" viewBox="0 0 10 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0.421484 7.08782L8.59974 0L9.31396 0.824355L1.61114 7.5L9.31396 14.1756L8.59974 15L0.421484 7.91218C0.30207 7.80858 0.233398 7.65812 0.233398 7.5C0.233398 7.34188 0.30207 7.19142 0.421484 7.08782Z" fill="black"/>
            </svg>
          </PaginationButton>
        </Fragment>
      )}
      {arr}
      {limit > PAGE_LIMIT && (
        <Fragment>
          <PaginationButton
            page={Math.min(limit, page + 1)}
            query={query}
            baseUrl={baseUrl}>
            {/* prettier-ignore */}
            <svg width="10" height="15" viewBox="0 0 10 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M9.57852 7.08782L1.40026 0L0.686035 0.824355L8.38886 7.5L0.686035 14.1756L1.40026 15L9.57852 7.91218C9.69793 7.80858 9.7666 7.65812 9.7666 7.5C9.7666 7.34188 9.69793 7.19142 9.57852 7.08782Z" fill="black"/>
            </svg>
          </PaginationButton>
          <PaginationButton page={limit} query={query} baseUrl={baseUrl}>
            {/* prettier-ignore */}
            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14.5785 7.08782L6.40026 0L5.68604 0.824355L13.3889 7.5L5.68604 14.1756L6.40026 15L14.5785 7.91218C14.6979 7.80858 14.7666 7.65812 14.7666 7.5C14.7666 7.34188 14.6979 7.19142 14.5785 7.08782Z" fill="black"/>
              <path d="M9.12591 7.08782L0.947656 0L0.233398 0.824355L7.93622 7.5L0.233398 14.1756L0.947656 15L9.12591 7.91218C9.24532 7.80803 9.31399 7.65812 9.31399 7.5C9.31399 7.34188 9.24529 7.19194 9.12591 7.08782Z" fill="black"/>
            </svg>
          </PaginationButton>
        </Fragment>
      )}
    </div>
  );
}

export default Pagination;
