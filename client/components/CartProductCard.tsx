import React from "react";
import CartProduct from "../models/CartProduct";
import computeTotal from "../helpers/computeTotal";
import formatPrice from "../helpers/formatPrice";
import padNumber from "../helpers/padNumber";

interface CartProductCardProps {
  cartProduct: CartProduct;
  compact?: boolean;
}

function CartProductCard({
  cartProduct,
  compact = false
}: CartProductCardProps) {
  if (!cartProduct.previewImage) {
    return null;
  }

  let total = computeTotal(
    cartProduct.product.price,
    cartProduct.quantity,
    cartProduct.product
  );
  return (
    <div className={"cart__product-card" + (compact ? " compact" : "")}>
      <div className="cart__img-container">
        <img src={cartProduct.previewImage} alt="" />
      </div>
      <div>
        <h3 className="cart__product-name">{cartProduct.product.name}</h3>
        <p className="cart__product-price">{formatPrice(total)}</p>
        <p className="cart__divider" />
        <ul className="cart__info">
          <li>
            Colour:{" "}
            <span
              className="cart__colour"
              style={{ backgroundColor: cartProduct.colour }}
            />
          </li>
          <li>Size: {cartProduct.size}</li>
          <li>Quantity: {padNumber(cartProduct.quantity)}</li>
        </ul>
      </div>
    </div>
  );
}

export default CartProductCard;
