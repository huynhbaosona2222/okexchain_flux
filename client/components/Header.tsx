import React, { useState, useContext, ComponentProps } from "react";
import HeaderSearchPopup from "./HeaderSearchPopup";
import SizeTablePopup from "./SizeTablePopup";
import CartSidebar from "./CartSidebar";
import AppContext from "../contexts/AppContext";
import CartContext from "../contexts/CartContext";
import useModal from "../hooks/useModal";

function Logo() {
  return (
    <a href="/" id="logo-wrapper">
      <div id="logo">
        <img src="/public/images/chillax_logo.png" alt="Logo" />
        <div>
          <p className="site-name">CHILLAX HOMEWEAR</p>
          <p className="site-desc">Homewear &amp; Accessories</p>
        </div>
      </div>
    </a>
  );
}

function HamburgerButton(props: ComponentProps<"button">) {
  return (
    <button
      {...props}
      type="button"
      className={props.className + " hamburger-icon"}>
      <svg
        style={{ transform: "rotate(180deg)" }}
        width="25"
        height="25"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M18.718 5.58974H1.28207C0.573828 5.58974 0 5.01591 0 4.30771C0 3.5995 0.573828 3.02563 1.28207 3.02563H18.718C19.4262 3.02563 20 3.59946 20 4.30771C20 5.01595 19.4262 5.58974 18.718 5.58974Z"
          fill="#FF492E"
        />
        <path
          d="M11.5385 11.2821H1.28207C0.573828 11.2821 0 10.7082 0 10C0 9.29182 0.573828 8.71796 1.28207 8.71796H11.5385C12.2467 8.71796 12.8205 9.29178 12.8205 10C12.8205 10.7083 12.2467 11.2821 11.5385 11.2821Z"
          fill="#FF492E"
        />
        <path
          d="M18.718 11.2821H16.6667C15.9585 11.2821 15.3846 10.7083 15.3846 10C15.3846 9.29178 15.9584 8.71796 16.6667 8.71796H18.718C19.4262 8.71796 20 9.29178 20 10C20 10.7083 19.4262 11.2821 18.718 11.2821Z"
          fill="#FFBBC0"
        />
        <path
          d="M18.718 16.3077H1.28207C0.573828 16.3077 0 15.7338 0 15.0256C0 14.3174 0.573828 13.7435 1.28207 13.7435H18.718C19.4262 13.7435 20 14.3174 20 15.0256C20 15.7338 19.4262 16.3077 18.718 16.3077Z"
          fill="#FF492E"
        />
      </svg>
    </button>
  );
}

function Header({ route }: { route: string }) {
  let [searchActive, setSearchActive] = useState(false);
  let [sizeTableActive, setSizeTableActive] = useState(false);
  let [cartActive, setCartActive] = useState(false);
  let [navActive, setNavActive] = useState(false);

  let navRef = useModal(function() {
    setNavActive(false);
  });

  let { user, login } = useContext(AppContext);
  let { cartProductList } = useContext(CartContext);

  return (
    <header className="header">
      <Logo />
      <HamburgerButton onClick={() => setNavActive(true)} />

      <nav
        className={"header-nav--wrapper" + (navActive ? " active" : "")}
        ref={navRef}>
        <ul className="header-nav">
          <li className="search-icon">
            <button type="button" onClick={() => setSearchActive((p) => !p)}>
              {/* prettier-ignore */}
              <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14.2043 9.41644C14.2043 8.94364 13.814 8.56039 13.3326 8.56039H4.35378C3.87233 8.56039 3.48206 8.94364 3.48206 9.41644C3.48206 9.88923 3.87233 10.2725 4.35378 10.2725H13.3326C13.814 10.2725 14.2043 9.88923 14.2043 9.41644Z" fill="#111010"/>
                <path d="M4.35378 11.9846C3.87233 11.9846 3.48206 12.3678 3.48206 12.8406C3.48206 13.3134 3.87233 13.6966 4.35378 13.6966H9.80692C10.2884 13.6966 10.6786 13.3134 10.6786 12.8406C10.6786 12.3678 10.2884 11.9846 9.80692 11.9846H4.35378Z" fill="#111010"/>
                <path d="M6.40233 20.2026H3.4869C2.52556 20.2026 1.74345 19.4345 1.74345 18.4905V3.42416C1.74345 2.48012 2.52556 1.71208 3.4869 1.71208H14.2043C15.1656 1.71208 15.9477 2.48012 15.9477 3.42416V8.68881C15.9477 9.1616 16.338 9.54485 16.8194 9.54485C17.3009 9.54485 17.6912 9.1616 17.6912 8.68881V3.42416C17.6912 1.53608 16.1269 0 14.2043 0H3.4869C1.56422 0 0 1.53608 0 3.42416V18.4905C0 20.3786 1.56422 21.9146 3.4869 21.9146H6.40233C6.88379 21.9146 7.27406 21.5314 7.27406 21.0586C7.27406 20.5858 6.88379 20.2026 6.40233 20.2026Z" fill="#111010"/>
                <path d="M19.2352 12.3943C18.2156 11.393 16.5565 11.393 15.5375 12.3936L10.7516 17.083C10.65 17.1826 10.575 17.3053 10.5332 17.4402L9.49097 20.8098C9.39856 21.1085 9.47907 21.4329 9.70109 21.6564C9.86742 21.8238 10.0934 21.9147 10.3251 21.9147C10.4027 21.9147 10.4809 21.9045 10.5578 21.8836L14.0763 20.9265C14.2211 20.8871 14.353 20.8116 14.4594 20.7074L19.2353 16.0262C20.2549 15.0249 20.2549 13.3957 19.2352 12.3943ZM13.39 19.3366L11.6198 19.8181L12.1379 18.1433L15.3671 14.9792L16.6002 16.1901L13.39 19.3366ZM18.0031 14.8151L17.8342 14.9806L16.6014 13.77L16.7697 13.605C17.1096 13.2712 17.6626 13.2712 18.0025 13.605C18.3424 13.9388 18.3424 14.4818 18.0031 14.8151Z" fill="#111010"/>
                <path d="M13.3326 5.13623H4.35378C3.87233 5.13623 3.48206 5.51948 3.48206 5.99227C3.48206 6.46507 3.87233 6.84832 4.35378 6.84832H13.3326C13.814 6.84832 14.2043 6.46507 14.2043 5.99227C14.2043 5.51948 13.814 5.13623 13.3326 5.13623Z" fill="#111010"/>
              </svg>
              Tra cứu đơn hàng
            </button>
            <HeaderSearchPopup
              active={searchActive}
              setActive={setSearchActive}
            />
          </li>
          <li className="cart-icon">
            <a href="/cart">
              {/* prettier-ignore */}
              <svg width="22" height="22" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.2579 16.8937H7.59448V17.8622H22.2579V16.8937Z" fill="black"/>
                <path d="M20.3268 20.2834H9.53149V21.2519H20.3268V20.2834Z" fill="black"/>
                <path d="M15.4843 23.6673H14.5157V24.6358H15.4843V23.6673Z" fill="black"/>
                <path d="M17.4213 23.6673H16.4528V24.6358H17.4213V23.6673Z" fill="black"/>
                <path d="M13.5472 23.6673H12.5787V24.6358H13.5472V23.6673Z" fill="black"/>
                <path d="M28.063 11.0886H18.7028L26.0433 3.74802C26.7992 2.99211 26.7992 1.76377 26.0433 1.00786C25.311 0.275578 24.0354 0.275578 23.3091 1.00786L14.1142 10.2027C13.8602 10.4567 13.7008 10.7638 13.6181 11.0886H1.93701C0.86811 11.0886 0 11.9567 0 13.0256C0 13.9764 0.685039 14.7618 1.58858 14.9272L6.44291 27.5413H23.5571L28.4114 14.9272C29.315 14.7618 30 13.9705 30 13.0256C30 11.9567 29.1319 11.0886 28.063 11.0886ZM14.7992 10.8878L23.9941 1.6929C24.1772 1.50983 24.4193 1.40944 24.6791 1.40944C24.939 1.40944 25.1811 1.50983 25.3642 1.6929C25.7421 2.07085 25.7421 2.68503 25.3642 3.06298L17.3386 11.0886L16.1693 12.2579C15.7913 12.6358 15.1772 12.6358 14.7992 12.2579C14.752 12.2106 14.7106 12.1575 14.6752 12.1043C14.6693 12.0984 14.6634 12.0866 14.6634 12.0748C14.6339 12.0275 14.6102 11.9803 14.5866 11.9331V11.9272C14.4449 11.5787 14.5157 11.1712 14.7992 10.8878ZM22.8957 26.5728H7.10433L2.63976 14.9567H27.3602L22.8957 26.5728Z" fill="black"/>
              </svg>
              {cartProductList.length}
            </a>
          </li>
          <span className="divider" aria-hidden="true" />
          <li className={(route === "/" ? "active" : "") + " to-sidebar"}>
            <a href="/">Trang chủ</a>
          </li>
          <li className={route === "/shop" ? "active" : ""}>
            <a href="/shop">Shop</a>
          </li>
          <li className="to-sidebar">
            <button type="button" onClick={() => setSizeTableActive(true)}>
              Bảng size
            </button>
          </li>
          <li className={(route === "/about" ? "active" : "") + " to-sidebar"}>
            <a href="/about">Giới thiệu</a>
          </li>
          <li
            className={(route === "/contact" ? "active" : "") + " to-sidebar"}>
            <a href="/contact">Liên hệ</a>
          </li>
          <li className={route === "/account" && user ? "active" : ""}>
            {user ? (
              <a href="/account">Tài khoản</a>
            ) : (
              <button type="button" onClick={login}>
                Đăng nhập
              </button>
            )}
          </li>
          <li className="cart-icon hide-sidebar">
            <button type="button" onClick={() => setCartActive(true)}>
              {/* prettier-ignore */}
              <svg width="22" height="22" viewBox="0 0 30 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.2579 16.8937H7.59448V17.8622H22.2579V16.8937Z" fill="black"/>
                <path d="M20.3268 20.2834H9.53149V21.2519H20.3268V20.2834Z" fill="black"/>
                <path d="M15.4843 23.6673H14.5157V24.6358H15.4843V23.6673Z" fill="black"/>
                <path d="M17.4213 23.6673H16.4528V24.6358H17.4213V23.6673Z" fill="black"/>
                <path d="M13.5472 23.6673H12.5787V24.6358H13.5472V23.6673Z" fill="black"/>
                <path d="M28.063 11.0886H18.7028L26.0433 3.74802C26.7992 2.99211 26.7992 1.76377 26.0433 1.00786C25.311 0.275578 24.0354 0.275578 23.3091 1.00786L14.1142 10.2027C13.8602 10.4567 13.7008 10.7638 13.6181 11.0886H1.93701C0.86811 11.0886 0 11.9567 0 13.0256C0 13.9764 0.685039 14.7618 1.58858 14.9272L6.44291 27.5413H23.5571L28.4114 14.9272C29.315 14.7618 30 13.9705 30 13.0256C30 11.9567 29.1319 11.0886 28.063 11.0886ZM14.7992 10.8878L23.9941 1.6929C24.1772 1.50983 24.4193 1.40944 24.6791 1.40944C24.939 1.40944 25.1811 1.50983 25.3642 1.6929C25.7421 2.07085 25.7421 2.68503 25.3642 3.06298L17.3386 11.0886L16.1693 12.2579C15.7913 12.6358 15.1772 12.6358 14.7992 12.2579C14.752 12.2106 14.7106 12.1575 14.6752 12.1043C14.6693 12.0984 14.6634 12.0866 14.6634 12.0748C14.6339 12.0275 14.6102 11.9803 14.5866 11.9331V11.9272C14.4449 11.5787 14.5157 11.1712 14.7992 10.8878ZM22.8957 26.5728H7.10433L2.63976 14.9567H27.3602L22.8957 26.5728Z" fill="black"/>
              </svg>
              {cartProductList.length}
            </button>
          </li>
        </ul>
      </nav>

      <SizeTablePopup
        show={sizeTableActive}
        onClose={() => setSizeTableActive(false)}
      />
      <CartSidebar show={cartActive} onClose={() => setCartActive(false)} />
    </header>
  );
}

export default Header;
