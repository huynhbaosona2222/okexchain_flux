import React, { useState, useCallback, FormEvent, useContext } from "react";
import ModalProps from "../models/ModalProps";
import useModal from "../hooks/useModal";
import AppContext from "../contexts/AppContext";

interface LoginFormElement extends HTMLFormElement {
  user: HTMLInputElement;
  password: HTMLInputElement;
}

function LoginPopup({ show, onClose }: ModalProps) {
  let ref = useModal(onClose);
  let [type, setType] = useState(0);

  let handleLogin = useCallback(
    function (e: FormEvent) {
      e.preventDefault();
      let form = e.target as LoginFormElement;

      let user = form.user.value;
      let password = form.password.value;

      // TODO: do something with `payload`
      let payload =
        type === 0 ? { email: user, password } : { phone: user, password };
      console.log(payload);
    },
    [type]
  );

  let { signup, forgotPassword } = useContext(AppContext);

  return (
    <div className={"my-modal--wrapper" + (show ? " active" : "")} ref={ref}>
      <div className="my-modal auth">
        <button type="button" className="auth__close-btn" onClick={onClose}>
          &times;
        </button>
        <h1 className="auth__title">Đăng nhập</h1>
        <p className="auth__tutorial">
          Vui lòng chọn 1 trong 2 phương thức đăng nhập sau
        </p>
        <form className="auth-form" onSubmit={handleLogin}>
          <label className="auth-form__field">
            <div className="auth-form__field-name">
              <button
                type="button"
                className={type === 0 ? "active" : ""}
                onClick={() => setType(0)}>
                Email
              </button>
              <span>|</span>
              <button
                type="button"
                className={type === 1 ? "active" : ""}
                onClick={() => setType(1)}>
                Số điện thoại
              </button>
            </div>
            <input
              type="text"
              className="auth-form__field-value"
              name="user"
              placeholder={
                type === 0 ? "Nhập địa chỉ email" : "Nhập số điện thoại"
              }
              required
              {...(type === 1
                ? { inputMode: "numeric", pattern: "[0-9]*" }
                : {})}
            />
          </label>
          <label className="auth-form__field">
            <span className="auth-form__field-name">Mật khẩu</span>
            <input
              type="password"
              className="auth-form__field-value"
              name="password"
              placeholder="Nhập mật khẩu của bạn"
              required
            />
          </label>
          <button type="submit" className="auth-form__submit-btn">
            ĐĂNG NHẬP
          </button>
        </form>
        <div className="auth-footer">
          <button
            type="button"
            className="auth-footer__action"
            onClick={signup}>
            Đăng ký
          </button>
          <button
            type="button"
            className="auth-footer__action"
            onClick={forgotPassword}>
            Quên mật khẩu?
          </button>
        </div>
      </div>
    </div>
  );
}

export default LoginPopup;
